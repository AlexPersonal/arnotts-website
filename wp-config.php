<?php
/**
 * Deployment environment switch.
 *
 */

/**
 * Depending on the currently called URL it'll be decided which database
 * configuration will be taken.
 *
 * - 'local' should be chosen for local development
 * - 'staging' will point to the staging database
 * - 'uat' will point to the uat database
 */


if (!isset($_SERVER['DOCUMENT_ROOT']) || empty($_SERVER['DOCUMENT_ROOT']))
    $_SERVER['DOCUMENT_ROOT'] = __DIR__;

if (strpos($_SERVER['HTTP_HOST'],'.local') !== false || strpos($_SERVER['HTTP_HOST'],'localhost') !== false) {

	 require $_SERVER['DOCUMENT_ROOT'] . '/config/local.php';
  // require $_SERVER['DOCUMENT_ROOT'] . '/config/staging.php';

} else if((strpos($_SERVER['HTTP_HOST'], 'arnotts-corporate-stage.orchard.net.au') !== false) ||
    (strpos($_SERVER['HTTP_HOST'], 'stage') !== false))  {

	require $_SERVER['DOCUMENT_ROOT'] . '/config/staging.php';

} else if((strpos($_SERVER['HTTP_HOST'], 'aws143-001d.server-access.com') !== false))  {

    require $_SERVER['DOCUMENT_ROOT'] . '/config/uat.php';

} else if((strpos($_SERVER['HTTP_HOST'], 'prelive') !== false) ||
    (strpos($_SERVER['HTTP_HOST'], 'server-access.com') !== false) ||
    (strpos($_SERVER['HTTP_HOST'], 'arnotts.com.au') !== false))  {

    require $_SERVER['DOCUMENT_ROOT'] . '/config/live.php';

}

define( 'WP_AUTO_UPDATE_CORE', false );


require $_SERVER['DOCUMENT_ROOT'].'/config/global-configs.php';
  
  

if(is_admin()) {
    add_filter('filesystem_method', create_function('$a','return "direct";' ));
    define('FS_CHMOD_DIR', 0751);
}
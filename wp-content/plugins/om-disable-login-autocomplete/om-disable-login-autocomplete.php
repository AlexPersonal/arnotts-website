<?php
/**
 * Plugin Name: OM Remove login autocomplete
 * Description: Disable login password autocomplete
 * Version: 0.1
 * Author: Kevin Miller via https://buzelac.com/2014/10/disabling-wordpress-login-password-autocomplete/
 * License: WTFPL
 */
 
add_action('login_init', 'om_autocomplete_login_init');
function om_autocomplete_login_init()
{
	ob_start();
}
 
add_action('login_form', 'om_autocomplete_login_form');
function om_autocomplete_login_form()
{
	$content = ob_get_contents();
	ob_end_clean();
 
	$content = str_replace('id="loginform"', 'id="loginform" autocomplete="off"', $content);
 
	echo $content;
}

add_action('lostpassword_init', 'om_autocomplete_lostpassword_init');
function om_autocomplete_lostpassword_init()
{
	ob_start();
}
 
add_action('lostpassword_form', 'om_autocomplete_lostpassword_form');
function om_autocomplete_lostpassword_form()
{
	$content = ob_get_contents();
	ob_end_clean();

	$content = str_replace('id="lostpasswordform"', 'id="lostpasswordform" autocomplete="off"', $content);

	echo $content;
}

<?php
/*
Title: Tealium Settings Section
Setting: tealium_tool_settings
*/

/* Proprietary - Copyright Campbell Soup Company - Not authorized for use on any properties not owned by Campbell Soup Company */
$post_types = get_post_types();

//Remove post types revision and nav_menu_item
if(($key = array_search('revision', $post_types)) !== false) {
    unset($post_types[$key]);
}
if(($key = array_search('nav_menu_item', $post_types)) !== false) {
    unset($post_types[$key]);
}

$page_types = array(
    '__generic' => 'All Pages',
    '__category' => 'Category Pages',
    '__search' => 'Search Pages'
);
foreach($post_types as $p){
    $page_types[$p] = $p;
}

$site_keys = array(
    null => 'Select a Value',
    'site_title' => 'Site Title',
    'site_tagline' => 'Site Tagline',
    'site_language' => 'Site Language',
    'site_url' => 'Site Root URL'
);


$post_keys = array(
    null => 'Select a Value',
    'ID' => 'ID',
    'post_author' => 'post_author',
    'post_name' => 'post_name',
    'post_type' => 'post_type',
    'post_title' => 'post_title',
    'post_date' => 'post_date',
    'post_date_gmt' => 'post_date_gmt',
    'post_content' => 'post_content',
    'post_excerpt' => 'post_excerpt',
    'post_status' => 'post_status',
    'comment_status' => 'comment_status',
    'ping_status' => 'ping_status',
    'post_password' => 'post_password',
    'post_parent' => 'post_parent',
    'post_modified' => 'post_modified',
    'post_modified_gmt' => 'post_modified_gmt',
    'comment_count' => 'comment_count',
    'menu_order' => 'menu_order'
);

$calculated_fields = array(
    null => 'Select a Value',
    'title' => 'Page Title',
    'excerpt' => 'Page Excerpt'
);

piklist('field', array(
    'type' => 'text'
    ,'field' => 'tealium_account'
    ,'label' => 'Tealium Account'
));

piklist('field', array(
    'type' => 'text'
    ,'field' => 'tealium_profile'
    ,'label' => 'Tealium Profile'
));

piklist('field', array(
    'type' => 'text'
    ,'field' => 'tealium_environment'
    ,'label' => 'Tealium Environment'
));


foreach($page_types as $ptype => $plabel) :
    $is_special_ptype = substr($ptype, 0, 2) === '__';

    $taxonomies = get_object_taxonomies($ptype, 'objects');
    //Collapse from array of object to key/value
    foreach(array_keys($taxonomies) as $key) :
        $taxonomies[$key] = $taxonomies[$key]->label;
    endforeach;
    
    $taxonomies[null] = 'Select a Value';
    piklist('field', array(
        'type' => 'html'
        ,'label' => ''
        ,'field' => 'field-header-' . $ptype // 'field' is only required for a settings page.
        ,'description' => ''
        ,'value' =>sprintf('<h2>Settings for %1$s</h2>', $plabel)
    ));
    
    piklist('field', array(
        'type' => 'group'
        ,'field' => 'tealium-' . $ptype . '-static-settings'
        ,'label' => 'Static Mappings'
        ,'columns' => 12
        ,'add_more' => true
        ,'fields' => array(
            array(
                'type' => 'text'
                ,'field' => 'wp_variable'
                ,'columns' => 6
                ,'attributes' => array(
                  'placeholder' => 'Static Value'
                )
            )
            ,array(
              'type' => 'text'
              ,'field' => 'udo_variable'
              ,'columns' => 6
              ,'attributes' => array(
                'placeholder' => 'UDO Variable'
              )
            )
        )
    ));
        
    piklist('field', array(
        'type' => 'group'
        ,'field' => 'tealium-' . $ptype . '-site-settings'
        ,'label' => 'Site Setting Mappings'
        ,'columns' => 12
        ,'add_more' => true
        ,'fields' => array(
            array(
                'type' => 'select'
                ,'field' => 'wp_variable'
                ,'columns' => 6
                ,'attributes' => array(
                  'placeholder' => 'WordPress Value'
                ),
                'choices' => $site_keys
            )
            ,array(
              'type' => 'text'
              ,'field' => 'udo_variable'
              ,'columns' => 6
              ,'attributes' => array(
                'placeholder' => 'UDO Variable'
              )
            )
        )
    ));
    
    if(sizeof($taxonomies) > 1){
        piklist('field', array(
            'type' => 'group'
            ,'field' => 'tealium-' . $ptype . '-taxonomy-settings'
            ,'label' => 'Taxonomy Mappings'
            ,'columns' => 12
            ,'add_more' => true
            ,'fields' => array(
                array(
                    'type' => 'select'
                    ,'field' => 'wp_variable'
                    ,'columns' => 6
                    ,'attributes' => array(
                      'placeholder' => 'WordPress Value'
                    ),
                    'choices' => $taxonomies
                    ,'value' => null
                )
                ,array(
                  'type' => 'text'
                  ,'field' => 'udo_variable'
                  ,'columns' => 6
                  ,'attributes' => array(
                    'placeholder' => 'UDO Variable'
                  )
                )
            )
        ));
    }
    
    if(!$is_special_ptype){
        piklist('field', array(
            'type' => 'group'
            ,'field' => 'tealium-' . $ptype . '-post-settings'
            ,'label' => 'Post Mappings'
            ,'columns' => 12
            ,'add_more' => true
            ,'fields' => array(
                array(
                    'type' => 'select'
                    ,'field' => 'wp_variable'
                    ,'columns' => 6
                    ,'attributes' => array(
                      'placeholder' => 'WordPress Value'
                    ),
                    'choices' => $post_keys
                )
                ,array(
                  'type' => 'text'
                  ,'field' => 'udo_variable'
                  ,'columns' => 6
                  ,'attributes' => array(
                    'placeholder' => 'UDO Variable'
                  )
                )
            )
        ));
    
        piklist('field', array(
            'type' => 'group'
            ,'field' => 'tealium-' . $ptype . '-calculated-settings'
            ,'label' => 'Calculated Mappings'
            ,'columns' => 12
            ,'add_more' => true
            ,'fields' => array(
                array(
                    'type' => 'select'
                    ,'field' => 'wp_variable'
                    ,'columns' => 6
                    ,'attributes' => array(
                      'placeholder' => 'WordPress Value'
                    ),
                    'choices' => $calculated_fields
                )
                ,array(
                  'type' => 'text'
                  ,'field' => 'udo_variable'
                  ,'columns' => 6
                  ,'attributes' => array(
                    'placeholder' => 'UDO Variable'
                  )
                )
            )
        ));
        
        piklist('field', array(
            'type' => 'group'
            ,'field' => 'tealium-' . $ptype . '-meta-settings'
            ,'label' => 'Meta Mappings'
            ,'columns' => 12
            ,'add_more' => true
            ,'fields' => array(
                array(
                    'type' => 'text'
                    ,'field' => 'wp_variable'
                    ,'columns' => 6
                    ,'attributes' => array(
                      'placeholder' => 'Meta Key'
                    ),
                    'choices' => $post_keys
                )
                ,array(
                  'type' => 'text'
                  ,'field' => 'udo_variable'
                  ,'columns' => 6
                  ,'attributes' => array(
                    'placeholder' => 'UDO Variable'
                  )
                )
            )
        ));
    }
endforeach;

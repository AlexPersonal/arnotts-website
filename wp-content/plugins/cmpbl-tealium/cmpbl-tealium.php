<?php
/*
Plugin Name: Campbell Tealium Configuration
Description: Allows for more customized configuration of Tealium UDO
Author: Rory Murphy
Author URI: https://github.com/rorymurphy/
Version: 1.0.0
License: Proprietary - Copyright Campbell Soup Company - Not authorized for use on any properties not owned by Campbell Soup Company
Plugin Type: Piklist
*/

class Tealium_Plugin {
    
    function __construct() {
        $this->register_hooks();
    }
    
    function register_hooks(){
        add_action('init', array($this, 'check_for_piklist'));
        add_action('wp_head', array($this, 'print_header'));
        add_filter('piklist_admin_pages', array($this, 'piklist_tools_setting_pages'));
    }
    
    function check_for_piklist()
    {
      if(is_admin())
      {
       include_once('class-piklist-checker.php');

       if (!piklist_checker::check(__FILE__))
       {
         return;
       }
      }
    }
    
    function piklist_tools_setting_pages($pages)
    {
        $pages[] = array(
            'page_title' => 'Tealium Settings' //__('Tealium Settings')
            ,'menu_title' => 'Tealium Settings'//__('Settings', 'piklist')
            ,'sub_menu' => 'tools.php' //Under Tools menu
            ,'capability' => 'manage_options'
            ,'menu_slug' => 'tealium_settings'
            ,'setting' => 'tealium_tool_settings'
            ,'menu_icon' => plugins_url('piklist/parts/img/piklist-icon.png')
            ,'page_icon' => plugins_url('piklist/parts/img/piklist-page-icon-32.png')
            ,'single_line' => true
            ,'default_tab' => 'Basic'
            ,'save_text' => 'Save Tealium Settings'
        );

      return $pages;
    }
    
    function get_page_types()
    {
        $obj = get_queried_object();
        $page_types = array('__generic');
        if(is_category()){
            $page_types[] = '__category';
        }elseif(is_search()){
            $page_types[] = '__search';
        }elseif($obj instanceof WP_Post){
            $page_types[] = $obj->post_type;
        }
        return $page_types;
    }
    function print_header(){

        $page_types = $this->get_page_types();
        $sections = array('static', 'site', 'taxonomy', 'post', 'calculated', 'meta');
        
        $obj = get_queried_object();
        $isPost = $obj instanceof WP_Post;
        if($isPost){
            $post_types[] = $obj->post_type;
        }
        
        $tealium_account = get_option('tealium_account', null);
        $tealium_profile = get_option('tealium_profile', null);
        $tealium_environment = get_option('tealium_environment', null);
        
        $option = get_option('tealium_tool_settings', array());
 
        $tealium_account = $option['tealium_account'];
        $tealium_profile = $option['tealium_profile'];
        $tealium_environment = $option['tealium_environment'];
        
        $udo = array();
        foreach($page_types as $ptype){
            foreach($sections as $s){
                switch($s){
                    case 'static':
                        $key = sprintf('tealium-%1$s-%2$s-settings', $ptype, 'static');
                        if(array_key_exists($key, $option)){
                            $settings = $option[$key];
                            $this->get_static_settings($settings, $udo);
                        }
                        break;
                    case 'site':
                        $key = sprintf('tealium-%1$s-%2$s-settings', $ptype, 'site');
                        if(array_key_exists($key, $option)){
                            $settings = $option[$key];
                            $this->get_site_settings($settings, $udo);
                        }
                        break;
                    case 'taxonomy':
                        $key = sprintf('tealium-%1$s-%2$s-settings', $ptype, 'taxonomy');
                        if($isPost && array_key_exists($key, $option)){
                            $settings = $option[$key];
                            $this->get_taxonomy_settings($settings, $udo, $obj);
                        }
                        break;
                    case 'post':
                        $key = sprintf('tealium-%1$s-%2$s-settings', $ptype, 'post');
                        if($isPost && array_key_exists($key, $option)){
                            $settings = $option[$key];
                            $this->get_post_settings($settings, $udo, $obj);
                        }
                        break;
                    case 'calculated':
                        $key = sprintf('tealium-%1$s-%2$s-settings', $ptype, 'calculated');
                        if($isPost && array_key_exists($key, $option)){
                            $settings = $option[$key];
                            $this->get_calculated_settings($settings, $udo, $obj);
                        }
                        break;
                    case 'meta':
                        $key = sprintf('tealium-%1$s-%2$s-settings', $ptype, 'meta');
                        if($isPost && array_key_exists($key, $option)){
                            $settings = $option[$key];
                            $this->get_meta_settings($settings, $udo, $obj);
                        }
                        break;
                }
            }
        }
        
        printf('<script type="text/javascript">var utag_data = %1$s; </script>', json_encode($udo));
        $script_text = <<<EOD
<script type="text/javascript">
    (function(a,b,c,d){
    a='//tags.tiqcdn.com/utag/%1\$s/%2\$s/%3\$s/utag.js';
    b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
    a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
    })();
</script>         
EOD;
        if($tealium_account && $tealium_profile && $tealium_environment){
            printf($script_text, $tealium_account, $tealium_profile, $tealium_environment);
        }
    }
    
    function get_static_settings(&$settings, &$udo){
        if($settings != null){
            for($i=0; $i < sizeof($settings['wp_variable']); $i++){
                if($settings['wp_variable'][$i] == null){break;}
                $udo[$settings['udo_variable'][$i]] = $settings['wp_variable'][$i];
            }
        }
    }
    
    function get_site_settings(&$settings, &$udo){
        if($settings != null){
            for($i=0; $i < sizeof($settings['wp_variable']); $i++){
                $attrib = $settings['wp_variable'][$i];
                $udo_var = $settings['udo_variable'][$i];
                if($udo_var == null){ break; }
                switch($attrib){
                    case 'site_title':
                        $udo[$udo_var] = get_bloginfo('name');
                        break;
                    case 'site_tagline':
                        $udo[$udo_var] = get_bloginfo('description');
                        break;
                    case 'site_language':
                        $udo[$udo_var] = get_bloginfo('language');
                        break;
                    case 'site_url':
                        $udo[$udo_var] = get_bloginfo('wpurl');
                        break;
                    default:
                        throw new Exception("Invalid attribute " . $attrib);
                }
            }
        }
    }

    function get_taxonomy_settings(&$settings, &$udo, &$obj){
        if($settings != null){
            for($i=0; $i < sizeof($settings['wp_variable']); $i++){
                $taxonomy = $settings['wp_variable'][$i];
                $udo_var = $settings['udo_variable'][$i];
                if($taxonomy == null){break;}
                $terms = wp_get_object_terms($obj->ID, $taxonomy);
                foreach(array_keys($terms) as $key){
                    $terms[$key] = $terms[$key]->slug;
                }
                $udo[$udo_var] = $terms;
            }
        }
    }

    function get_post_settings(&$settings, &$udo, &$obj){
        if($settings != null){
            for($i=0; $i < sizeof($settings['wp_variable']); $i++){
                $attrib = $settings['wp_variable'][$i];
                $udo_var = $settings['udo_variable'][$i];
                if($attrib == null){break;}
                
                $udo[$udo_var] = $obj->$attrib;
            }
        }    
    }

    function get_calculated_settings(&$settings, &$udo, &$obj){
        if($settings != null){
            for($i=0; $i < sizeof($settings['wp_variable']); $i++){
                $attrib = $settings['wp_variable'][$i];
                $udo_var = $settings['udo_variable'][$i];
                if($attrib == null){break;}
                
                switch($attrib){
                    case 'title':
                        $udo[$udo_var] = wp_title();
                        break;
                    case 'excerpt':
                        $udo[$udo_var] = self::get_excerpt($obj);
                        break;
                }
            }
        }
    }
    
    function get_meta_settings(&$settings, &$udo, &$obj){
        if($settings != null){
            for($i=0; $i < sizeof($settings['wp_variable']); $i++){
                $attrib = $settings['wp_variable'][$i];
                $udo_var = $settings['udo_variable'][$i];
                if($attrib == null){break;}
                
                $udo[$udo_var] = get_post_meta($obj->ID, $attrib);
            }
        }           
    }
    
//    static function get_excerpt(&$obj){
//        global $post;  
//        $save_post = $post;
//        $post = $obj;
//        $output = get_the_excerpt();
//        $post = $save_post;
//        return $output;
//    }
}

$tealium_plugin = new Tealium_Plugin();





<?php
/*
 * Template Name: Media Centre Listing Page
 */
require('header.php');

?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
        $currentPostPageID = $post->ID;

        // Get the hero image
        $heroImageUrl = (has_post_thumbnail()) ? omGetPostThumbnailUrl(get_post_thumbnail_id($post->ID), null) : omGetPostThumbnailUrl(getFallbackHeroImage(), null);
    ?>

<div class="page-hero" style="<?php echo 'background-image:url(\''. $heroImageUrl .'\');'?>">
    <div class="container">
        <h1>
            <?php the_title(); ?>
        </h1>
    </div>
</div>

<div class="section-global--color-6">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php if (function_exists('om_breadcrumb_lists')) { om_breadcrumb_lists(); } ?>
            </div>
        </div>
    </div>
</div>

<div class="" role="main">
    <div class="section-global section-global--color-6 page-intro search-form-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                <?php the_content() ?>

            <?php endwhile;

            else:

                // If no content, include the "No posts found" template.
                get_template_part('content', 'none');

            endif;
            ?>

                </div><!-- /.col-sm-12 -->
            </div><!-- /.row -->

            <?php // SECONDARY LOOP FOR MEDIA CENTRE POSTS ?>
            <?php $allMediaCentrePosts = getAllMediaCentrePosts(); ?>
            <?php $mediaCentrePosts = omGetMediaCentrePosts(); ?>
            <?php $postsPerPage = omGetMediaCentrePostsPerPage(); ?>
            <?php $currentPage = omGetCurrentPage(); ?>
            <?php $selectedYear = omGetMediaCentreYear(); ?>
            <?php $allMediaCentrePosts = omGetAvailableMediaCentreTypeAndYears($allMediaCentrePosts); ?>
            <?php $years = $allMediaCentrePosts['years']; ?>

            <div class="row">
                <div class="col-sm-12">
                    <form action="/<?php echo $wp->query_vars['pagename']; ?>/" method="GET" class="row search-form">
                        <div class="col-md-2 search-item">
                            <label class="keywordLabel" for="keywordInput">search news by</label>
                        </div>
                        <div class="col-md-4 search-item">
                            <input type="text" id="keywordInput" name="keywords" placeholder="Keywords" value="<?php echo sanitize_text_field($wp->query_vars['keywords']); ?>">
                        </div>
                        <div class="col-md-3 search-item">
                            <?php omGetMediaCentreCategoriesSelect(); ?>
                        </div>

                        <?php if(count($years)) :?>
                            <div class="col-md-3 search-item">
                                <select name="mcyear" class="year-drop-down">
                                    <option value="">Choose year</option>
                                    <?php foreach($years as $year => $value) : ?>

                                        <?php if($year == $selectedYear) :?>
                                            <option selected="selected" value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                        <?php else: ?>
                                            <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>

                                <button type="submit" class="btn btn-default">Go</button>
                            </div>
                        <?php endif; ?>
                    </form>
                </div>
            </div>
        </div><!-- /.container -->
    </div><!-- /.page-intro -->

    <div class="section-global search-list">

        <div class="container">
            <div class="row">
                <?php // SET THE LOOP START POINTS ?>
                <?php $startLoop = (!$currentPage ||  $currentPage == 1) ? 0 : ($currentPage * $postsPerPage) - $postsPerPage; ?>

                <div class="col-lg-9">

                    <?php $keywords = sanitize_text_field($wp->query_vars['keywords']); ?>
                    <?php $newstype = sanitize_text_field($wp->query_vars['newstype']); ?>
                    <?php $year = sanitize_text_field($wp->query_vars['mcyear']); ?>
                    <?php $mediaCentreLength  = count($mediaCentrePosts) ?>
                    <?php $resultsCopy = ($mediaCentreLength) ? 'Your search has ' . $mediaCentreLength . ' results' : 'Sorry, your search has 0 results'; ?>

                    <div class="search-tagline">
                        <p><?php echo $resultsCopy ?></p>

                        <?php if($keywords || $year || $newstype) : ?>
                            <p>You searched for <?php echo omGetMediaCentreSearchParameterList($keywords, $newstype, $year); ?></p>
                        <?php endif; ?>
                    </div>

                    <?php for($i = $startLoop; $i < $mediaCentreLength && ($i < $startLoop + $postsPerPage); $i++): ?>

                    <?php
                        $mediaCentrePostID = $mediaCentrePosts[$i]->ID;
                        $media_center_homepage_image = (get_post_meta($mediaCentrePostID, 'homepage_grid_tile_mobile_and_listing', true));
                        $media_centre_excerpt = (get_post_meta($mediaCentrePostID, 'media_centre_excerpt', true));
                        $media_centre_asset_url = omGetPostThumbnailUrl($media_center_homepage_image['ID'], null);
                        $media_centre_post_date = mysql2date('j F Y', $mediaCentrePosts[$i]->post_date);
                        $media_centre_link = get_permalink($mediaCentrePostID);
                        ?>

                    <div class="search-result-item">
                        <div class="row">
                            <div class="col-sm-4 media-image">
                                <a href="<?php echo $media_centre_link ?>">
                                    <img src="<?php echo $media_centre_asset_url ?>" alt="<?php echo $mediaCentrePosts[$i]->post_title; ?>" />
                                </a>
                            </div>
                            <div class="col-sm-8 search-text--wrapper">
                                <h3><?php echo $mediaCentrePosts[$i]->post_title; ?></h3>
                                <p class="date"><?php echo $media_centre_post_date; ?></p>
                                <p><?php echo $media_centre_excerpt; ?> </p>
                                <a href="<?php echo $media_centre_link; ?>" class="global-link">Read more</a>
                            </div>
                        </div>
                    </div>

                    <?php endfor; ?>

                    <?php omCustomLoopPagination($mediaCentrePosts, $postsPerPage); ?>

                </div><!-- /.col-lg-9 -->

                <div class="col-lg-3">
                    <?php omGetSidbarCrossLinkTiles($currentPostPageID); ?>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>
</div><!-- .main-content -->

<?php omGetBottomCrossLinkTiles($currentPostPageID); ?>

<?php require('footer.php'); ?>
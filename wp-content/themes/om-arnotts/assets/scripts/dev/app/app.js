/*global define, Modernizr */
define([
    'responsiveCarousel',
    'productsOverviewPage',
    'productsDetailPage',
    'uberMenu',
    'addThis',
    'announcement',
    'feedbackForm'
], function (ResponsiveCarousel, ProductsOverviewPage, ProductsDetailPage, Ubermenu, addthis, announcement, feedbackForm) {
    'use strict';

    function OMApp() {

        var init = function init() {
            var $productsOverview = $('.js-section-products-overview'),
                $productsTabView = $('.js-section-products-view-tabs'),
                $productsAccordionView = $('.js-section-view-accordion'),
                ProductsOverviewPageCon,
                ProductsDetailPageCon;

            if($productsOverview.length) {
                ProductsOverviewPageCon = new ProductsOverviewPage($productsOverview);
            }

            if($productsTabView.length || $productsAccordionView.length) {
                ProductsDetailPageCon = new ProductsDetailPage($productsTabView, $productsAccordionView);
            }

            bindEvents();
        };

        /**
         * Bind event listeners for the fundraiser wheel
         */
        var bindEvents = function bindEvents() {

        };

        init();
    }

    return new OMApp();
});

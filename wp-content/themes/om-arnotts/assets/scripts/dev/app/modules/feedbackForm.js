/*global define, Modernizr */
define([
    'jquery',
    'recaptcha'
], function ($, recaptcha) {
    'use strict';
    function FeedbackForm() {
        var self = this;
        var init = function init() {
            bindEvents();
        };

        /**
        *
        * Toggle form on dropdown change
        *
        */
        var toggleForm = function toggleForm(e) {
            var $elementValue = $(e.currentTarget).val();

            if($elementValue == "complaints") {
                $('.js-feedback-form-content').addClass('complaints');
            }else{
                $('.js-feedback-form-content').removeClass('complaints');
            }
        };

        /**
        *
        * Toggle state on dropdown change
        *
        */
        var toggleState = function toggleState(e) {
            var $elementValue = $(e.currentTarget).val();
            var $stateField = $('.js-state-dropdown');

            if($elementValue == "New Zealand") {
                $stateField.hide();
            }else{
                $stateField.show();
            }
        };

        /**
        *
        * Form validation
        *
        */
        var validateSubmission = function validateSubmission(e) {
            var form_ok = true;
            var simple_email_regex = /^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
            var no_numbers_regex = /^\d+$/;
            var no_characters_regex = /^[0-9 ]*(?:\.\d{1,2})?$/;
            var recaptchaElement = $('.g-recaptcha');
            var matchDate =/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
            var email_ok = simple_email_regex.test($('.emailInput').val());

            // alert('test ' + email_ok);

            if(!email_ok && $('.emailInput').val().length > 0) {
                $('.emailInput').parent().addClass('invalid-input-wrapper');
                form_ok = false;
            }else{
                $('.emailInput').parent().removeClass('invalid-input-wrapper');
            }

            //Postcode length
            if($('.postcodeInput').val().length < 4 && $('.postcodeInput').val().length > 0) {
                $('.postcodeInput').parent().addClass('error-input-wrapper');
                form_ok = false;
            }else{
                $('.postcodeInput').parent().removeClass('error-input-wrapper');
            }

            //Telephone length
            if($('.telephoneInput').val().length > 20 && $('.telephoneInput').val().length !== 0 && $('.js-toggle-form').val() === "complaints") {
                $('.telephoneInput').parent().addClass('invalid-input-wrapper');
                form_ok = false;
            }else{
                $('.telephoneInput').parent().removeClass('invalid-input-wrapper');
            }

            //Best before structure
            if(!matchDate.test($('.bestBeforeDropdown').val()) && $('.bestBeforeDropdown').val().length > 0 && $('.js-toggle-form').val() === "complaints") {
                $('.bestBeforeDropdown').parent().addClass('error-date-input-wrapper');
                form_ok = false;
            }else if($('.bestBeforeDropdown').val().length > 0){
                var dateString = $('.bestBeforeDropdown').val();
                var dayString = dateString.substr(0, 2);
                var monthString = dateString.substr(3, 2);
                var yearString = dateString.substr(6, 4);

                var date = new Date(yearString,monthString-1,dayString);
                if (date.getFullYear() == yearString && date.getMonth()+1 == monthString && date.getDate() == dayString) {
                    $('.bestBeforeDropdown').parent().removeClass('error-date-input-wrapper');
                } else {
                    $('.bestBeforeDropdown').parent().addClass('error-date-input-wrapper');
                }
                $('.bestBeforeDropdown').parent().removeClass('error-input-wrapper');

            }else{
                $('.bestBeforeDropdown').parent().removeClass('error-input-wrapper');
            }

            if($('.js-toggle-form').val() === "generalFeedback") {
                var required_fields = $('.js-field-required-global');
                var no_numbers = $('.js-no-numbers');
                var no_characters = $('.js-no-characters');
            }else{
                var required_fields = $('.js-field-required-global, .js-field-required-complaints');
                var no_numbers = $('.js-no-numbers-complaints, .js-no-numbers');
                var no_characters = $('.js-no-characters-complaints, .js-no-characters');
            }

            required_fields.each(function() {
                var child = $(this).children('.js-check-field');
                if(!child.val()) {
                    $(this).addClass('error-input-wrapper');
                    form_ok = false;
                }else{
                    $(this).removeClass('error-input-wrapper');
                }
            });

            // Check comment box and add error message depending on complaint or feedback
            var getTextAreaParent = $(".js-check-field-comment-box").parent();
            if($(".js-check-field-comment-box").val() === '') {
                if($('.js-toggle-form').val() === "generalFeedback") {
                    getTextAreaParent.addClass('error-input-wrapper-feedback');
                    getTextAreaParent.removeClass('error-input-wrapper-complaint');
                    form_ok = false;
                }else if($('.js-toggle-form').val() === "complaints"){
                    getTextAreaParent.addClass('error-input-wrapper-complaint');
                    getTextAreaParent.removeClass('error-input-wrapper-feedback');
                    form_ok = false;
                }else{
                    getTextAreaParent.removeClass('error-input-wrapper-complaint');
                    getTextAreaParent.removeClass('error-input-wrapper-feedback');
                }
            }else{
                getTextAreaParent.removeClass('error-input-wrapper-complaint');
                getTextAreaParent.removeClass('error-input-wrapper-feedback');
            }

            no_numbers.each(function() {
                var child = $(this).children('.js-check-field');
                if(no_numbers_regex.test(child.val())) {
                    $(this).addClass('error-nonumber-wrapper');
                    form_ok = false;
                }else{
                    $(this).removeClass('error-nonumber-wrapper');
                }
            });

            no_characters.each(function() {
                var child = $(this).children('.js-check-field');
                if(!no_characters_regex.test(child.val())) {
                    $(this).addClass('error-nocharacter-wrapper');
                    form_ok = false;
                }else{
                    $(this).removeClass('error-nocharacter-wrapper');
                }
            });

            if(!$('.js-checkbox-field').is(':checked')) {
                $('.js-checkbox-field').parent().addClass('error-input-wrapper');
                form_ok = false;
            }else{
                $('.js-checkbox-field').parent().removeClass('error-input-wrapper');
            }

            if(form_ok) {
                return true;
            }

            $('html, body').animate({scrollTop: $(".js-feedback-form-content").offset().top-20}, 600);
            $('.global-form-error-message').fadeIn();
            return false;
        }

        /**
         * Bind event listeners for the FeedbackForm pop up
         */
        var bindEvents = function bindEvents() {
            $('.js-toggle-form').on('change', toggleForm);
            $('.js-country-dropdown').on('change', toggleState);
            $('.js-feedback-form').on('submit', validateSubmission);
        };

        init();
    }
    return new FeedbackForm;
});

/*global define, Modernizr */
define([
    'jquery',
    'utilities'
], function ($, Utilities) {
    'use strict';

    function ProductsDetailPage($productsTabView, $productsAccordionView) {

        var $tabsView = $productsTabView,
            $accordionView = $productsAccordionView,
            $selectedAccordionPanel = '',
            selectedAccordionPanelID = '',
            $selectedTabPanel = '',
            selectedTabPanelID = '';

        var init = function init() {
            bindEvents();
        };

        /**
         * Function that takes the ID of the selected tab and makes sure to activate the corresponding
         * accordion panel.
         *
         * @param activeTabID { String } The ID of the previously selected tab panel
         */
        var activateCorrespondingAccordionPanel = function activateCorrespondingAccordionPanel(activeTabID) {

            $selectedAccordionPanel = $('[href="' + activeTabID + '-accordion"]');

            if($selectedAccordionPanel.length) {
                $selectedAccordionPanel.click();
            }
        };

        /**
         * Function that takes the ID of the selected accordion item and makes sure to activate the corresponding
         * tab panel.
         *
         * @param activeAccordionID { String } the id of the previously selected accordion panel.
         */
        var activateCorrespondingTabPanel = function activateCorrespondingTabPanel(activeAccordionID) {

            var tabID = '',
                $activateThisTab;

            // Convert the accordion id to the corresponding tab id by removing the strng-accordion
            if(activeAccordionID.length) {
                tabID = activeAccordionID.replace('-accordion', '');
                $activateThisTab = $('[href="' + tabID + '"]');

                if($activateThisTab.length) {
                    $activateThisTab.click();
                }
            }
        };


        /**
         * Once the user switches to Mobile the page should scroll to the selected accordion tab if there
         * is one selected. On initial load on mobile there won't be anything selected.
         */
        var switchedToMobile = function switchedToMobile() {

            if($selectedAccordionPanel.length) {

                var $panelHeading = $selectedAccordionPanel.parents('.panel-heading');

                $('html, body').animate({
                    scrollTop: $panelHeading.offset().top
                }, 1000);
            }
        };


        /**
         * Bind event listeners for the fundraiser wheel
         */
        var bindEvents = function bindEvents() {

            // Actions once the user switches to mobile
            Utilities.$body.on('changeFromDesktopToMobile', switchedToMobile);

            // ON MOBILE ( ACCORDION )
            $accordionView.find('.panel').on('shown.bs.collapse',function (e) {

                // Only act on mobile
                if(Utilities.isMobileView()) {
                    // Get the ID of the currently selected accordion panel to set the active state on the corresponding
                    // desktop tab
                    $selectedAccordionPanel = $(e.currentTarget).find('a[data-toggle="collapse"]');
                    selectedAccordionPanelID = $selectedAccordionPanel.attr('href');

                    var $panelHeading = $selectedAccordionPanel.parents('.panel-heading');

                    // Make sure to activate the corresponding tab view
                    activateCorrespondingTabPanel(selectedAccordionPanelID);

                    if(Utilities.isMobileView()) {
                        $('html, body').animate({
                            scrollTop: $panelHeading.offset().top
                        }, 1000);
                    }
                }
            });

            // ON DESKTOP ( TABS )
            $tabsView.on('shown.bs.tab','a[data-toggle="tab"]',function (e) {

                if(!Utilities.isMobileView()) {
                    $selectedTabPanel = $(e.currentTarget);
                    selectedTabPanelID = $selectedTabPanel.attr('href');

                    // Make sure to activate the corresponding accordion view
                    activateCorrespondingAccordionPanel(selectedTabPanelID);
                }
            });
        };

        init();
    }

    return ProductsDetailPage;
});


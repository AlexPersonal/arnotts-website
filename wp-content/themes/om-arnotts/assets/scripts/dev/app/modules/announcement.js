/*global define, Modernizr */
define([
    'jquery'
], function ($, Utilities) {
    'use strict';
    function Announcement() {
        var self = this;
        var init = function init() {
            bindEvents();
        };

        /**
        *
        * Close announcement banner
        * 
        */
        var animateAnnouncemnt = function animateAnnouncemnt(e) {
            $element = $(e.currentTarget).parents('.announcement--wrapper');
            $element.toggleClass('open');
            $element.toggleClass('close');
            e.preventDefault();

        };

        /**
        *
        * Check for cookie
        * 
        */    
        var getCookie = function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
            }
            return "";
        }

        /**
         * Bind event listeners for the announcement pop up
         */
        var bindEvents = function bindEvents() {
            $('.js-close-announcement').on('click', animateAnnouncemnt);
            

            if(!getCookie('announcement')) {
                document.cookie="announcement=seen";
                setTimeout(function(){ 
                    $('.announcement--wrapper').addClass('open');
                }, 1000);
            }
        };

        init();
    }

    return new Announcement;
});
define(['jquery'],
    function ($) {

        jQuery(".js-view-more-products").click(function (e) {
            e.preventDefault();



            var $viewMoreAnchor = jQuery(e.currentTarget),
                // Products will be fetched only once
                areProductsFetched = false;

            // Prevent double clicks.
            $viewMoreAnchor.prop('disabled', true);

            // Only get further products if they haven't been requested before.
            if (!$viewMoreAnchor.data('areProductsFetched')) {

                var nonce = $viewMoreAnchor.attr("data-nonce"),
                    productCategoryID = $viewMoreAnchor.attr("data-product-category-id");

                $.ajax({
                    type: 'post',
                    dataType: "json",
                    url: ViewAllProducts.ajaxurl,
                    data: {
                        action: 'view-more-products',
                        productCategoryID: productCategoryID,
                        nonce: ViewAllProducts.nonce
                    }
                }).done(function (data) {
                    //console.log('done: ', data);

                    if (data.success) {

                        // Add elements
                        var productsLength = data.products.length,
                            $viewMoreButtonLi = $viewMoreAnchor.parent('li'),
                            newListElements = '';

                        if (productsLength) {

                            for (var i = 0; i < productsLength; i++) {

                                newListElements += '<li>';
                                newListElements += '<a href="' + data.products[i]['post_permalink'] + '" class="tile tile--style-3">';
                                newListElements += '<div class="image--container">';

                                if (data.products[i]['post_thumb']) {
                                    newListElements += data.products[i]['post_thumb'];
                                }

                                newListElements += '</div>';
                                newListElements += '<div class="tile--footer">';
                                newListElements += '<p>' + data.products[i]['post_title'] + '</p>';
                                newListElements += '</div>';
                                newListElements += '</a>';

                                newListElements += '</li>';
                            }
                        }


                        $(newListElements).insertBefore($viewMoreButtonLi).hide().fadeIn();

                        // Update the button copy
                        toggleViewMoreButton($viewMoreAnchor, false);

                        $viewMoreAnchor.attr('data-are-products-fetched', 'true');

                        // Enable the view more button again
                        $viewMoreAnchor.prop('disabled', false);


                    } else {
                        console.error('The request was not successful.');
                    }

                }).fail(function (jqXHR, textStatus) {
                    console.error('request failed: ', arguments);

                    // Enable the view more button again
                    $viewMoreAnchor.prop('disabled', false);
                });



            // All products for this section had been fetched therefore now hide all products
            // Except the first 4 and toggle the button class from 'View less' to 'View more'
            } else {

                toggleViewMoreButton($viewMoreAnchor, true);

            }
        });

        /**
         * Toggles the rendering of the 'View More' button to 'View Less'
         * @param $viewMoreAnchor { Jquery } the 'View more / View less' button (DOM element) that was clicked on
         * @param showAllExcept { Number } the number of items that you want to keep visible (used for view less). Starting at the index of 0.
         */
        var toggleViewMoreButton = function toggleViewMoreButton($viewMoreAnchor, showAllExcept) {

            // How many products should stay visible starting at 0
            var numberOfVisibleProducts = 3;

            // View less
            if($viewMoreAnchor.hasClass('global-link--minus')) {
                $viewMoreAnchor.removeClass('global-link--minus');
                $viewMoreAnchor.addClass('global-link--plus');

                if(showAllExcept) {
                    $viewMoreAnchor.parents('li').siblings('li:gt(' + numberOfVisibleProducts + ')').fadeOut();
                }

                // View more
            } else {
                $viewMoreAnchor.removeClass('global-link--plus');
                $viewMoreAnchor.addClass('global-link--minus');

                if(showAllExcept) {
                    $viewMoreAnchor.parents('li').siblings('li:gt(' + numberOfVisibleProducts + ')').fadeIn();
                }
            }

            // Enable the view more button again
            $viewMoreAnchor.prop('disabled', false);
        }
    });
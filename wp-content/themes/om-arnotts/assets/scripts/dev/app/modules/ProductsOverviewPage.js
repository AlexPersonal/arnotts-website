/*global define, Modernizr */
define([
    'jquery',
    'utilities',
    'viewMoreProducts'
], function ($, Utilities) {
    'use strict';

    function ProductsOverviewPage($productsOverview) {

        var currentClickedTabID,
            prevClickedTabID;

        var init = function init() {
            bindEvents();
        };

        /**
         * This hides all accordion panels on mobile on the products overview page. This
         * is being used for mobile.
         */
        var hideAllOnProductsOverview = function hideAllOnProductsOverview() {

            if (Utilities.isMobileView()) {

                // PRODUCTS OVERVIEW and ON MOBILE
                $('.panel-collapse.in').collapse('hide');

                $productsOverview.one('show.bs.collapse', function (e) {


                    // Make sure all other panels close (only used on mobile)
                    $('.panel-collapse.in').collapse('hide');

                    var $panels = $('.panel-collapse');

                    $panels.each(function (i, el) {

                        var elID = '#' + $(el).attr('id');

                        if (currentClickedTabID != elID) {
                            var anchor = $('[href="' + elID + '"]');
                            anchor.addClass('collapsed');
                        } else {

                        }
                    });
                });

               setTimeout(function() {

                if(currentClickedTabID) {

                    $(currentClickedTabID).collapse('show');

                    var $clickedAccordionHeading = $('[href="'+currentClickedTabID+'"]');

                    $clickedAccordionHeading.removeClass('collapsed');

                }
               },350);

            }
        };

        /**
         * This shows all products overview panels. This is being used on the desktop view only.
         */
        var showAllOnProductsOverview = function showAllOnProductsOverview() {

            // SHOW ALL ON DESKTOP
            $('.panel-collapse').collapse('show');

        };

        /**
         * Once the user switches to Mobile the page should scroll to the selected accordion tab if there
         * is one selected. On initial load on mobile there won't be anything selected.
         */
        var switchedToMobile = function switchedToMobile() {
            hideAllOnProductsOverview();
        };

        /**
         * Once the user switches back to desktop the page should scroll to the tabs
         */
        var switchedToDesktop = function switchedToDesktop() {
            showAllOnProductsOverview();
        };

        /**
         * Bind event listeners for the fundraiser wheel
         */
        var bindEvents = function bindEvents() {

            // Actions once the user switches to mobile
            Utilities.$body.on('changeFromDesktopToMobile', switchedToMobile);
            Utilities.$body.on('changeFromMobileToDesktop', switchedToDesktop);


            // PRODUCTS OVERVIEW INIT
            if(!Utilities.isMobileView()) {
                showAllOnProductsOverview();

                // ONLY BEING USED ON DESKTOP
                $('.js-scroll-to-section a').on('click', function() {
                    Utilities.scrollToElement($($(this).attr('href')), -40);
                });

            } else {

                hideAllOnProductsOverview();
            }

            // Because of the desktop / mobile toggle we need to
            // keep track of the previously and currently clicked element as
            // the bootstrap callbacks show.bs.collapse, shown.bs.collapse and hidden.bs.collapse
            // don't return the currently clicked element.
            $('.panel-heading').on('click','a' ,function(e) {

                var $clickedPanelHeading = $(e.currentTarget);

                // Save the previous tab to close that one only if a 2nd panel has
                // been opened
                if(currentClickedTabID) {
                    prevClickedTabID = currentClickedTabID;
                }

                // Only save the current clicked item if it was opened
                currentClickedTabID = ($clickedPanelHeading.hasClass('collapsed')) ? $clickedPanelHeading.attr('href') : '';

                if(prevClickedTabID) {
                    $(prevClickedTabID).collapse('hide');
                    $('[href="'+prevClickedTabID+'"]').addClass('collapsed');
                }
            });


            $productsOverview.on('shown.bs.collapse', function (e) {

                // On mobile: Scroll to the opened element
                if(Utilities.isMobileView()) {
                    Utilities.scrollToElement($('[href="' + currentClickedTabID + '"]'), -30 ,400);

                }
            });
        };

        init();
    }

    return ProductsOverviewPage;
});


define([
    'jquery',
    'utilities',
    'slick'
], function ($, Utilities) {
	"use strict";

	$(function(){
		var carouselConfig;

		[].forEach.call(document.querySelectorAll(".js-responsive-carousel"), function(element){

			// this shouldn't happen but, in case...
			if(!element) {
				return console.error("this shouldn't happen, but an error with a DOM element with the class responsive assigned to it.");
			}

			if(element.className.indexOf("js-hero-carousel") > -1) {

                _setViewportHeight(element);

				carouselConfig = _getConfigs("js-hero-carousel");

			} else if(element.className.indexOf("js-feature-carousel") > -1) {

				carouselConfig = _getConfigs("js-feature-carousel");
			} else if(element.className.indexOf("js-news-carousel") > -1) {

				carouselConfig = _getConfigs("js-news-carousel");
			} else {

				return console.error("carousel is missing a data-attribute or a class specifying what type of carousel you're trying to instantiate!");
			}


			$(element).slick(carouselConfig);
		});
	});

	/**
	 * Returns the carousels configs.
	 *
	 * @param type string determining the type of config to return
	 * @returns Object POJO for the responsive carousel's settings
	 * @private
	 */
	function _getConfigs(type) {
		if(type == "js-hero-carousel") {

			return {
                slidesToShow: 1,
                autoplay:true,
                fade:true,
                speed:1200,
                autoplayspeed: 2000,
                arrows: false,
                dots: false
			};
		} else if(type == "js-feature-carousel") {

			return {
				slidesToShow: 1,
                autoplay:false,
                fade:true,
                speed:1200,
                autoplayspeed: 2000,
    			arrows: true,
				dots: true
			};
		}
		else if(type == "js-news-carousel") {

			return {
				slidesToShow: 1,
                autoplay:false,
                fade:true,
                speed:1200,
                autoplayspeed: 2000,
    			arrows: true,
				dots: false
			};
		}
	}

    function _setViewportHeight(element) {

        var windowHeight;

        // ARNOTTS-354: fix for Chrome on ipad only
        if(Utilities.isMobileView() && (navigator.userAgent.match('CriOS'))) {

            windowHeight = window.innerHeight;
        } else {

            windowHeight = $(window).height()
        }

        $(element).height(windowHeight);
    }

    $(window).on("orientationchange",function() {
	  	setTimeout(function(){ _setViewportHeight($('.js-hero-carousel')); }, 500);
	});

	$(".js-scroll-section").on( "click", function() {
		Utilities.scrollToElement($('.section-whats-new-at-arnotts'), null, null);
	});

});


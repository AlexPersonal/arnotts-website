/*global define */
define([
    'jquery'
], function ($) {
    'use strict';

    /**
     * This object contains global functions that can be used across the whole application
     * (given that the Utilities object has been integrated)
     * @constructor
     */
    function Utilities() {

        var self = this,
            currentView,
            previousView;

        this.$body = $('body');

        /**
         * Event that fires an 'isMobile' event on mobile and 'isDesktop' custom
         * event.
         */
        this.fireEventOnViewChange = function fireEventOnViewChange() {


            // Defining 3 events 'changeFromDesktopToMobile', 'changeFromMobileToDesktop', 'isMobile' and 'isDesktop'
            // The difference is the frequency they are fired. changeFrom only fire if there is a breakpoint switch from desktop
            // to mobile. isMobile will continuously fire.

            // Edge case: you are on mobile and need to scroll to an element on orientation change then you would use isMobile
            // If you need 1 function only to trigger when you get from desktop to mobile you would register changeFromDesktopToMobile.
            $(window).on('resize', function() {

                if(self.isMobileView()) {

                    currentView = 'mobile';

                    if(!currentView) {
                        self.$body.trigger("changeFromDesktopToMobile");

                    } else if(previousView != currentView) {
                        self.$body.trigger("changeFromDesktopToMobile");
                    }

                    self.$body.trigger("isMobile");

                } else {

                    currentView = 'desktop';

                    if(!currentView) {
                        self.$body.trigger("changeFromMobileToDesktop");

                    } else if(previousView != currentView) {
                        currentView = 'desktop';

                        self.$body.trigger("changeFromMobileToDesktop");
                    }


                    self.$body.trigger("isDesktop");
                }

                previousView = currentView;
            })
        };

        /**
         * Register a custom event that triggers on mobile view
         * @returns {boolean}
         */
        this.isMobileView = function isMobileView() {
            return $(window).width() < 900;
        };


        /**
         * GA analytics 3.0
         * Documentation: https://developers.google.com/analytics/devguides/collection/analyticsjs/pages
         */
        this.omTrackPage = function omTrackPage() {
            try {
                if(ga && ga != 'undefined') {
                    ga('send', 'pageview');
                }
            } catch(e) {
                console.log('GA only setup on live. Please add further code in /wp-content/themes/om-dashed-design/header.php for testing')
            }
        };

        /**
         * GA analytics 3.0
         * Documentation: https://developers.google.com/analytics/devguides/collection/analyticsjs/advanced
         * Documentation: https://developers.google.com/analytics/devguides/collection/analyticsjs/events
         *
         * @param category
         * @param action
         * @param label
         * @param page
         */
        this.omTrackEvent = function omTrackEvent(category, action, label, page) {

            try {
                if(ga) {
                    if (ga) {
                        if (category && action && label && page) {
                            ga('send', 'event', category, action, label, {'page': page});

                        } else if (category && action && label && !page) {
                            ga('send', 'event', category, action, label);

                        } else if (category && page && !action) {
                            ga('send', 'event', category, null, {'page': page});

                        } else if (category && action && !page && !label) {
                            ga('send', 'event', category, action);

                        } else if (category && page && action) {
                            ga('send', 'event', category, action, {'page': page});
                        }
                    }
                }
            } catch(e) {
                console.log('GA only setup on live. Please add further code in /wp-content/themes/om-dashed-design/header.php for testing')
            }
        };

        /**
         * Copied this over from the 'Common.js' file to save another request just to use a single function.
         * This is used to especially for IE8 to support HTML5's placeholders.
         */
        var omPlaceholders = function omPlaceholders() {
            /*
             * Adapted from http://www.beyondstandards.com/archives/input-placeholders/
             */
            var supported = !!("placeholder" in document.createElement("input"));
            if (supported) return;
            $("input[placeholder][name!='username'][type!='password']").each(function () {
                var placeholder = $(this).attr('placeholder');
                if ($(this).val() == '') $(this).addClass('placeholder').val(placeholder);
                $(this).focus(function () {
                    if ($(this).val() == placeholder) $(this).removeClass('placeholder').val('');
                    return false;
                }).blur(function () {
                    if ($(this).val() == '') $(this).addClass('placeholder').val(placeholder);
                });
            });
            $('#aspnetForm').submit(function () {
                $('input[placeholder]').each(function () {
                    var placeholder = $(this).attr('placeholder');
                    if ($(this).val() == placeholder) $(this).val('');
                });
            });
        };

        /**
         * Scrolls to a given element on the page. This function is configured to take a custom correction pixel distance
         * and speed.
         *
         * @param: $jqueryObject { jQuery } the jquery object you want to scroll to on the page
         * @param: correctionPixel { Number } The number of pixels that you need for scroll correction.
         * @param: speed { Number } The speed that the animation should take place with in ms. If not set it'll fall back to 400ms.
         */
        this.scrollToElement = function scrollToElement($jqueryObject, correctionInPixels, speed) {

            if($jqueryObject.length) {

                // Initialize values
                correctionInPixels = (correctionInPixels) ? correctionInPixels : 0;
                speed = (speed) ? speed : 400;

                $('html, body').animate({
                    scrollTop: $jqueryObject.offset().top + correctionInPixels
                }, speed);
            }
        };

        // Register events to listen to window size changes.
        self.fireEventOnViewChange();
    }


    return new Utilities;
});

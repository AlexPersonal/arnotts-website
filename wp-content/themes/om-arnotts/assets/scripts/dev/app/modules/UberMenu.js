/*global define, Modernizr */
define([
    'jquery',
    'utilities'
], function ($, Utilities) {
    'use strict';

    function UberMenu() {

        var init = function init() {
            // alert('running');
            bindEvents();
        };

        /**
         * Registered handler that fires when view is mobile.
         */
        var clickMobileMenu = function clickMobileMenu(e) {
            console.log('click on mobile');
            var $menuButton = $(e.currentTarget),
                $menuButtonChildren = $menuButton.find('.fa');

            $menuButton.toggleClass('menuOpen');
            $menuButtonChildren.toggleClass('fa-bars');
            $menuButtonChildren.toggleClass('icon-close');

        };

        /**
         * Bind event listeners for the fundraiser wheel
         */
        var bindEvents = function bindEvents() {
            // Register event on mobile menu click
           Utilities.$body.on("touchend click", ".ubermenu-responsive-toggle", clickMobileMenu);
        };

        init();
    }

    return new UberMenu();
});

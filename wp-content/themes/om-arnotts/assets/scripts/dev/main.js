/*global require, requirejs, define */
requirejs.config({
    baseURL: 'scripts/dev',
    paths: {
        'jquery'                   : 'vendor/libs/jquery',
        'bootstrap-tabs'           : 'vendor/bootstrap/tab',
        'bootstrap-collapse'       : 'vendor/bootstrap/collapse',
        'bootstrap-dropdown'       : 'vendor/bootstrap/dropdown',
        'bootstrap-transition'     : 'vendor/bootstrap/transition',
        'slick'			           : 'plugins/slick',
        'addThis'                  : 'plugins/addthis',
        'recaptcha'			       : 'plugins/recaptcha',
        'responsiveCarousel'	   : 'app/modules/responsiveCarousel',
        'productsOverviewPage'     : 'app/modules/ProductsOverviewPage',
        'productsDetailPage'       : 'app/modules/ProductsDetailPage',
        'viewMoreProducts'         : 'app/modules/viewMoreProducts',
        'utilities'                : 'app/modules/Utilities',
        'uberMenu'                 : 'app/modules/UberMenu',
        'announcement'             : 'app/modules/announcement',
        'feedbackForm'             : 'app/modules/feedbackForm'
    },

    shim: {
        // By adding the Dependencies we don't need a reference to here,
        // we avoid having a long deps list in the module itself.
        // Perfect for declaring jQuery Plugins as dependencies without
        // fluffing out the module declaration.
        'bootstrap-tabs':           ['jquery'],
        'app/app': {
            deps: [
                'jquery',
                'bootstrap-tabs',
                'bootstrap-collapse',
                'bootstrap-dropdown',
                'bootstrap-transition'
            ]
        }
    },
    config : {

    },
    catchError: true
});

requirejs.onError = function(err){
    'use strict';
    console.error('Error:', err);
};

require([
    'jquery',
    'app/app'
], function () {
    'use strict';
});

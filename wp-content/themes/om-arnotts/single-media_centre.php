<?php
/**
 * The default media centre single template template
 *
 * @package WordPress
 * @since omarnotts
 */


require('header.php');

?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php
    // Get the hero image
    $heroImageUrl = (has_post_thumbnail()) ? omGetPostThumbnailUrl(get_post_thumbnail_id($post->ID), null) : omGetPostThumbnailUrl(getFallbackHeroImage(), null);
    ?>

    <div class="page-hero" style="<?php echo 'background-image:url(\''. $heroImageUrl .'\');'?>">
        <div class="container">
            <h1>
                <?php the_title(); ?>
            </h1>
        </div>
    </div>

    <div class="section-global--color-6 print-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <?php if (function_exists('om_breadcrumb_lists')) { om_breadcrumb_lists(); } ?>
                </div>
                <div class="col-sm-6">
                    <?php include_once('add-this.php'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content" role="main">
        <div class="container">
            <div class="row">
                <div class="col-md-8">

                    <div class="wysiwyg">
                        <?php the_content() ?>
                    </div>

                </div>
                <!-- /.col-md-8 -->

                <div class="col-md-4">
                    <?php omGetSecondaryMenu($post); ?>

                    <?php omGetSidbarCrossLinkTiles($post->ID); ?>
                </div>
            </div>
            <!-- /.row -->

            <?php
            endwhile;

            else:
                // If no content, include the "No posts found" template.
                get_template_part('content', 'none');

            endif;
            ?>

        </div><!-- /.container -->

    </div><!-- /.main-content -->

<?php omGetBottomCrossLinkTiles($post->ID); ?>

<?php require('footer.php'); ?>

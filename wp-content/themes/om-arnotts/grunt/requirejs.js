/**
 * Created by jessica.nierth on 14/03/2015.
 */
module.exports = {
    compile: {
        options: {
            generateSourceMaps: false,
            logLevel: 4,
            baseUrl: "assets/scripts/dev",
            include: './main',
            out: "assets/scripts/main.min.js",
            optimize: 'uglify',
            mainConfigFile:'assets/scripts/dev/main.js'
        }
    },
    main: {
        options: {
            generateSourceMaps: false,
            logLevel: 4,
            baseUrl: "assets/scripts/dev",
            include: './main',
            out: "assets/scripts/main.js",
            optimize: 'none',
            mainConfigFile:'assets/scripts/dev/main.js'
        }
    }
};
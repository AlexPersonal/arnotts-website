/**
 * Created by jessica.nierth on 14/03/2015.
 */
module.exports = {
    watchUberMenuStyles: {
        src: {
            files: [
                'assets/styles/less/vendor/ubermeganav/custom.less'
            ]
        }
    }
};
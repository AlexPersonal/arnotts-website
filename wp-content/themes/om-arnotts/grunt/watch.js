/**
 * Created by jessica.nierth on 14/03/2015.
 */
module.exports = {
    src: {
        files: [
            'assets/scripts/dev/*.js',
            'assets/scripts/dev/**/*.js'
        ],
        tasks: ['requirejs']
    },
    recess: {
        files: [
            'assets/styles/less/*.less',
            'assets/styles/less/**/*.less',
            'assets/styles/less/**/**/*.less'
        ],
        tasks: ['recess']
    }
};
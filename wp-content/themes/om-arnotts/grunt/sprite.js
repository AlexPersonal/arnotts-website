/**
 * Created by jessica.nierth on 14/03/2015.
 */
module.exports = {
    sprite:{
        src: 'assets/images/sprites/*.png',
        dest: 'assets/images/sprites.png',
        destCss: 'assets/styles/sprites.css',
        padding: 10
    }
};
/**
 * Created by jessica.nierth on 14/03/2015.
 */
module.exports = {
    options: {
        compile: true,
        banner: '',
        strictPropertyOrder: true
    },
    styleguide: {
        src: ['assets/styles/less/styleguide.less'],
        dest: 'assets/styles/styleguide.css'
    },
    styleguideMin: {
        options: {
            compress: true
        },
        src: ['assets/styles/less/styleguide.less'],
        dest: 'assets/styles/styleguide.min.css'
    },
    main: {
        src: ['assets/styles/less/main.less'],
        dest: 'style.css'
    },
    min: {
        options: {
            compress: true
        },
        src: ['assets/styles/less/main.less'],
        dest: 'style.min.css'
    },
    uberMenuStyles: {
        options: {
            compress: true
        },
        src: ['assets/styles/less/vendor/ubermeganav/ubermenu-main.less'],
        dest: '../../plugins/ubermenu/custom/custom.css' // required by ubermenu
    }
};
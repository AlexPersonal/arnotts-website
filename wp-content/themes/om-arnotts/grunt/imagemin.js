/**
 * Created by jessica.nierth on 14/03/2015.
 */
module.exports = {
    dynamic: {
        files: [{
            expand: true,
            cwd: '../../../wp-content/uploads',
            src: ['**/*.{png,jpg,gif}'],
            dest: '../../../wp-content/compressed/'
        }]
    },
    png: {
        options: {
            optimizationLevel: 7
        },
        files: [
            {
                // Set to true to enable the following options…
                expand: true,
                // cwd is 'current working directory'
                cwd: '../../../wp-content/uploads',
                src: ['**/*.png'],
                // Could also match cwd line above. i.e. project-directory/img/
                dest: '../../../wp-content/compressed/',
                ext: '.png'
            }
        ]
    },
    jpg: {
        options: {
            progressive: true
        },
        files: [
            {
                // Set to true to enable the following options…
                expand: true,
                // cwd is 'current working directory'
                cwd: '../../../wp-content/uploads',
                src: ['**/*.jpg'],
                // Could also match cwd. i.e. project-directory/img/
                dest: '../../../wp-content/compressed/',
                ext: '.jpg'
            }
        ]
    }
};

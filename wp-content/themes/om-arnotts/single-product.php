<?php


require('header.php');

?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
        // Get the hero image
        $heroImageUrl = (has_post_thumbnail()) ? omGetPostThumbnailUrl(get_post_thumbnail_id($post->ID), null) : omGetPostThumbnailUrl(getFallbackHeroImage(), null);
    ?>
<div class="section-products-detail">
    <div class="page-hero" style="<?php echo 'background-image:url(\''. $heroImageUrl .'\');'?>">
        <div class="container">
            <h1>
                <?php the_title(); ?>
            </h1>
        </div>
    </div>

    <div class="section-global--color-6">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php if (function_exists('om_breadcrumb_lists')) { om_breadcrumb_lists(); } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="section-global section-global--color-6 page-intro">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <?php $productPreview = get_post_meta( $post->ID, 'product_overview_image', true ); ?>

                    <?php if(!empty($productPreview['ID'])): ?>
                        <div class="page-intro--image">
                            <div class="page-intro--image-wrapper">
                                <?php $imageURL = omGetImageUrlByAttachmentID($productPreview['ID'], null);?>
                                <img src="<?php echo $imageURL ?>" alt="<?php echo $productPreview->post_title; ?>"/>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-md-8">
                    <div class="page-intro--copy">
                        <?php the_content() ?>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>

    <div class="section-global section-global--color-12">
        <div class="container">
            <?php

                // 1st level can be product category or a product
                $subProductCategories = omGetProductSubCategoriesByProductGroupID($post->ID);
                $numberOfSubProductCategories = count($subProductCategories);

                if($numberOfSubProductCategories) {

                    // Check if the first element is a category or a product (if it doesn't have a child it is a product)
                    $childOfProduct = omGetAllProductsByProductSubCategoryID($subProductCategories[0]->ID);


                    // MULTIPLE PRODUCTS WITH SUBCATEGORY
                    if (!empty($childOfProduct)) {

                        include(locate_template('backend/templates/content-products-multiple-within-subcategory.php'));

                    // MULTIPLE PRODUCTS WITHOUT SUBCATEGORY
                    } else {

                        // If $childProduct is empty the '$subProductCategories' are actually the 'products' as they don't have a parent.
                        // Just giving it other var names for better readability
                        $products = $subProductCategories;
                        $productsLength = $numberOfSubProductCategories;

                        include(locate_template('backend/templates/content-products-multiple-products.php'));

                    }

                // SINGLE PRODUCT
                } else {

                    // If $childProduct is empty the '$subProductCategories' are actually the 'products' as they don't have a parent.
                    // Just giving it other var names for better readability
                    $products = $subProductCategories;
                    $productsLength = $numberOfSubProductCategories;

                    include(locate_template('backend/templates/content-products-single.php'));
                }

            ?>

            <?php endwhile;

                else:

                // If no content, include the "No posts found" template.
                get_template_part('content', 'none');

                endif;
            ?>
        </div>
    </div>

    <!-- Add link to Nutritional information if a pdf is linked in the sidebar -->
    <?php omGetNutritionalInformationLink($post->ID); ?>


    <!-- Print out the product promotions section if there is one -->
    <?php omGetProductPromotionPanel($post->ID); ?>

    <!-- Print out the products social media list if there is at least one set up in the CMS -->
    <?php omGetSocialMediaList($post->post_title, $post->ID); ?>

    <!-- Print out the products recipe list if there is at least one set up in the CMS -->
    <?php omGetRecipeList($post->post_title); ?>

    <!-- Print out recommended products section if there are ones -->
    <?php omGetRecommendedProductsByPostMetaProductRelation($post->post_title, $post->ID); ?>

</div><!-- section-products-detail -->
<?php require('footer.php'); ?>
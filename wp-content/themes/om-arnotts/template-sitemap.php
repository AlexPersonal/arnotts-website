<?php
/*
 * Template Name: Sitemap
 */

require('header.php');

?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
    // Get the hero image
    $heroImageUrl = (has_post_thumbnail()) ? omGetPostThumbnailUrl(get_post_thumbnail_id($post->ID), null) : omGetPostThumbnailUrl(getFallbackHeroImage(), null);
    ?>

    <div class="page-hero" style="<?php echo 'background-image:url(\''. $heroImageUrl .'\');'?>">
        <div class="container">
            <h1>
                <?php the_title(); ?>
            </h1>
        </div>
    </div>


    <div class="section-global--color-6">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php if (function_exists('om_breadcrumb_lists')) { om_breadcrumb_lists(); } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="section-global sitemap-list--wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php echo do_shortcode('[om-print-sitemap]'); ?>


                    <?php endwhile;

                    else:

                        // If no content, include the "No posts found" template.
                        get_template_part('content', 'none');

                    endif;
                    ?>
                </div><!-- /.col-sm-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>

<?php require('footer.php'); ?>
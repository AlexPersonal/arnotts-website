/* jshint node: true */

module.exports = function(grunt) {
  "use strict";

  // Makes grunt.loadTask unnecessary as this will look in your package.json for the dependencies
  require('load-grunt-tasks')(grunt);

  // We don't want to scroll through a huge grunt file therefore splitting this up into smaller pieces
  // see folder 'grunt' for more information
  require('load-grunt-config')(grunt);

};

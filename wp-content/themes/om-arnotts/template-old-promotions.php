<?php
/*
 * Template Name: Promotions Old Listing Page
 */
require('header.php');

?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
        $currentPostPageID = $post->ID;

        // Get the hero image
        $heroImageUrl = (has_post_thumbnail()) ? omGetPostThumbnailUrl(get_post_thumbnail_id($post->ID), null) : omGetPostThumbnailUrl(getFallbackHeroImage(), null);
    ?>

<div class="page-hero" style="<?php echo 'background-image:url(\''. $heroImageUrl .'\');'?>">
    <div class="container">
        <h1>
            <?php the_title(); ?>
        </h1>
    </div>
</div>

<div class="section-global--color-6">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php if (function_exists('om_breadcrumb_lists')) { om_breadcrumb_lists(); } ?>
            </div>
        </div>
    </div>
</div>

<div class="promotions-page--wrapper" role="main">
    <div class="section-global section-global--color-6 page-intro search-form-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                <?php the_content() ?>

            <?php endwhile;

            else:

                // If no content, include the "No posts found" template.
                get_template_part('content', 'none');

            endif;
            ?>

                </div><!-- /.col-sm-12 -->
            </div><!-- /.row -->

            <?php // SECONDARY LOOP FOR Promotions POSTS 

            $allPromotionsPosts = omGetOldPromotionPosts();
            $postsPerPage = omGetPromotionsPostsPerPage();
            $currentPage = omGetCurrentPage();
            $startLoop = (!$currentPage ||  $currentPage == 1) ? 0 : ($currentPage * $postsPerPage) - $postsPerPage;
            ?>
            
        </div><!-- /.container -->
    </div><!-- /.page-intro -->

    <div class="section-global search-list">

        <div class="container">
            <div class="row">
                <?php // SET THE LOOP START POINTS ?>

                <div class="col-lg-12">

                    <?php $PromotionsLength  = count($allPromotionsPosts); ?>

                    <div class="row visible-lg visible-md js-section-products-view-tabs">
                        <div class="col-sm-12">
                            <div role="tabpanel">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="col-sm-3">
                                        <a href="<?php echo get_permalink( 54 ); ?>" >
                                            Current
                                        </a>
                                    </li>
                                    <li role="presentation" class="col-sm-3 active">
                                        <a href="<?php echo get_permalink( 1594 ); ?>">
                                            Past
                                        </a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <!-- Nav tabs -->
                                    <div role="tabpanel" class="tab-pane fade active in" id="current">
                                    <?php for($i = $startLoop; $i < $PromotionsLength && ($i < $startLoop + $postsPerPage); $i++): ?>

                                    <?php
                                        $promotions_post_id = $allPromotionsPosts[$i]->ID;
                                        $promotions_image = (get_post_meta($promotions_post_id, 'promotions_asset', true));
                                        $promotions_excerpt = (get_post_meta($promotions_post_id, 'promotions_excerpt', true));
                                        $promotions_asset_url = omGetPostThumbnailUrl($promotions_image['ID'], 'om-promotion-size-small');
                                        $promotions_post_date = mysql2date('j F Y', $allPromotionsPosts[$i]->post_date);
                                        $promotions_date = mysql2date('j F Y', (get_post_meta($promotions_post_id, 'promotions_date', true)));
                                        $promotions_external_link = (get_post_meta($promotions_post_id, 'promotions_external_link', true));
                                        $promotions_link = ($promotions_external_link) ? $promotions_external_link : get_permalink($promotions_post_id);
                                        ?>


                                        <div class="search-result-item">
                                            <div class="row">
                                                <div class="col-sm-4 media-image">
                                                    <a href="<?php echo $promotions_link ?>" <?php if ($promotions_external_link): ?> target="_blank" <?php endif; ?>>
                                                        <img src="<?php echo $promotions_asset_url ?>" alt="<?php echo $allPromotionsPosts[$i]->post_title; ?>" />
                                                    </a>
                                                </div>
                                                <div class="col-sm-8 search-text--wrapper">
                                                    <h3><?php echo $allPromotionsPosts[$i]->post_title; ?></h3>
                                                    <p class="date"><?php echo $promotions_date; ?></p>
                                                    <p><?php echo $promotions_excerpt; ?> </p>
                                                    <a href="<?php echo $promotions_link; ?>" class="global-link" <?php if ($promotions_external_link): ?> target="_blank" <?php endif; ?>>Read more</a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php endfor; ?>
                                    <?php if($PromotionsLength == 0): ?>
                                        <h2>There are no past promotions available.</h2>
                                        <p>Please check back soon</p>
                                    <?php endif; ?>
                                    <?php omCustomLoopPagination($allPromotionsPosts, $postsPerPage, 'past'); ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="row hidden-md hidden-lg section-products-overview js-section-view-accordion">
                        <div class="panel-group container" id="accordion">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <a class="collapsed" href="<?php echo get_permalink( 54 ); ?>">
                                            current
                                        </a>
                                    </h3>
                                </div>
                                
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#current-accordion">
                                            past
                                        </a>
                                    </h3>
                                </div>
                                <div id="current-accordion" class="list-products-overview panel-collapse in">

                                    <div class="panel-body">
                                        <?php for($i = $startLoop; $i < $PromotionsLength && ($i < $startLoop + $postsPerPage); $i++): ?>

                                        <?php
                                            $promotions_post_id = $allPromotionsPosts[$i]->ID;
                                            $promotions_homepage_image = (get_post_meta($promotions_post_id, 'promotions_asset', true));
                                            $promotions_excerpt = (get_post_meta($promotions_post_id, 'promotions_excerpt', true));
                                            $promotions_asset_url = omGetPostThumbnailUrl($promotions_homepage_image['ID'], 'om-promotion-size-large');
                                            $promotions_post_date = mysql2date('j F Y', $allPromotionsPosts[$i]->post_date);
                                            $promotions_date = mysql2date('j F Y', (get_post_meta($promotions_post_id, 'promotions_date', true)));
                                            $promotions_external_link = (get_post_meta($promotions_post_id, 'promotions_external_link', true));
                                            $promotions_link = ($promotions_external_link) ? $promotions_external_link : get_permalink($promotions_post_id);
                                            ?>


                                            <div class="search-result-item">
                                                <div class="row">
                                                    <div class="col-sm-4 media-image">
                                                        <a href="<?php echo $promotions_link ?>" <?php if ($promotions_external_link): ?> target="_blank" <?php endif; ?>>
                                                            <img src="<?php echo $promotions_asset_url ?>" alt="<?php echo $allPromotionsPosts[$i]->post_title; ?>" />
                                                        </a>
                                                    </div>
                                                    <div class="col-sm-8 search-text--wrapper">
                                                        <h3><?php echo $allPromotionsPosts[$i]->post_title; ?></h3>
                                                        <p class="date"><?php echo $promotions_date; ?></p>
                                                        <p><?php echo $promotions_excerpt; ?> </p>
                                                        <a href="<?php echo $promotions_link; ?>" class="global-link" <?php if ($promotions_external_link): ?> target="_blank" <?php endif; ?>>Read more</a>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endfor; ?>
                                        <?php if($PromotionsLength == 0): ?>
                                            <h2>There are no past promotions available.</h2>
                                            <p>Please check back soon</p>
                                        <?php endif; ?>
                                        <?php omCustomLoopPagination($allPromotionsPosts, $postsPerPage); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!-- /.col-lg-9 -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>
</div><!-- .main-content -->

<?php omGetBottomCrossLinkTiles($currentPostPageID); ?>

<?php require('footer.php'); ?>
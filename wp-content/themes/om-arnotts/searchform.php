<?php
/**
 * Overwrite Wordpress search from from 'source/wordpress/wp-includes/general-template.php'
 * with bootstrap3 HTML structure
 *
 * @package WordPress
 * @since omarnotts
 */
?>

<form class="navbar-form navbar-right" role="search" method="get" id="searchform" action="<?php esc_url( home_url( '/' ) ) ?>">

	<?php echo '<label class="control-label screen-reader-text" for="global-search">' . __('Search:') . '</label>' ?>
	<div class="form-group">
		<input class="form-control" type="text" value="<?php echo get_search_query(); ?>" name="s" placeholder="Search" id="global-search" />
	</div>
	<button type="submit" class="btn btn-default" id="searchsubmit">Submit</button>
</form>


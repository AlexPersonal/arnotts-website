<?php
/*
 * Template Name: Feedback Page
 */
require('header.php');

?>

<?php if (have_posts()) : while (have_posts()) : the_post();
$currentPostPageID = $post->ID;
// Get the hero image
        $heroImageUrl = (has_post_thumbnail()) ? omGetPostThumbnailUrl(get_post_thumbnail_id($post->ID), null) : omGetPostThumbnailUrl(getFallbackHeroImage(), null);
?>

<div class="page-hero" style="<?php echo 'background-image:url(\''. $heroImageUrl .'\');'?>">
    <div class="container">
        <h1>
            <?php the_title(); ?>
        </h1>
    </div>
</div>

<div class="section-global--color-6">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php if (function_exists('om_breadcrumb_lists')) { om_breadcrumb_lists(); } ?>
            </div>
        </div>
    </div>
</div>
<div class="feedback-form" role="main">
    <form action="/feedback/" method="POST" name="feedbackForm" class="js-feedback-form">
        <div class="section-global section-global--color-6 page-intro search-form-wrapper">
            <div class="container">
                <?php
                    $errors = omValidateMail();
                    if(empty($_POST) && !strlen($errors) == 0):
                ?>
                <?php the_content() ?>
                <?php
                    endif;
                ?>

                <?php endwhile;

                else:

                    // If no content, include the "No posts found" template.
                    get_template_part('content', 'none');

                endif;
                ?>
            </div>
            <?php
                $errors = omValidateMail();
                if(empty($_POST) && !strlen($errors) == 0):
            ?>
            <div class="search-form section-global--color-9">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 search-item">
                            <label class="keywordLabel" for="keywordInput">Select type of feedback</label>
                        </div>
                        <div class="col-sm-5">
                            <select name="feedbackDropdown" class="js-toggle-form" autocomplete="off">
                                <option value="generalFeedback">General feedback / Question</option>
                                <option value="complaints">Make a complaint</option>
                            </select>
                        </div>
                    </div>
                </div><!-- /.container -->
            </div>
            <?php
            endif;
            ?>
        </div><!-- /.page-intro -->

        <div class="section-global js-feedback-form-content">

            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <p class="global-form-error-message">We are missing some information. Please ensure you fill out all non-optional fields and try again.</p>
                        <?php
                        $errors = "";
                        if (!empty($_POST)):
                            $errors = omValidateMail();

                            if(strlen($errors) > 0):
                            ?>
                            <div class="server-side-error-msg">
                            <?php
                                echo($errors);
                            ?>
                            </div>
                            <?php
                            endif;
                        endif;
                        if(!empty($_POST) && strlen($errors) == 0):
                            omSendFeedback();
                        ?>
                            <h3>Thanks for your feedback!</h3>
                            <p class="thank-you-feedback-text">You may also like to browse through our <a href="<?php echo get_permalink( 460 ); ?>">Frequently Asked Questions</a> where there’s a good chance you’ll find the answer you’re looking for.</p>
                            <a href="/" class="global-link">return home</a>
                        <?php
                        else:
                        ?>
                        <h3 class="js-show-general">General Feedback / Question</h3>
                        <h3 class="js-show-complaints">make a complaint</h3>
                        <p>(* indicates mandatory fields)</p>
                        <div class="section-feedback-form">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-12 js-field-required-different">
                                        <label class="js-show-general" for="feedbackInput">Type in your feedback or question in the box *</label>
                                        <label class="js-show-complaints" for="feedbackInput">Brief description of the issue *</label>
                                        <textarea name="feedbackInput" class="form-control js-check-field-comment-box" maxlength="1000"></textarea>
                                        <p class="error required-feedback-error">Please enter a description of your general feedback / question.</p>
                                        <p class="error required-complaint-error">Please enter a description of your complaint.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6 js-field-required-global js-no-numbers">
                                        <label for="firstNameInput">First name *</label>
                                        <input name="firstNameInput" type="text" class="form-control js-check-field" maxlength="30" />
                                        <p class="error required-error">Please enter your first name.</p>
                                    </div>
                                    <div class="col-sm-6 js-field-required-global js-no-numbers">
                                        <label for="lastNameInput">Last name *</label>
                                        <input name="lastNameInput" type="text" class="form-control js-check-field" maxlength="30" />
                                        <p class="error required-error">Please enter your last name.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6 js-field-required-global js-check-email">
                                        <label for="emailInput">Email *</label>
                                        <input name="emailInput" type="text" class="form-control emailInput js-check-field" />
                                        <p class="error required-error">Please enter your email address.</p>
                                        <p class="error invalid">This doesn't look like a valid email. Please try again.</p>
                                    </div>
                                    <div class="col-sm-6 js-no-characters-complaints js-field-required-complaints">
                                        <label for="telephoneInput">Telephone (including area code) <span class="required-star js-show-complaints">*</span></label>
                                        <input name="telephoneInput" type="tel" class="form-control js-check-field telephoneInput" maxlength="20" />
                                        <p class="error required-error">Please enter your telephone number.</p>
                                        <p class="error invalid">This doesn't look like a valid telephone number. Please try again.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section-feedback-form">
                            <div class="row">
                                <h3 class="col-sm-12">Additional contact details</h3>
                                <p class="col-sm-12">(optional)</p>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label for="streetInput">Street</label>
                                        <input name="streetInput" class="form-control" type="text" maxlength="30" />
                                        <p class="error"></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="suburbInput">Suburb</label>
                                        <input name="suburbInput" type="text" class="form-control" maxlength="30" />
                                        <p class="error"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label for="countryDropdown">Country</label>
                                        <select name="countryDropdown" class="js-country-dropdown form-control">
                                            <option value="" disabled selected>Select Your Option</option>
                                            <option value="Australia">Australia</option>
                                            <option value="New Zealand">New Zealand</option>
                                        </select>
                                        <p class="error"></p>
                                    </div>
                                    <div class="col-sm-6 js-state-dropdown">
                                        <label for="stateDropdown">State</label>
                                        <select name="stateDropdown" class="form-control">
                                            <option value="" disabled selected>Select Your Option</option>
                                            <option value="NSW">NSW</option>
                                            <option value="QLD">QLD</option>
                                            <option value="SA">SA</option>
                                            <option value="TAS">TAS</option>
                                            <option value="VIC">VIC</option>
                                            <option value="WA">WA</option>
                                            <option value="ACT">ACT</option>
                                            <option value="NT">NT</option>
                                        </select>
                                        <p class="error"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6 js-no-characters">
                                        <label for="postcodeInput">Postcode</label>
                                        <input name="postcodeInput" type="text" class="form-control js-check-field postcodeInput" maxlength="4" />
                                        <p class="error required-error">Please enter your 4-digit postcode.</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="js-show-complaints section-feedback-form">
                            <div class="row">
                                <h3 class="col-sm-12">Product Details</h3>
                                <div class="form-group">
                                    <div class="col-sm-6 js-field-required-complaints js-no-numbers-complaints">
                                        <label for="productNamesInput">Product name *</label>
                                        <input name="productNamesInput" type="text"  class="form-control js-check-field" maxlength="100" />
                                        <p class="error required-error">Please enter a product name.</p>
                                    </div>
                                    <div class="col-sm-6 js-field-required-complaints">
                                        <label for="packetSizeInput">Packet size *</label>
                                        <input name="packetSizeInput" type="text" class="form-control js-check-field" maxlength="30" />
                                        <p class="error required-error">Please enter the packet size.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6 js-field-required-complaints">
                                        <label for="howOftenDropdown">How often do you eat the product *</label>
                                        <select name="howOftenDropdown" class="form-control js-check-field">
                                            <option value="" disabled selected>Select Your Option</option>
                                            <option value="FREQ">Frequent</option>
                                            <option value="OCCAS">Occasional</option>
                                            <option value="FIRST">First Time</option>
                                        </select>
                                        <p class="error required-error">Please select how often you eat this product.</p>
                                    </div>
                                    <div class="col-sm-6 best-before--wrapper">
                                        <label for="bestBeforeDropdown">Best before date (DD/MM/YYYY)</label>
                                        <input name="bestBeforeDropdown" type="text" placeholder="DD/MM/YYYY" class="form-control bestBeforeDropdown js-check-field" maxlength="10" />
                                        <p class="error date-error">This doesn't look like a valid date. Please ensure that it is entered as DD/MM/YYYY.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6 js-field-required-complaints js-no-numbers-complaints">
                                        <label for="wherePurchasedInput">Store where purchased *</label>
                                        <input name="wherePurchasedInput" type="text" class="form-control js-check-field" maxlength="30" />
                                        <p class="error required-error">Please enter the store where purchased.</p>
                                    </div>
                                    <div class="col-sm-6 js-field-required-complaints js-no-numbers-complaints">
                                        <label for="storeLocationInput">Location of store *</label>
                                        <input name="storeLocationInput" type="text" class="form-control js-check-field" maxlength="30" />
                                        <p class="error required-error">Please enter the location of the store.</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="section-terms-feedback-form">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="checkbox" name="privacyStatment" value="true" class="js-checkbox-field"><label for="privacyStatment">I have read and agree to the <a href="<?php echo get_permalink( 62 ); ?>">Privacy Statement</a> and agree to be contacted regarding this submission.</label>
                                    <p class="error required-error">Please agree to the privacy statement.</p>
                                </div>
                                <div class="col-sm-12 submit-btn">
                                    <button type="submit" class="btn btn-default" id="submitBtn">Submit</button>
                                </div>
                            </div>
                        </div>
                        <?php
                        endif;
                        ?>
                    </div>
                    <div class="col-lg-3">
                        <?php omGetSidbarCrossLinkTiles($currentPostPageID); ?>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>
    </form>
</div><!-- .main-content -->
<?php omGetBottomCrossLinkTiles($currentPostPageID); ?>

<?php require('footer.php'); ?>
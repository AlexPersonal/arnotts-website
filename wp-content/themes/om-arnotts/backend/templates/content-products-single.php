<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 20/01/2015
 * Time: 4:09 PM
 */
?>

<div class="row">
    <div class="col-sm-12">

        <ul class="list-products-detail">
            <li class="<?php echo omGetProductPackageImageListClass(1); ?>">
                <div class="tile tile--style-5">
                    <div class="image--container">
                        <?php $productPackageImage = get_post_meta( $post->ID, 'product_package_image', true ); ?>
                        <?php $productWeight = get_post_meta( $post->ID, 'product_weight', true ); ?>
                        <?php if(!empty($productPackageImage['ID'])): ?>
                            <?php echo omGetImageByAttachmentID($productPackageImage['ID'], 'om-package-size-medium') ?>
                        <?php endif; ?>
                    </div>
                    <div class="tile--footer">
                        <p class="product--title"><?php the_title(); ?></p>
                        <p class="product--weight"><?php echo $productWeight; ?></p>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
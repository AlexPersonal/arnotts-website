<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 20/01/2015
 * Time: 4:09 PM
 */
?>
<h2><?php the_title(); ?> Range</h2>

<div class="row">
    <div class="col-sm-12">

        <?php if($productsLength):?>
            <ul class="list-products-detail">

                <?php for($k=0; $k < $productsLength; $k++) : ?>

                    <li class="<?php echo omGetProductPackageImageListClass($productsLength); ?>">
                        <div class="tile tile--style-5">
                            <div class="image--container">
                                <?php $productPreview = get_post_meta( $products[$k]->ID, 'product_package_image', true ); ?>
                                <?php $productWeight = get_post_meta( $products[$k]->ID, 'product_weight', true ); ?>
                                <?php if(!empty($productPreview['ID'])): ?>
                                    <?php echo omGetImageByAttachmentID($productPreview['ID'],'om-package-size-medium') ?>
                                <?php endif; ?>
                            </div>
                            <div class="tile--footer">
                                <p class="product--title"><?php echo $products[$k]->post_title; ?></p>
                                <p class="product--weight"><?php echo $productWeight; ?></p>
                            </div>
                        </div>
                    </li>

                <?php endfor; ?>

            </ul>
        <?php endif; ?>
    </div>
</div>
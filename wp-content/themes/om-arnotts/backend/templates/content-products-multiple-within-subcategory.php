<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 20/01/2015
 * Time: 3:59 PM
 */

?>

<h2>The <?php the_title(); ?> Range</h2>

<div class="row visible-lg visible-md js-section-products-view-tabs">
    <div class="col-sm-12">
        <div role="tabpanel">

            <!-- Nav tabs -->
            <ul class="nav nav-justified nav-tabs" role="tablist">
                <?php for($i = 0; $i < $numberOfSubProductCategories; $i ++): ?>

                    <?php $productSubCategoryTitle = $subProductCategories[$i]->post_title; ?>
                    <?php $productSubCategoryTitleCSS = omConvertStringToValidID($subProductCategories[$i]->post_title); ?>

                    <li role="presentation" class="<?php echo ($i==0) ? 'active' : '';?>">
                        <a href="#<?php echo $productSubCategoryTitleCSS; ?>" <?php if($categories[$i]->cat_name){?> aria-controls="<?php echo omConvertStringToValidID($categories[$i]->cat_name); ?>" <?php } ?> role="tab" data-toggle="tab">
                            <?php echo $productSubCategoryTitle ?>
                        </a>
                    </li>
                <?php endfor; ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">

                <?php for($j = 0; $j < $numberOfSubProductCategories; $j ++): ?>

                    <?php $productSubCategoryContent = $subProductCategories[$j]->post_content; ?>
                    <?php $productSubCategoryTitleCSS = omConvertStringToValidID($subProductCategories[$j]->post_title); ?>
                    <?php $products = omGetAllProductsByProductSubCategoryID($subProductCategories[$j]->ID); ?>
                    <?php $productsLength = count($products); ?>

                        <!-- Nav tabs -->
                        <div role="tabpanel" class="tab-pane fade <?php echo ($j == 0) ? 'active in' : '';?>" id="<?php echo $productSubCategoryTitleCSS; ?>">

                            <div class="products-detail--wysiwyg-content">
                                <?php echo $productSubCategoryContent; ?>
                            </div>
                            <?php if($productsLength):?>
                                <ul class="list-products-detail">

                                <?php for($k=0; $k < $productsLength; $k++) : ?>

                                    <li class="<?php echo omGetProductPackageImageListClass($productsLength); ?>">
                                        <div class="tile tile--style-5">
                                            <div class="image--container">
                                                <?php $productPreview = get_post_meta( $products[$k]->ID, 'product_package_image', true ); ?>
                                                <?php $productWeight = get_post_meta( $products[$k]->ID, 'product_weight', true ); ?>
                                                <?php if(!empty($productPreview['ID'])): ?>
                                                    <?php echo omGetImageByAttachmentID($productPreview['ID'], 'om-package-size-medium') ?>
                                                <?php endif; ?>
                                            </div>
                                            <div class="tile--footer">
                                                <p class="product--title"><?php echo $products[$k]->post_title; ?></p>
                                                <p class="product--weight"><?php echo $productWeight; ?></p>
                                            </div>
                                        </div>
                                    </li>

                                <?php endfor; ?>

                                </ul>
                            <?php endif; ?>
                        </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</div>



<div class="row hidden-md hidden-lg section-products-overview js-section-view-accordion">
    <div class="panel-group container" id="accordion">
        <?php for($k = 0; $k < $numberOfSubProductCategories; $k ++): ?>
        <?php $productSubCategoryTitle = $subProductCategories[$k]->post_title; ?>
        <?php $productSubCategoryContent = $subProductCategories[$k]->post_content; ?>
        <?php $productSubCategoryTitleCSS = omConvertStringToValidID($subProductCategories[$k]->post_title); ?>
        <?php $products = omGetAllProductsByProductSubCategoryID($subProductCategories[$k]->ID); ?>
        <?php $productsLength = count($products); ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $productSubCategoryTitleCSS; ?>-accordion">
                        <?php echo $productSubCategoryTitle ?>
                    </a>
                </h3>
            </div>
            <div id="<?php echo $productSubCategoryTitleCSS; ?>-accordion" class="list-products-overview panel-collapse collapse">

                <div class="panel-body">
                    <div class="products-detail--wysiwyg-content">
                        <?php echo $productSubCategoryContent; ?>
                    </div>
                    <?php if($productsLength):?>
                        <ul class="list-products-detail">

                        <?php for($l=0; $l < $productsLength; $l++) : ?>

                            <li>
                                <div class="tile tile--style-5">
                                    <div class="image--container">
                                        <?php $productPreview = get_post_meta( $products[$l]->ID, 'product_package_image', true ); ?>
                                        <?php $productWeight = get_post_meta( $products[$l]->ID, 'product_weight', true ); ?>
                                        <?php if(!empty($productPreview['ID'])): ?>
                                            <?php echo omGetImageByAttachmentID($productPreview['ID'], 'om-package-size-medium') ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="tile--footer">
                                        <p class="product--title"><?php echo $products[$l]->post_title; ?></p>
                                        <p class="product--weight"><?php echo $productWeight; ?></p>
                                    </div>
                                </div>
                            </li>

                        <?php endfor; ?>

                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endfor ?>
    </div>
</div>

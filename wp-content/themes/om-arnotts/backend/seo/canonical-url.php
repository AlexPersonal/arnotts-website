<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 25/02/2015
 * Time: 3:51 PM
 */

if(!function_exists('aioseop_canonical_url')) {
    add_filter( 'aioseop_canonical_url', 'seoguye_custom_url' );

    /**
     * Overwrite the 'All in One SEO' filter to redirect to www if none www was set.
     * @param $url { String } The $url the user is currently on
     * @return mixed { String } The url to be used in the canonical meta tag
     */
    function seoguye_custom_url( $url ) {
        global $post;
        if ( !empty( $post ) ) {
            $url_meta = get_post_meta( $post->ID, 'url', true );
            if ( !empty( $url_meta ) ) return $url_meta;
        }

        $protocol = stripos($url,'https') !== false ? 'https://' : 'http://';

        // Add www to the canonical url if site runs without www.
        if(strpos($url,'www') === false) {

            $url = str_replace($protocol, $protocol.'www.', $url);

        }

        return $url;
    }
}


<?php

/**
 * Takes the currently visible product, looks which type that product was ticked for 'recommended_product_relation_type'. Then this
 * section will be taken to find other products that were set to have the same product relation type.
 *
 * @param $postTitle
 * @param $productID
 */
function omGetRecommendedProductsByPostMetaProductRelation($postTitle, $productID) {
    $productCategory= get_post_meta( $productID, 'recommended_product_relation_type', true );

    $args = array(
        'post_type' => 'product',
        'post_status' => 'publish',
        'meta_query' => array(
            array(
                'key' => 'recommended_product_relation_type',
                'value' => $productCategory,
                'compare' => '='
            )
        ),
        'exclude' => $productID,
        'posts_per_page' => 1000,
        'suppress_filters' => true

    );
    $recommendedPostsTmp = get_posts($args);

    // Only add this array if it's length is > 0
    if(count($recommendedPostsTmp)) {

        $recommendedPostsLength = count($recommendedPostsTmp);

        for($i = 0; $i < $recommendedPostsLength; $i++) {
            $recommendedPostID = $recommendedPostsTmp[$i]->ID;

            // Unfortunately the meta_query returns all products if there is no matching meta_value
            if(get_post_meta($recommendedPostID, 'recommended_product_relation_type', true)) {
                $recommendedPosts[$recommendedPostID] = $recommendedPostsTmp[$i];
            }
        }
    }

    $recommendedPostsLength = count($recommendedPosts);

    if($recommendedPostsLength) {

        // Randomize the order
        shuffle($recommendedPosts);

        // Only 4 max
        if($recommendedPostsLength > 4) {
            // We only need to show 4 items therefore slice away whatever is not needed here.
            $recommendedPosts = array_slice($recommendedPosts, 0, 4);
        }

        // If there are any recommended posts render the section
        if(count($recommendedPosts)) {
            renderRecommendedProductsPanel($postTitle, $recommendedPosts);
        }
    }

}

/**
 * NOT BEING USED BECAUSE OF CHANGE REQUEST ARNOTTS-340
 * Takes the currently visible product, looks in which product_category it is in. Grabs further products
 * from those categories and if the total number is < 4 it will pull further products from other categories.
 *
 * @param $postTitle { String } The posts title.
 * @param $productID { Number } The id of the currently viewed product.
 * @return Array $recommendedPosts Array of further recommended products.
 */
function omGetRecommendedProductsByTaxonomyProductCategory($postTitle, $productID) {

    // Get the categories that are attached to the product
    $productCategories = get_the_terms( $productID,'product_category');

    // Caching the array of product category IDs that the current product is attached to
    $productCategoryIDs = array();

    // To save the results in
    $recommendedPosts = array();

    // Loop through the array of categories and
    foreach($productCategories as $productCategory) {

        // Save the IDs of product categories in another array
        // This array will be used if there are not enough products within the categories that the current
        // post is attached to.
        array_push($productCategoryIDs, $productCategory->term_taxonomy_id);

        // Call for other products within the same product_category as the current product, excluding
        // the current product.
        $recommendedPostArgs = array(
            'posts_per_page' => 4,
            'offset' => 0,
            'orderby' => 'rand',
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_category',
                    'field' => 'id',
                    'terms' => array($productCategory->term_taxonomy_id),
                    'orderby' => 'rand'
                )),
            'post_type' => 'product',
            'exclude' => array($productID),
            'post_parent' => 0,
            'post_status' => 'publish'
        );

        $recommendedPostsTmp = get_posts($recommendedPostArgs);

        // Only add this array if it's length is > 0
        if(count($recommendedPostsTmp)) {

            $recommendedPostsLength = count($recommendedPostsTmp);

            for($i = 0; $i < $recommendedPostsLength; $i++) {
                $recommendedPosts[$recommendedPostsTmp[$i]->ID] = $recommendedPostsTmp[$i];
            }
        }
    }

    $recommendedPostsLength = count($recommendedPosts);

    // Randomize the order
    shuffle($recommendedPosts);

    if($recommendedPostsLength < 4) {

        $missingItems = 4 - $recommendedPostsLength;

        $recArray = omGetFurtherProductsFromOtherProductCategory($productCategoryIDs, $missingItems);
        $recArrayLength = count($recArray);

        for($j = 0; $j < $recArrayLength; $j++) {
            $recommendedPosts[$recArray[$j]->ID] = $recArray[$j];
        }
    } else {

        // We only need to show 4 items therefore slice away whatever is not needed here.
        $recommendedPosts = array_slice($recommendedPosts, 0, 4);
    }

    // If there are any recommended posts render the section
    if(count($recommendedPosts)) {
        renderRecommendedProductsPanel($postTitle, $recommendedPosts);
    }

};

/**
 * Request further products from another product category if not enough elements could be found.
 * @param $categoryIDs { array } Array of 'product_category' IDs that need to be excluded.
 * @param $missingItems { number } The number of further product items needed.
 * @return mixed { Array } Array of further products.
 */
function omGetFurtherProductsFromOtherProductCategory($categoryIDs, $missingItems) {

    $recommendedPostArgs = array(
        'posts_per_page' => $missingItems,
        'orderby' => 'rand',
        'order' => 'ASC',
        'post_type' => 'product',
        'exclude' => array($categoryIDs),
        'post_parent' => 0,
        'post_status' => 'publish'
    );

    $recommendedPostsTmp = get_posts($recommendedPostArgs);

    return $recommendedPostsTmp;
}

/**
 * Will be called if there are enough recommended products. It'll render the 'recommended products' section.
 * @param $postTitle { String } The title of the product the recommended products will be added to
 * @param $recommendedProducts { Array } Array where the keys are the IDs of products posts.
 */
function renderRecommendedProductsPanel($postTitle, $recommendedProducts) { ?>

    <div class="section-global section-remove-row section-global--color-12 section-recommended-products">
        <div class="container">
            <h3>Love <?php echo $postTitle ?>?</h3>
            <p>You might also like...</p>
            <ul class="list-products-overview">
                <?php foreach($recommendedProducts as $recommendedProduct): ?>
                    <li>
                        <a href="<?php echo get_permalink($recommendedProduct->ID); ?>" class="tile tile--style-3">
                            <div class="image--container">
                                <?php $productThumb = get_post_meta( $recommendedProduct->ID, 'product_overview_image', true ); ?>

                                <?php if(!empty($productThumb['ID'])): ?>
                                    <?php $imageURL = omGetImageUrlByAttachmentID($productThumb['ID'], 'om-package-size');?>
                                    <img src="<?php echo $imageURL ?>" alt="<?php echo $recommendedProduct->post_title; ?>"/>
                                <?php endif; ?>
                            </div>

                            <div class="tile--footer">
                                <p><?php echo $recommendedProduct->post_title; ?></p>
                            </div>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>

<?php } ?>
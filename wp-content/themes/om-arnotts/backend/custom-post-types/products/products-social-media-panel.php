<?php

/**
 * Retrieves the list of social media elements
 * @param $productTitle { String } the title of the product the user is currently viewing.
 * @param $postID { Number } The id of the post
 */
function omGetSocialMediaList($productTitle, $postID) {

    $socialMediaArray = array();

    for($i = 0; $i <= 5; $i++) {
        $socialMediaIcon =  get_post_meta( $postID, 'social_media_icon_'.$i.'_style', true );
        $socialMediaLink =  get_post_meta( $postID, 'social_media_icon_'.$i.'_link', true );

        if($socialMediaIcon && $socialMediaLink) {
            $socialMediaArray[strtolower($socialMediaIcon)] = $socialMediaLink;
        }
    }

    omRenderRecommendedProductsPanel($productTitle, $socialMediaArray);
}

/**
 * Renders the list of social media elements on the screen. The user can choose up to 5 different social media
 * channels [facebook, twitter, youtube, instagram, tumblr]
 * @param $productTitle { String } The title of the product that the user currently views
 * @param $socialMediaArray { Array } Array of social media elements with an icon and link whereas the key is the smaller case string of the social media channel that will be used for styling purposes.
 */
function omRenderRecommendedProductsPanel($productTitle, $socialMediaArray) { ?>

    <?php $socialMediaLinksLength = count($socialMediaArray); ?>

    <!-- Only print the social media section if there are any elements -->
    <?php if($socialMediaLinksLength): ?>
        <div class="section-global section-global--color-6 section-product-social-media">
            <div class="container">
                <div class="social-media--wrapper">
                    <div class="horizontal-gold-line"></div>
                    <ul class="list--product-social-media">
                        <?php foreach($socialMediaArray as $socialMediaElementKey => $socialMediaElementValue): ?>
                            <li>
                                <a class="icon-<?php echo $socialMediaElementKey; ?>" href="<?php echo $socialMediaElementValue; ?>" target="_blank"></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="container">
                <h3>Get Social with <?php echo $productTitle ?>!</h3>
            </div>
        </div>
    <?php endif; ?>
<?php } ?>

<?php

/**
 * Retrieves the list of recipes depending on there product title
 * @param $productTitle { String } the title of the product the user is currently viewing.
 * @param $postID { Number } The id of the post
 */
function omGetRecipeList($postTitle) {

    $recipeArgs = array(
        'posts_per_page'   => '-1',
        'category'         => '',
        'category_name'    => '',
        'post_type'        => 'recipe',
        'post_status'      => 'publish'
    );

    $recipes = new WP_Query($recipeArgs);
    $recipesPosts = $recipes->posts;
    $recipesLength  = count($recipesPosts);
    $j = 0;


    $recipeArray = array();
    for($i = 0; $i < $recipesLength; $i++):
        $recipesPostID = $recipesPosts[$i]->ID;
        $product = get_post_meta($recipesPostID, 'recipes_with_selected_product', true);
        $recipeID = $product['ID'];
        $productParentsArray = get_post_ancestors($recipeID);
        $productParentsArrayLength = count($productParentsArray);

        if(omGetProductTitle($productParentsArrayLength, $productParentsArray, $recipeID) == $postTitle):
            array_push($recipeArray,$recipesPosts[$i]);
        endif;
    endfor;
    $newArrayLength = count($recipeArray);

    if($newArrayLength > 0):

        omRenderProductRecipeTiles($postTitle, $recipeArray, $newArrayLength);

    endif;
} 


function omGetProductTitle($productParentsArrayLength, $productParentsArray, $recipeID) {
    // If it is a child product
    if($productParentsArrayLength) {
        $productTopParentID = $productParentsArray[$productParentsArrayLength - 1];
        $linkToProductPage = get_permalink($productTopParentID);
        $parentTitle = get_the_title($productTopParentID);
    // If it is a parent product
    } else {
        $parentTitle = get_the_title($recipeID);

    }

    return $parentTitle;
}

function omRenderProductRecipeTiles($postTitle, $recipeArray, $newArrayLength) {
    ?>
    <div class="section-global section-product-recipes section-global--color-6">
        <div class="container">
            <div class="row">
                <h3 class="col-sm-12">Recipes with <?php echo $postTitle ?></h3>
            <?php
                if($recipeArray >= 3) {
                    shuffle($recipeArray); 
                }
                for($j = 0; $j < $newArrayLength; $j++): 
                    $recipesPostID = $recipeArray[$j]->ID;
                    $recipes_image = (get_post_meta($recipesPostID, 'recipe_thumbnail_image', true));
                    $recipes_asset_url = omGetPostThumbnailUrl($recipes_image['ID'], null);
                    $recipes_serves = (get_post_meta($recipesPostID, 'recipe_serves', true));
                    $recipe_difficulty = (get_post_meta($recipesPostID, 'recipe_difficulty', true));
                    $recipe_preparation_time = (get_post_meta($recipesPostID, 'recipe_preparation_time', true));
                    $recipes_link = get_permalink($recipesPostID);

                    if($x < 3):
                    ?>
                        <a href="<?php echo $recipes_link ?>" class="recipe-tile--wrapper col-sm-4">
                            <div class="recipe-tile">
                                <img src="<?php echo $recipes_asset_url ?>" alt="<?php echo $recipeArray[$j]->post_title; ?>" />
                                <h3 class="recipe-title"><?php echo $recipeArray[$j]->post_title; ?></h3>
                                <div class="text-wrapper">
                                    <h3><?php echo $recipeArray[$j]->post_title; ?></h3>
                                    <p>Difficulty: <span><?php echo $recipe_difficulty; ?></span></p>
                                    <p>Prep Time: <span><?php echo $recipe_preparation_time; ?></span></p>
                                    <p>Serves: <span><?php echo $recipes_serves; ?></span></p>
                                </div>
                            </div>
                        </a>
                    <?php
                    $x++;
                    endif;   

                endfor;
            ?>
            </div>
            <div class="row">
                <a href="<?php echo get_permalink( 50 ); ?>?recipecat=<?php echo $postTitle ?>" class="col-sm-12 global-link" >
                    VIEW OTHER <?php echo $postTitle ?> RECIPES
                </a>
            </div>
        </div>
    </div>
<?php } ?>
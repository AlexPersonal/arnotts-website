<?php

function omGetProductPromotionPanel($productPostID) {

    $promotionPanelArgs = array(
        'posts_per_page'   => 5,
        'offset'           => 0,
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'product_promotion_pa',
        'post_mime_type'   => '',
        'post_parent'      => '',
        'post_status'      => 'publish',
        'suppress_filters' => true );

    $promotionPanels = get_posts($promotionPanelArgs);
    $promotionPanelsLength = count($promotionPanels);

    for($i=0; $i < $promotionPanelsLength; $i++) {

        $promotionPanelRelationships = get_post_meta( $promotionPanels[$i]->ID, 'product_promotion_panel_relationship', false );
        $promotionPanelRelationshipsLength = count($promotionPanelRelationships);

        // Loop through the promotions panels and get the ones that are attached to the current product
        for($j=0; $j < $promotionPanelRelationshipsLength; $j++) {

            if($promotionPanelRelationships[$j]['ID'] == $productPostID) {

                // Return the promotions panel post that was attached to the productPostID
                omRenderPromotionPanel($promotionPanels[$i]);

                // The requirement expects only 1 promotion panel to be visible, hence break
                // the loop if you already found one promotion panel that was attached to a particular product.
                return;
            }
        }
    }

    return '';
}

function omRenderPromotionPanel($promotionPanelPost) {

    if($promotionPanelPost != NULL):
?>
    <div class="section-global section-global--color-6">
        <div class="container">

            <?php $promotionsPanelID = $promotionPanelPost->ID;  ?>
            <?php $promotionsPanelHeading = get_post_meta( $promotionsPanelID, 'product_promotion_panel_heading', true ); ?>
            <?php $promotionsPanelSubHeading = get_post_meta( $promotionsPanelID, 'product_promotion_sub_heading', true ); ?>
            <?php $promotionsPanelImage = get_post_meta( $promotionsPanelID, 'product_promotion_image', true ); ?>
            <?php $promotionsPanelImageID = $promotionsPanelImage['ID']; ?>
            <?php $promotionsPanelBtnCopy = get_post_meta( $promotionsPanelID, 'product_promotion_button_copy', true ); ?>
            <?php $promotionsPanelInternalLink = get_post_meta( $promotionsPanelID, 'product_promotion_cta_internal_link', true ); ?>
            <?php $promotionsPanelExternalLink = get_post_meta( $promotionsPanelID, 'product_promotion_cta_external_link', true ); ?>

            <div class="product-promotion-panel">

               <?php $attachmendUrl =  omGetPostThumbnailUrl($promotionsPanelImageID, null); ?>

                <div class="product-promotion--background" style="background-image:url('<?php echo $attachmendUrl; ?>')">
                    <div class="copy-layer">
                        <div class="vert-align">

                            <?php if($promotionsPanelHeading): ?>
                                <h3><?php echo $promotionsPanelHeading; ?></h3>
                            <?php endif; ?>

                            <?php if($promotionsPanelSubHeading): ?>
                                <div class="description">
                                    <p><?php echo $promotionsPanelSubHeading; ?></p>
                                </div>
                            <?php endif; ?>

                            <?php echo omReturnButtonIfRequiredWithInternalLinkID($promotionsPanelInternalLink, $promotionsPanelExternalLink,$promotionsPanelBtnCopy); ?>

                        </div>
                    </div>
                </div><!-- .product-promotion-panel -->
            </div>
        </div>
    </div>

<?php
    endif;
    }
?>
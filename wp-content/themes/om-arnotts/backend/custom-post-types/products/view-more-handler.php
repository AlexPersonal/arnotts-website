<?php

class ViewMoreProducts {

    public function __construct()
    {


        add_action( 'wp_ajax_nopriv_view-more-products', array( &$this, 'getAllProducts' ) );
        add_action( 'wp_ajax_view-more-products', array( &$this, 'getAllProducts' ) );
        add_action( 'init', array( &$this, 'init' ) );

    }

    public function init()
    {
       wp_localize_script( 'main', 'ViewAllProducts', array(
            'ajaxurl' => admin_url( 'admin-ajax.php' , is_ssl() ? 'admin' : 'http' ),
            'nonce' => wp_create_nonce( 'view-more-products-nonce' )
        ) );
    }

    /**
     * Gets the next products with an offset of 4 and returns the result as a JSON back to FED.
     */
    public function getAllProducts()
    {
        if ( ! isset( $_POST['nonce'] ) || ! wp_verify_nonce( $_POST['nonce'], 'view-more-products-nonce' ) )
            die ( 'Invalid Nonce' );

        if(isset($_POST['productCategoryID'])) {

            // Send header content type
            header( "Content-Type: application/json" );

            // Prepare the products results array
            $products = omGetResidingProducts($_POST['productCategoryID']);
            $newProducts = array();

            for($i = 0; $i < count($products); $i++) {


                $productThumb = get_post_meta( $products[$i]->ID, 'product_overview_image', true );
                $imageURL = omGetImageUrlByAttachmentID($productThumb['ID'], 'om-package-size');
                $image = '';

                if($imageURL) {
                    $image =  '<img src="'. $imageURL .'" alt="'. $products[$i]->post_title .'"/>';
                }


                $newProducts[$i] = array(
                    'ID' => $products[$i]->ID,
                    'post_title' => $products[$i]->post_title,
                    'post_permalink' => get_permalink($products[$i]->ID),
                    'post_thumb' => $image
                );
            }

            $productsResult = array(
                'success' => true,
                'productCategoryID' => $_POST['productCategoryID'],
                'products' => $newProducts,
                'time' => time()
            );

            // Prepare JSON
            echo json_encode( $productsResult );
        }

        exit;
    }

}

$ajaxExample = new ViewMoreProducts();




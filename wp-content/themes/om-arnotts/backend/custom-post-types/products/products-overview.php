<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 17/01/2015
 * Time: 1:59 PM
 */

/**
 * Gets the 1st level of all product categories.
 * @return array
 */
function omGetAll1stLevelProductCategories() {
    $args = array(
        'type'                     => '',
        'child_of'                 => 0,
        'parent'                   => 0,
        'orderby'                  => 'name',
        'order'                    => 'ASC',
        'hide_empty'               => 1,
        'hierarchical'             => 0,
        'exclude'                  => '',
        'include'                  => '',
        'number'                   => '',
        'taxonomy'                 => 'product_category',
        'pad_counts'               => false
    );

    $categories = get_categories($args);

    return $categories;
}

/**
 * @param $category {Array}
 * @return array
 */
function omGetProducts($category) {

    $products = get_posts(array(
        'posts_per_page'   => 1000,
        'offset'           => 0,
        'tax_query' => array(
            array(
                'taxonomy' => 'product_category',
                'field' => 'id',
                'terms' => array($category->cat_ID)
            )),
        'exclude'          => '',
        'post_type'        => 'product',
        'post_parent'      => 0,
        'post_status'      => 'publish'
    ));

    return $products;
}

function omGetProductsBySingleCategoryID($categoryID) {
    // Favourites doesn't only show the parents
    if($categoryID == 44) {

        $products = get_posts(array(
            'posts_per_page'   => 1000,
            'offset' => 0,
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_category',
                    'field' => 'id',
                    'terms' => array($categoryID)
                )),
            'exclude'          => '',
            'post_type'        => 'product',
            'post_status'      => 'publish'
        ));

        // Only show the 1st level (parents) of a group of categories
    } else {
        $products = get_posts(array(
            'posts_per_page'   => 1000,
            'offset'           => 0,
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_category',
                    'field' => 'id',
                    'terms' => array($categoryID)
                )),
            'exclude'          => '',
            'post_type'        => 'product',
            'post_parent'      => 0,
            'post_status'      => 'publish'
        ));
    }

    return $products;
}

/**
 * Fetches residing products in dependance to a given category ID.
 *
 * @param $categoryID { Number } The id of the category that residing products need to be fetched from.
 * @return mixed { Array } Array with the rest of the products.
 */
function omGetResidingProducts($categoryID) {

    $products = get_posts(array(
        'posts_per_page'   => 1000,
        'offset' => 4,
        'post_parent' => 0,
        'tax_query' => array(
            array(
                'offset' => 4,
                'taxonomy' => 'product_category',
                'field' => 'id',
                'terms' => array($categoryID)
            )),
        'exclude'          => '',
        'post_type'        => 'product',
        'post_status'      => 'publish',
        'suppress_filters' => true
    ));


    return $products;
}
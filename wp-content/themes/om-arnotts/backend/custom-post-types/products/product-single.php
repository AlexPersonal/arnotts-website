<?php

/**
 * @param $postID { number } The id of the parent product that we need to get the child products from.
 * @return mixed { array } An array of elements or empty array of child products.
 */
function omGetProductSubCategoriesByProductGroupID($postID) {

    $childProductsArgs = array(
        'parent' => $postID,
        'child_of' => $postID,
        'hierarchical' => 0,
        'sort_order' => 'ASC',
        'sort_column' => 'menu_order',
        'post_type' => 'product' );

    $childProducts = get_pages($childProductsArgs);

    return $childProducts;
}

/**
 * Get all products from a Subcategory.
 *
 * @param $subCategoryID { number } The ID of the $subCategory that we want the products from.
 * SubCategories can be (e.g. for Vita-Wheat: 'Crispbread', 'Cracker', 'Lunch Slices')
 *
 * @return array { array } Array of products or empty array.
 */
function omGetAllProductsByProductSubCategoryID($subCategoryID) {

    $productsBySubCategoryArgs = array(
        'child_of' => $subCategoryID,
        'hierarchical' => 0,
        'sort_column' => 'menu_order',
        'sort_order' => 'ASC',
        'post_type' => 'product' );

    return get_pages($productsBySubCategoryArgs);
}

/**
 * Returns the fitting bootstrap css class.
 *
 * @param $numberOfPackages { Number } The number of total images
 * @return string { String } Returns the bootstrap css class that defines the number of columns
 */
function omGetProductPackageImageListClass($numberOfPackages) {

    switch($numberOfPackages):

        case $numberOfPackages % 4 == 0:
            $cssClass = 'col-sm-3';
            break;
        case $numberOfPackages % 3 == 0:
            $cssClass = 'col-sm-4';
            break;
        case $numberOfPackages % 2 == 0 && $numberOfPackages > 2:
            $cssClass = 'col-sm-3';
            break;
        case $numberOfPackages % 2 == 0 || $numberOfPackages == 1:
            $cssClass = 'col-sm-6';
            break;
        default:
            $cssClass = 'col-sm-4';
    endswitch;

    return $cssClass;
}

function omGetNutritionalInformationLink($postID) {

    $selectedPDF = get_post_meta( $postID, 'nutritional_information', true );

    if($selectedPDF) {
        $pdfLink = omGetPDFFromCustomizerPanel($selectedPDF);
    } else {
        return;
    }

    if($pdfLink) {
    ?>
    <div class="container">
        <div class="section-nutritional-information"><a class="global-link" href="<?php echo $pdfLink ?>" target="_blank" download="">Nutritional Information</a></div>
    </div>
<?php
    }
}

function omGetPDFFromCustomizerPanel($pdfReference) {

    $linkToPDF = '';

    switch($pdfReference) {
        case 'PDF 1':
            $linkToPDF = get_theme_mod('omarnotts_nutritional_information_file_1');
            break;
        case 'PDF 2':
            $linkToPDF = get_theme_mod('omarnotts_nutritional_information_file_2');
            break;
        default:
    }

    return $linkToPDF;
}
?>
<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 6/02/2015
 * Time: 12:07 PM
 */

/** Sets the number of posts that the user wants to see per page
 * @return int { Number } Sets the number of visible posts when doing the pagination
 */
function omGetMediaLibraryPostsPerPage() {
    return 6;
}



/**
 * Returns the currently selected filetype.
 * @return mixed { String } Empty string or the value for the filetype that the user searched for on the media library filter form.
 */
function omGetMediaLibraryFileType() {
    return get_query_var('filetype');
}

/**
 * @return mixed { String } Empty string or value for the year that the user searched for on the
 * media library filter form.
 */
function omGetMediaLibraryYear() {
    return get_query_var('mlyear');
}

/**
 * Used for the filter form on the media library page.
 * @return string { String } Returns the string used as key for the keyword request.
 */
function omGetMediaLibraryKeywordParamKey() {
    $omFilterKeywordsParam = 'keywords';

    return $omFilterKeywordsParam;
}

/**
 * Returns the value from the keyword query parameter.
 * @return mixed { String } Empty or the value for the key 'keyword'
 */
function omGetMediaLibraryKeywords() {
    return get_query_var(''.omGetMediaLibraryKeywordParam());
}


/**
 * Returns a requested category ID or an empty array.
 * @return mixed { String } Null or The ID of the currently selected category.
 */
function omGetMediaLibraryKeywordParam() {

    $catID = get_query_var(omGetMediaLibraryKeywordParamKey());

    return $catID;
}

function getAllMediaLibraryPosts() {
    $mediaLibraryArgs = array(
        'posts_per_page'   => '-1',
        'category'         => '',
        'category_name'    => '',
        'post_type'        => 'media_library',
        'post_status'      => 'publish'
    );

    $mediaLibraries = new WP_Query($mediaLibraryArgs);

    if($mediaLibraries->posts) {

        $mediaLibraries = $mediaLibraries->posts;

    } else {
        $mediaLibraries = null;
    }


    return $mediaLibraries;
}

/**
 * Fetches the MediaLibrary custom post types.
 * @return mixed
 */
function omGetMediaLibraryPosts() {

    $paged = omGetCurrentPage();
    $year = omGetMediaLibraryYear();
    $filetype = omGetMediaLibraryFileType();
    $s = get_query_var('keywords');

    $mediaLibraryArgs = array(
        'posts_per_page'   => '-1',
        'paged'            => $paged,
        'category'         => '',
        'category_name'    => '',
        'post_type'        => 'media_library',
        'post_status'      => 'publish',
        's' => $s,
        'year' => $year
    );

    $mediaLibraries = new WP_Query($mediaLibraryArgs);

    if($mediaLibraries->posts) {

        if($filetype != '') {

            // If a filte type is set, filter the result as it is a relationship field.
            $mediaLibraries = omFilterArrayByFileType($mediaLibraries->posts, $filetype);

        } else {
            $mediaLibraries = $mediaLibraries->posts;
        }

    } else {
        $mediaLibraries = null;
    }

    return $mediaLibraries;
}

/**
 * Takes the array of all mediaLibraryPosts, filters it by the selected file and returns a new array
 * containing only the posts with the previously selected filetype.
 *
 * @param $mediaLibraryQueryResult { Array } Array of mediaLibrary posts.
 * @param $filetype { String } The filetype that the user had selected to filter for.
 * @return array { Array } Filtered array only containing the media library posts that match the selected file type.
 */
function omFilterArrayByFileType($mediaLibraryQueryResult, $filetype)
{

    $result = array();

    foreach ($mediaLibraryQueryResult as $mediaLibraryPost) {

        $mediaLibraryPostID = $mediaLibraryPost->ID;
        $media_library_asset = (get_post_meta($mediaLibraryPostID, 'media_library_asset', true));

        $media_library_asset_url = $media_library_asset['guid'];
        $media_library_file_type = wp_check_filetype($media_library_asset_url);


        if(array_key_exists('ext', $media_library_file_type)) {
            $assetFileType = $media_library_file_type['ext'];

            if($assetFileType === $filetype) {

                $result[] = $mediaLibraryPost;
            }
        }
    }

    return $result;

}

/**
 * Used for the 'Choose Filetype' select box on the media library page. This will return the available file
 * types.
 *
 * @param $mediaLibraryPosts { array } Containinn the media library posts
 * @return array { Array } Returns either an empty array or results where the array keys represent the available file types.
 */
function omGetAvailableMediaLibraryAssetFiletypesAndYears($mediaLibraryPosts) {
    $mediaLibraryPostsLength = count($mediaLibraryPosts);
    $result = array();

    for($i = 0; $i < $mediaLibraryPostsLength; $i++) {

        $mediaLibraryPostID = $mediaLibraryPosts[$i]->ID;
        $media_library_asset = (get_post_meta($mediaLibraryPostID, 'media_library_asset', true));
        $mediaLibraryPostYear = mysql2date('Y', $mediaLibraryPosts[$i]->post_date);
        
        $media_library_asset_url = $media_library_asset['guid'];
        $media_library_file_type = wp_check_filetype($media_library_asset_url);

        if($mediaLibraryPostYear) {
            $result['years'][$mediaLibraryPostYear] = $mediaLibraryPosts[$i];
        }

        if(array_key_exists('ext',$media_library_file_type)) {
            $result['filetypes'][$media_library_file_type['ext']] = $mediaLibraryPosts[$i];
        }
    }

    return $result;
}

/**
 * Grabs the values and prints out a formatted list of used search parameters.
 *
 * @param $keywords { String } The keywords string the user had searched for.
 * @param $fileType { String } The filetype the user had searched for.
 * @param $year { String } The year YYYY that the user had filtered for.
 * @return string { String } Formatted list of search parameters.
 */
function omGetMediaLibrarySearchParameterList($keywords, $fileType, $year) {
    $searchParams = array();

    if($keywords) {
        $searchParams[] = $keywords;
    }

    if($fileType) {
        $searchParams[] = $fileType;
    }

    if($year) {
        $searchParams[] = $year;
    }

    $searchParameters = '<span>'.implode('</span> - <span>', $searchParams).'</span>';

    return $searchParameters;
}

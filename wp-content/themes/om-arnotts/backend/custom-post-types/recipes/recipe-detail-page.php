<?php
/**
 * Provides the recipe cheat sheet.
 * @param $recipeID { Integer } The number of the current post (custom post type recipe)
 * @return array { Array } 2 dimensional array where the key is the info to show containing a title, icon and the content to show.
 */
function omGetRecipeCheatSheet($recipeID) {

    $recipeCheatSheetArray = array();

    // Grab data from the recipe post type
    $recipeDifficulty = get_post_meta($recipeID, 'recipe_difficulty', true);
    $recipeArnottsProduct = get_post_meta($recipeID, 'recipe_arnotts_product', true);
    $recipeCookingTime = get_post_meta($recipeID, 'recipe_cooking_time', true);
    $recipePreparationTime = get_post_meta($recipeID, 'recipe_preparation_time', true);
    $recipeChillTime = get_post_meta($recipeID, 'recipe_chill_time', true);
    $recipeServes = get_post_meta($recipeID, 'recipe_serves', true);

    if($recipeDifficulty) {
        $recipeCheatSheetArray['difficulty']['title'] = 'Difficulty';
        $recipeCheatSheetArray['difficulty']['icon-type'] = 'difficulty';
        $recipeCheatSheetArray['difficulty']['content'] = $recipeDifficulty;
    }

    if($recipeArnottsProduct) {
        $recipeCheatSheetArray['arnottsproduct']['title'] = 'Arnott\'s product';
        $recipeCheatSheetArray['arnottsproduct']['icon-type'] = 'arnotts-product';
        $recipeCheatSheetArray['arnottsproduct']['content'] = $recipeArnottsProduct;
    }

    if($recipeCookingTime) {
        $recipeCheatSheetArray['cooking-time']['title'] = 'Cooking time';
        $recipeCheatSheetArray['cooking-time']['icon-type'] = 'cooking-time';
        $recipeCheatSheetArray['cooking-time']['content'] = $recipeCookingTime;
    }

    if($recipePreparationTime) {
        $recipeCheatSheetArray['prep-time']['title'] = 'Prep time';
        $recipeCheatSheetArray['prep-time']['icon-type'] = 'prep-time';
        $recipeCheatSheetArray['prep-time']['content'] = $recipePreparationTime;
    }

    if($recipeChillTime) {
        $recipeCheatSheetArray['chill-time']['title'] = 'Chill time';
        $recipeCheatSheetArray['chill-time']['icon-type'] = 'chill-time';
        $recipeCheatSheetArray['chill-time']['content'] = $recipeChillTime;
    }

    if($recipeServes) {
        $recipeCheatSheetArray['serves']['title'] = 'Serves';
        $recipeCheatSheetArray['serves']['icon-type'] = 'serves';
        $recipeCheatSheetArray['serves']['content'] = $recipeServes . '+';
    }

    return $recipeCheatSheetArray;
}

/**
 * Returns the Product recipe's cross link tile
 * @param $recipeID { integer } The id of the recipe that is currently being viewed.
 */
function omGetRecipeProductCrossLinkTile($recipeID) {
    // Grab the title for the 'featured product tile'
    $crossLinkTitle = get_post_meta($recipeID, 'recipe_product_cross_link_title', true);

    // Grab the product that was hooked up with this recipe
    $product = get_post_meta($recipeID, 'recipe_product_cross_link_tile', true);
    $productID = $product['ID'];

    // Get the products parent
    $productParentsArray = get_post_ancestors($productID);
    $productParentsArrayLength = count($productParentsArray);

    // If it is a child product
    if($productParentsArrayLength) {
        $productTopParentID = $productParentsArray[$productParentsArrayLength - 1];
        $linkToProductPage = get_permalink($productTopParentID);
        // Get the product image to show on this 'Featured Product' tile
        $productImage = get_post_meta($productID, 'product_package_image', true);
        $productImageUrl = omGetPostThumbnailUrl($productImage['ID'],'om-package-size-medium');

    // If it is a parent product
    } else {
        $linkToProductPage = get_permalink($productID);

        // Get the product image to show on this 'Featured Product' tile
        $productImage = get_post_meta( $productID, 'product_overview_image', true );
        $productImageUrl = omGetPostThumbnailUrl($productImage['ID'], 'om-media-centre-size');

    }

    // Get the copy that needs to be shown in the grey area beneath the product image.
    $recipeCrossLinkSubHeading = get_post_meta($recipeID, 'recipe_product_cross_link_tile_sub_headline', true);

    // Only printout the sidebar if these values are set.
    if($crossLinkTitle && $productImage && $recipeCrossLinkSubHeading && $linkToProductPage) :
?>
    <ul class="list-products-overview">
        <li>
            <h3><?php echo $crossLinkTitle ?></h3>
            <a href="<?php echo $linkToProductPage;?>"
               class="tile tile--style-3">
                <div class="image--container">
                    <img src="<?php echo $productImageUrl; ?>" alt="<?php #echo $crossLinkTitle ?>"/>
                </div>

                <div class="tile--footer">
                    <p><?php echo $recipeCrossLinkSubHeading; ?></p>
                </div>
            </a>
        </li>
    </ul>
<?php
    endif;

}

/**
 * Not being used: Just in case client needs to have this tile more generic
 * Returns the cross link tile if there was one attached to the recipe post.
 * @param $recipeID { Integer } The ID of the current recipe post.
 */
function omGetRecipeGeneralCrossLinkTile($recipeID) {
    $recipeCrosslinkTile = get_post_meta($recipeID, 'recipe_cross_link_tile', true);



    $recipeCrossLinkSubHeading = get_post_meta($recipeID, 'recipe_cross_link_tile_sub_headline', true);
    $recipeCrosslinkTileID = $recipeCrosslinkTile['ID'];

    $crossLinkBackgroundImage = get_post_meta($recipeCrosslinkTileID, 'cross_link_background_image', true);
    $crossLinkBackgroundImageID = $crossLinkBackgroundImage['ID'];
    $crossLinkBackgroundImage = omGetPostThumbnailUrl($crossLinkBackgroundImageID,'tablet');

    $crossLinkTitle = get_post_meta($recipeCrosslinkTileID, 'cross_link_title', true);
    $crossLinkTileInternalLink = get_post_meta($recipeCrosslinkTileID, 'cross_link_internal_link', true);
    $crossLinkTileExternalLink = get_post_meta($recipeCrosslinkTileID, 'cross_link_external_link', true);

?>
    <ul class="list-products-overview">
        <li>
            <h3><?php echo $crossLinkTitle ?></h3>
            <a href="<?php echo omReturnLink($crossLinkTileInternalLink, $crossLinkTileExternalLink); ?>" class="tile tile--style-3">
                <div class="image--container">
                    <img src="<?php echo $crossLinkBackgroundImage; ?>" alt="<?php echo $crossLinkTitle ?>"/>
                </div>

                <div class="tile--footer">
                    <p><?php echo $recipeCrossLinkSubHeading?></p>
                </div>
            </a>
        </li>
    </ul>
<?php
}

/**
 * Takes whatever has been added to the WYSIWYG editor of the currently viewed recipe post type for the 'Recipe Ingredients'.
 * @param $recipeID { Integer } The id of the currently viewed recipe
 * @return mixed { Boolean | String } returns false if there was nothing added to the WYSIWYG editor otherwise it'll return the content which is a string.
 */
function omGetRecipeIngredientsList($recipeID) {

    $ingredients = get_post_meta($recipeID, 'recipe_ingredients', true);

    $ingredients = (strlen($ingredients) == 0) ? false : $ingredients;

    return $ingredients;
}

/**
 * Takes whatever has been added to the WYSIWYG editor of the currently viewed recipe post type for the 'Recipe Method'.
 * @param $recipeID { Integer } The id of the currently viewed recipe
 * @return mixed { Boolean | String } returns false if there was nothing added to the WYSIWYG editor otherwise it'll return the content which is a string.
 */
function omGetRecipeHowToDoItList($recipeID) {
    $method = get_post_meta($recipeID, 'recipe_method', true);

    $method = (strlen($method) == 0) ? false : $method;

    return $method;

}

/**
 * Takes whatever has been added to the WYSIWYG editor of the currently viewed recipe post type for the 'Recipe Tips'.
 * @param $recipeID { Integer } The id of the currently viewed recipe
 * @return mixed { Boolean | String } returns false if there was nothing added to the WYSIWYG editor otherwise it'll return the content which is a string.
 */
function omGetRecipeTips($recipeID) {
    $method = get_post_meta($recipeID, 'recipe_tips', true);

    $method = (strlen($method) == 0) ? false : $method;

    return $method;

}

/**
 * Takes the currently visible recipe, looks which type it was ticked for 'recommended_recipes_relation_type'. Then this
 * section will be taken to find other recipes that were set to have the same recipe relation type.
 *
 * @param $postTitle {}
 * @param $recipeID
 */
function omGetRecommendedRecipesByPostMetaRecipeRelation($postTitle, $recipeID) {

    $recipeRelationType = get_post_meta( $recipeID, 'recommended_recipes_relation_type', true );

    $args = array(
        'post_type' => 'recipe',
        'post_status' => 'publish',
        'meta_query' => array(
            array(
                'key' => 'recommended_recipes_relation_type',
                'value' => $recipeRelationType,
                'compare' => '='
            )
        ),
        'exclude' => $recipeID,
        'posts_per_page' => 1000,
        'suppress_filters' => true

    );
    $recommendedPostsTmp = get_posts($args);

    // Only add this array if it's length is > 0
    if(count($recommendedPostsTmp)) {

        $recommendedPostsLength = count($recommendedPostsTmp);

        for($i = 0; $i < $recommendedPostsLength; $i++) {
            $recommendedPostID = $recommendedPostsTmp[$i]->ID;

            // Unfortunately the meta_query returns all products if there is no matching meta_value
            if(get_post_meta($recommendedPostID, 'recommended_recipes_relation_type', true)) {
                $recommendedPosts[$recommendedPostID] = $recommendedPostsTmp[$i];
            }
        }
    }

    $recommendedPostsLength = count($recommendedPosts);

    if($recommendedPostsLength) {

        // Randomize the order
        shuffle($recommendedPosts);

        // Only 3 max
        if($recommendedPostsLength > 3) {
            // We only need to show 3 items therefore slice away whatever is not needed here.
            $recommendedPosts = array_slice($recommendedPosts, 0, 3);
        }

        // If there are any recommended posts render the section
        if(count($recommendedPosts)) {
            renderRelatedRecipesPanel($postTitle, $recommendedPosts);
        }
    }
}

/**
 * Will be called on cooking time, foor google rich snippets
 */
function time_to_iso8601_duration($time) {
    $convertedTime = strtotime($time, 0);
    $units = array(
        "Y" => 365*24*3600,
        "D" =>     24*3600,
        "H" =>        3600,
        "M" =>          60,
        "S" =>           1,
    );

    $str = "P";
    $istime = false;

    foreach ($units as $unitName => &$unit) {
        $quot  = intval($convertedTime / $unit);
        $convertedTime -= $quot * $unit;
        $unit  = $quot;
        if ($unit > 0) {
            if (!$istime && in_array($unitName, array("H", "M", "S"))) { // There may be a better way to do this
                $str .= "T";
                $istime = true;
            }
            $str .= strval($unit) . $unitName;
        }
    }

    return $str;
}


/**
 * Will be called if there are any related recipes to be found. It'll render the 'related recipes' section.
 * @param $postTitle { String } The title of the product the recommended products will be added to
 * @param $recommendedRecipes { Array } Array where the keys are the IDs of products posts.
 */
function renderRelatedRecipesPanel($postTitle, $recommendedRecipes) { ?>

    <div class="section-global section-remove-row section-global--color-6 section-recommended-products">
        <div class="container">
            <h3>Related Recipes</h3>

            <ul class="related-recipes">

                <?php foreach($recommendedRecipes as $recommendedRecipe): ?>
                 <li class="recipe-tile--wrapper col-sm-4">

                    <?php
                    $recipeID = $recommendedRecipe->ID;
                    $recipeThumb = get_post_meta( $recipeID, 'recipe_thumbnail_image', true );
                    $recipe_excerpt = get_post_meta($recipeID, 'recipe_homepage_excerpt', true);
                    $recipes_serves = get_post_meta($recipeID, 'recipe_serves', true);
                    $recipe_difficulty = get_post_meta($recipeID, 'recipe_difficulty', true);
                    $recipe_preparation_time = get_post_meta($recipeID, 'recipe_preparation_time', true);
                    ?>

                    <?php if(!empty($recipeThumb['ID'])): ?>

                        <?php
                            if(function_exists('is_tablet') && is_tablet()) {
                                $imageURL = omGetImageUrlByAttachmentID($recipeThumb['ID'], 'om-tablet');
                            } else {
                                $imageURL = omGetImageUrlByAttachmentID($recipeThumb['ID'], 'om-related-recipe-size');
                            }

                        ?>


                        <a href="<?php echo get_permalink($recipeID); ?>" class="recipe-tile cross-tile--wrapper">

                            <img src="<?php echo $imageURL; ?>" alt="<?php echo $recommendedRecipe->post_title; ?>" />
                            <h3 class="recipe-title"><?php echo $recommendedRecipe->post_title; ?></h3>
                            <div class="text-wrapper">
                                <h3><?php echo $recommendedRecipe->post_title; ?></h3>
                                <p>Difficulty: <span><?php echo $recipe_difficulty; ?></span></p>
                                <p>Prep Time: <span><?php echo $recipe_preparation_time; ?></span></p>
                                <p>Serves: <span><?php echo $recipes_serves; ?></span></p>
                            </div>
                        </a>

                    <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>

<?php } ?>

<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 6/02/2015
 * Time: 12:07 PM
 */

/** Sets the number of posts that the user wants to see per page
 * @return int { Number } Sets the number of visible posts when doing the pagination
 */
function omGetMediaCentrePostsPerPage() {
    return 6;
}


/**
 * @return mixed { String } Empty string or value for the year that the user searched for on the
 * media library filter form.
 */
function omGetMediaCentreYear() {

    $mcYear = (sanitize_text_field(get_query_var('mcyear')));

    return $mcYear;
}

/**
 * @return mixed { String } Empty string or value for the newstype that the user searched for on the
 * media centre filter form.
 */
function omGetNewsType() {
    return get_query_var('newstype');
}

/**
 * Used for the filter form on the media library page.
 * @return string { String } Returns the string used as key for the keyword request.
 */
function omGetMediaCentreKeywordParamKey() {
    $omFilterKeywordsParam = 'keywords';

    return $omFilterKeywordsParam;
}

/**
 * Returns the value from the keyword query parameter.
 * @return mixed { String } Empty or the value for the key 'keyword'
 */
function omGetMediaCentreKeywords() {
    return get_query_var(''.omGetMediaCentreKeywordParam());
}


/**
 * Returns a requested category ID or an empty array.
 * @return mixed { String } Null or The ID of the currently selected category.
 */
function omGetMediaCentreKeywordParam() {

    $catID = get_query_var(omGetMediaCentreKeywordParamKey());

    return $catID;
}

function getAllMediaCentrePosts() {
    $mediaCentreArgs = array(
        'posts_per_page'   => '-1',
        'category'         => '',
        'category_name'    => '',
        'post_type'        => 'media_centre',
        'post_status'      => 'publish'
    );

    $mediaCentres = new WP_Query($mediaCentreArgs);

    if($mediaCentres->posts) {

        $mediaCentres = $mediaCentres->posts;

    } else {
        $mediaCentres = null;
    }

    return $mediaCentres;
}

/**
 * Fetches the MediaLibrary custom post types.
 * @return mixed
 */
function omGetMediaCentrePosts() {

    $paged = omGetCurrentPage();
    $year = omGetMediaCentreYear();
    $newsType = sanitize_text_field(get_query_var('newstype'));
    $s = sanitize_text_field(get_query_var('keywords'));

    if(omGetNewsType()) {
        $mediaCentreArgs = array(
            'posts_per_page'   => '-1',
            'paged'            => $paged,
            'category'         => '',
            'category_name'    => '',
            'post_type'        => 'media_centre',
            'post_status'      => 'publish',
            's' => $s,
            'year' => $year,
            'tax_query' => array(
                array(
                    'taxonomy' => 'media_centre_category',
                    'field'    => 'slug',
                    'terms'    => ''.$newsType,
                ),
            )
        );

    } else {
        $mediaCentreArgs = array(
            'posts_per_page'   => '-1',
            'paged'            => $paged,
            'category'         => '',
            'category_name'    => '',
            'post_type'        => 'media_centre',
            'post_status'      => 'publish',
            's' => $s,
            'year' => $year
        );
    }


    $mediaCentres = new WP_Query($mediaCentreArgs);

    if($mediaCentres->posts) {

        $mediaCentres = $mediaCentres->posts;

    } else {
        $mediaCentres = null;
    }

    return $mediaCentres;
}

/**
 * Grabs the values and prints out a formatted list of used search parameters.
 *
 * @param $keywords { String } The keywords string the user had searched for.
 * @param $newsType { String } The news type the user had searched for.
 * @param $year { String } The year YYYY that the user had filtered for.
 * @return string { String } Formatted list of search parameters.
 */
function omGetMediaCentreSearchParameterList($keywords, $newsType, $year) {
    $searchParams = array();

    if($keywords) {
        $searchParams[] = $keywords;
    }

    if($newsType) {
        $searchParams[] = $newsType;
    }

    if($year) {
        $searchParams[] = $year;
    }

    $searchParameters = '<span>'.implode('</span> - <span>', $searchParams).'</span>';

    return $searchParameters;
}


/**
 * Used for the 'Choose new type' and 'Choose year' select box on the media centre page. This will return the available years
 * and news types.
 *
 * @param $mediaCentrePosts { array } Containing the media centre posts
 * @return array { Array } Returns either an empty array or results where the array keys represent the available file types.
 */
function omGetAvailableMediaCentreTypeAndYears($mediaCentrePosts) {
    $mediaCentrePostsLength = count($mediaCentrePosts);
    $result = array();

    for($i = 0; $i < $mediaCentrePostsLength; $i++) {

        $mediaLibraryPostYear = mysql2date('Y', $mediaCentrePosts[$i]->post_date);

        if($mediaLibraryPostYear) {
            $result['years'][$mediaLibraryPostYear] = $mediaCentrePosts[$i];
        }
    }

    return $result;
}

/**
 * Retuns a list of terms that were saved in the custom taxonomy 'faq_category'
 */
function omGetMediaCentreCategoriesSelect() {

    $selectedCategorySlug = get_query_var('newstype');//omGetCurrentlySelectedCategorySlug();

    // Get all Terms that belong to the faq taxonomy
    $catArgs = array();
    $mediaCentreCategoryTerms = get_terms('media_centre_category', $catArgs);
    
    echo '<select name="newstype">';

    if(!count($mediaCentreCategoryTerms)) {
        echo '<option value="">No Categories available</option>';

    } else {
        echo '<option value="">All Categories</option>';

        foreach($mediaCentreCategoryTerms as $mediaCentreCategoryTerm) {

            $termName = strtolower(trim($mediaCentreCategoryTerm->name));

            if($termName == $selectedCategorySlug) {

                echo '<option selected="selected" value="'.sanitize_text_field($termName).'">'.$mediaCentreCategoryTerm->name.'</option>';

            } else {

                echo '<option value="'. sanitize_text_field($termName) .'">'.$mediaCentreCategoryTerm->name.'</option>';
            }
        }
    }

    echo '</select>';

}
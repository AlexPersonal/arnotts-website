<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 3/02/2015
 * Time: 2:02 PM
 */

function omGetSidbarCrossLinkTiles($postID) {
    $crossLinkTilesSidebar1 = get_post_meta($postID, 'cross_link_tiles_sidebar_1', false);
    $crossLinkTilesSidebar2 = get_post_meta($postID, 'cross_link_tiles_sidebar_2', false);

    $crossLinkTileSidebar1Length = count($crossLinkTilesSidebar1);


    if($crossLinkTilesSidebar1 && $crossLinkTilesSidebar1[0]) : ?>

        <?php  for ($i = 0; $i < $crossLinkTileSidebar1Length; $i++) : ?>
            <ul class="list-contacts list-contacts--sidebar">

            <?php
                $contactUsPost = $crossLinkTilesSidebar1[$i];
                $conatct_us_post_ID = $contactUsPost['ID'];
                $contact_us_type = (get_post_meta($conatct_us_post_ID, 'contact_type', true));
                $contact_us_style = (get_post_meta($conatct_us_post_ID, 'contact_style', true));

                $contact_us_type = ($contact_us_type) ? $contact_us_type : 'consumer-care';
                $contact_us_style = ($contact_us_style) ? $contact_us_style : 'style-1';
            ?>

                <li>
                    <div class="tile tile--<?php echo $contact_us_style; ?> tile--<?php echo $contact_us_type; ?>">
                        <h3><span><?php echo $contactUsPost['post_title']; ?></span></h3>
                        <?php echo $contactUsPost['post_content']; ?>
                    </div>
                </li>
            </ul>
        <?php endfor; ?>
    <?php endif; ?>


    <?php if($crossLinkTilesSidebar2 && $crossLinkTilesSidebar2[0]) : ?>

        <?php  for ($i = 0; $i < $crossLinkTileSidebar1Length; $i++) : ?>
            <div class="cross-tile--wrapper">

                <?php
                    $crossLinkBackgroundImage = get_post_meta($crossLinkTilesSidebar2[$i]['ID'], 'cross_link_background_image', true);
                    $crossLinkTitle = get_post_meta($crossLinkTilesSidebar2[$i]['ID'], 'cross_link_title', true);
                    $crossLinkTileInternalLink = get_post_meta($crossLinkTilesSidebar2[$i]['ID'], 'cross_link_internal_link', true);
                    $crossLinkTileExternalLink = get_post_meta($crossLinkTilesSidebar2[$i]['ID'], 'cross_link_external_link', true);
                ?>

                <a href="<?php echo omReturnLink($crossLinkTileInternalLink, $crossLinkTileExternalLink); ?>" class="cross-tile cross-tile--default"
                   style="background-image:url('<?php echo omGetPostThumbnailUrl($crossLinkBackgroundImage['ID'], 'om-tablet'); ?>')">
                    <span class="overlay"></span>

                    <h3><?php echo $crossLinkTitle; ?></h3>
                </a>
            </div>
        <?php endfor; ?>
    <?php endif; ?>
<?php
}

function omGetBottomCrossLinkTiles($postID)
{

    $crossLinkBottom = get_post_meta($postID, 'cross_link_tiles_bottom', false);
    $crossLinkTileLength = count($crossLinkBottom);

    if ($crossLinkTileLength && $crossLinkBottom[0]):
        ?>
            <div class="section-global section-global--color-6">
                <div class="container">
                    <div class="row">
                        <?php
                        for ($i = 0; $i < $crossLinkTileLength; $i++) :

                            ?>
                            <div class="col-md-6 cross-tile--wrapper">
                                <?php
                                    $crossLinkBackgroundImage = get_post_meta($crossLinkBottom[$i]['ID'], 'cross_link_background_image', true);
                                    $crossLinkTitle = get_post_meta($crossLinkBottom[$i]['ID'], 'cross_link_title', true);
                                    $crossLinkTileInternalLink = get_post_meta($crossLinkBottom[$i]['ID'], 'cross_link_internal_link', true);
                                    $crossLinkTileExternalLink = get_post_meta($crossLinkBottom[$i]['ID'], 'cross_link_external_link', true);

                                ?>


                                <a href="<?php echo omReturnLink($crossLinkTileInternalLink, $crossLinkTileExternalLink); ?>"
                                   class="cross-tile cross-tile--default"
                                   style="background-image:url('<?php echo omGetPostThumbnailUrl($crossLinkBackgroundImage['ID'], 'om-tablet'); ?>')">
                                    <span class="overlay"></span>

                                    <h3><?php echo $crossLinkTitle; ?></h3>
                                </a>
                            </div>
                        <?php endfor; ?>
                    </div>
                </div>
            </div>
    <?php endif; ?>
<?php } ?>
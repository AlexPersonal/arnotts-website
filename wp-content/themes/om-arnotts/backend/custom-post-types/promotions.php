<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 6/02/2015
 * Time: 12:07 PM
 */

/** Sets the number of posts that the user wants to see per page
 * @return int { Number } Sets the number of visible posts when doing the pagination
 */
function omGetPromotionsPostsPerPage() {
    return 5;
}

/**
 * Retrieves all 'promotions' posts.
 * @return null|WP_Query
 */
function getAllPromotionsPosts() {
    $PromotionsArgs = array(
        'posts_per_page'   => '-1',
        'category'         => '',
        'category_name'    => '',
        'post_type'        => 'promotion',
        'post_status'      => 'publish'
    );

    $Promotions = new WP_Query($PromotionsArgs);
    if($Promotions->posts) {

        $Promotions = $Promotions->posts;

    } else {
        $Promotions = null;
    }

    return $Promotions;
}

/**
 * Retrieves promotion posts that have an end date which isn't older than today's date.
 *
 * @return array { Array }
 */
function omGetNewPromotionPosts() {
    $allPromotionsPosts = getAllPromotionsPosts();
    $PromotionsLength  = count($allPromotionsPosts);
    $newPosts = array();

    for($m = 0; $m < $PromotionsLength; $m++):

        $PromotionsPostID = $allPromotionsPosts[$m]->ID;
        $promotions_date = (get_post_meta($PromotionsPostID, 'promotions_date', true));

        if(strtotime(date('c')) < strtotime($promotions_date)):
            array_push($newPosts, $allPromotionsPosts[$m]);
        endif;
    endfor;

    return $newPosts;
}

/**
 * Retrieves promotion posts that have an end date which is older than today's date.
 *
 * @return array { Array }
 */
function omGetOldPromotionPosts() {
    $allPromotionsPosts = getAllPromotionsPosts();
    $PromotionsLength  = count($allPromotionsPosts);
    $oldPosts = array();

    for($m = 0; $m < $PromotionsLength; $m++):

        $PromotionsPostID = $allPromotionsPosts[$m]->ID;
        $promotions_date = (get_post_meta($PromotionsPostID, 'promotions_date', true));

        if(strtotime(date('c')) > strtotime($promotions_date)):
            array_push($oldPosts, $allPromotionsPosts[$m]);
        endif;
    endfor;

    return $oldPosts;
}

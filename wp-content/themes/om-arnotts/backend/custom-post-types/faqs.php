<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 4/02/2015
 * Time: 4:57 PM
 */

/** Sets the number of posts that the user wants to see per page
 * @return int { Number } Sets the number of visible posts when doing the pagination
 */
function omGetFAQPostsPerPage() {
    return 6;
}

function omSetFAQCategoryFilterParam() {
    $omFilterCategoryParam = 'category';

    return $omFilterCategoryParam;
}

/**
 * Returns a requested category ID or an empty array.
 * @return mixed { String } Null or The ID of the currently selected category.
 */
function omGetCurrentlySelectedCategorySlug() {

    $catID = get_query_var(omSetFAQCategoryFilterParam());

    return $catID;
}

/**
 * Fetches the FAQ custom post types.
 * @return mixed
 */
function omGetFAQs() {

    $FAQCategory = get_query_var(omSetFAQCategoryFilterParam());
    $paged = get_query_var('paged');

    if(!$FAQCategory) {

        $faqArgs = array(
            'posts_per_page'   => '-1',
            'paged'            => $paged,
            'orderby'          => 'post_date',
            'post_type'        => 'faq',
            'post_status'      => 'publish',
            'suppress_filters' => true
        );

    } else {

        $faqArgs = array(
            'posts_per_page'   => '-1',
            'paged'            => $paged,
            'post_type'        => 'faq',
            'post_status'      => 'publish',
            'suppress_filters' => true,
            'tax_query' => array(
                array(
                    'taxonomy' => 'faq_category',
                    'field'    => 'slug',
                    'terms'    => ''.$FAQCategory,
                ),
            )
        );
    }

    $faqs = new WP_Query($faqArgs);

    if($faqs->posts) {

        $faqs = $faqs->posts;

    } else {
        $mediaCentres = null;
    }

    return $faqs;
}

/**
 * Retuns a list of terms that were saved in the custom taxonomy 'faq_category'
 */
function omGetFAQCategoriesSelect() {

    $selectedCategorySlug = omGetCurrentlySelectedCategorySlug();

    // Get all Terms that belong to the faq taxonomy
    $catArgs = array();
    $faqTerms = get_terms('faq_category', $catArgs);

    echo '<select name="'.omSetFAQCategoryFilterParam().'">';

    if(!count($faqTerms)) {
        echo '<option value="">No Categories available</option>';

    } else {
        echo '<option value="">All Categories</option>';

        foreach($faqTerms as $faqTerm) {

            $termName = strtolower(trim($faqTerm->name));

            if($termName == $selectedCategorySlug) {

                echo '<option selected="selected" value="'.sanitize_text_field($termName).'">'.$faqTerm->name.'</option>';

            } else {

                echo '<option value="'. sanitize_text_field($termName) .'">'.$faqTerm->name.'</option>';
            }
        }
    }

    echo '</select>';

}

<?php
/** Sets the number of posts that the user wants to see per page
 * @return int { Number } Sets the number of visible posts when doing the pagination
 */
function omGetRecipesPostsPerPage() {
    return 12;
}


function omGetRecipeType() {
    return get_query_var('recipetype');
}

function omGetRecipeCat() {
    return get_query_var('recipecat');
}

/**
 * Retrieves 'recipes' posts depending on search terms.
 * @return null|WP_Query
 */
function omGetRecipePosts() {

    $paged = omGetCurrentPage();
    $type = get_query_var('recipetype');
    $category_url = get_query_var('recipecat');

    if(omGetRecipeType()) {
        $recipeArgs = array(
            'posts_per_page'   => '-1',
            'paged'            => $paged,
            'category'         => '',
            'category_name'    => '',
            'post_type'        => 'recipe',
            'post_status'      => 'publish',
            's' => $s,
            'year' => $year,
            'tax_query' => array(
                array(
                    'taxonomy' => 'recipe_category',
                    'field'    => 'slug',
                    'terms'    => ''.$type,
                ),
            )
        );

    } else {
        $recipeArgs = array(
            'posts_per_page'   => '-1',
            'paged'            => $paged,
            'category'         => '',
            'category_name'    => '',
            'post_type'        => 'recipe',
            'post_status'      => 'publish'
        );
    }

    $recipes = new WP_Query($recipeArgs);

    if($recipes->posts) {

        if($category_url != '' && $filter != 'filter') {
            $recipes = omFilterArrayByCategory($recipes->posts, $category_url);
        }else {
            $recipes = $recipes->posts;
        }

    } else {
        $recipes = null;
    }

    return $recipes;
}

function omFilterArrayByCategory($recipeQueryResult, $filetype) {

    $result = array();

    foreach ($recipeQueryResult as $recipePost) {

        $recipePostID = $recipePost->ID;
        $recipe_asset = (get_post_meta($recipePostID, 'recipes_with_selected_product', true));
        $recipeID = $recipe_asset['ID'];
        $productParentsArray = get_post_ancestors($recipeID);
        $productParentsArrayLength = count($productParentsArray);

        if(omGetProductTitle($productParentsArrayLength, $productParentsArray, $recipeID) === $filetype) {
            array_push($result, $recipePost);
        }
    }
    return $result;

}

/**
 * Retrieves categories from the recipe taxonomy
 * @return null|WP_Query
 */
function omGetRecipesCategoriesSelect() {

    $selectedCategorySlug = get_query_var('recipetype');//omGetCurrentlySelectedCategorySlug();

    // Get all Terms that belong to the faq taxonomy
    $catArgs = array();
    $recipeCategoryTerms = get_terms('recipe_category', $catArgs);

    echo '<select name="recipetype">';
    if(count($recipeCategoryTerms) == 0) {
        echo '<option value="">No Recipes available</option>';

    } else {
        echo '<option value="">All Recipes</option>';

        foreach($recipeCategoryTerms as $recipeCategoryTerm) {

            $termName = strtolower(trim($recipeCategoryTerm->name));

            if($termName == $selectedCategorySlug) {

                echo '<option selected="selected" value="'.sanitize_text_field($termName).'">'.$recipeCategoryTerm->name.'</option>';

            } else {

                echo '<option value="'. sanitize_text_field($termName) .'">'.$recipeCategoryTerm->name.'</option>';
            }
        }
    }

    echo '</select>';

}
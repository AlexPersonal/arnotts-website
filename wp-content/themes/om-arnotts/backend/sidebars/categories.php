<?php
/**
 * Get the list of categories
 *
 * @return boolean true if there is more than 1 categorie available.
 */
function om_get_categories_list() {

	if ( false === ( $all_the_cool_cats = get_transient( 'om_get_categories_list' ) ) ) {
		// Create an array of all the categories that are attached to posts
		$all_the_cool_cats = get_categories( array(
			'hide_empty' => 1,
		) );


		// Count the number of categories that are attached to the posts
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'om_get_categories_list', $all_the_cool_cats );
	}

	return get_the_category_list();

	if ( 1 !== (int) $all_the_cool_cats ) {
		// This blog has more than 1 category so om_get_categories_list should return true
		//return true;
		return get_the_category_list();

	} else {
		// This blog has only 1 category so om_get_categories_list should return false
		return false;
	}
}

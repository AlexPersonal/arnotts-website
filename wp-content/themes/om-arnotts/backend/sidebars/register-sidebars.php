<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 1/06/2014
 * Time: 6:22 PM
 */

if (function_exists('register_sidebar')) {

	register_sidebar(array(
		'name' => 'Widget Area',
		'id'   => 'widget-area',
		'description'   => 'This is a widget area. Drag widgets in here.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>'
	));

	register_sidebar(array(
		'name' => 'Copywright',
		'id'   => 'copywright',
		'description'   => 'Copywright information',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>'
	));

    register_sidebar(array(
		'name' => 'Whats new',
		'id'   => 'what-s-new',
		'description'   => 'What\'s new copy',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	));

    register_sidebar(array(
		'name' => 'Explore',
		'id'   => 'explore',
		'description'   => 'Explore the world of Arnott\'s',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	));

}
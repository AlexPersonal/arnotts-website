<?php

/**
 * Adjusting the wordpress native tinyMC editor
 * @param $in
 * @return mixed
 */
function omTinymcAdjustments( $in ) {

    // Add custom colors
    $custom_colours = ' "ad0a08", "Primary theme color", "c2a87e", "Secondary theme color", "333", "Primary font color", "f3f0e7", "Light creme", "989898", "Light grey" ';
    $in['textcolor_map'] = '[' . $custom_colours . ']';

    // Add Headline styles
    $in['block_formats'] = "Paragraph=p; Heading 2=h2; Heading 3=h3; Heading 4=h4";

    // Add p-tags otherwise the contact us tiles won't work
    $in['wpautop'] = false;

    $in[] = 'styleselect';

    // Split into two toolbars
    $in['toolbar1'] = 'textcolor,bold,italic,strikethrough,bullist,numlist,blockquote,hr,alignleft,aligncenter,alignright,link,unlink,wp_more,spellchecker,wp_fullscreen,wp_adv';
    $in['toolbar2'] = 'formatselect,forecolor,pastetext,styleselect';

    $in['wordpress_adv_hidden'] = FALSE;

    return $in;
}

function omAddGlobalCtaClass( $init_array ) {  
    $style_formats = array(  
        array(  
            'title' => 'Global link style',  
            'selector' => 'a',  
            'classes' => 'global-link'
        )
    );  
    $init_array['style_formats'] = json_encode( $style_formats );  

    return $init_array;  

} 



add_filter( 'tiny_mce_before_init', 'omTinymcAdjustments' );
add_filter( 'tiny_mce_before_init', 'omAddGlobalCtaClass' );
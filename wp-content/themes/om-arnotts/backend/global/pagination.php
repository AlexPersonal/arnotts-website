<?php

/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @since Twenty Fourteen 1.0
 */
if ( ! function_exists( 'omListingPageNav' ) ) {

    function omListingPageNav() {

        // Don't print empty markup if there's only one page.
        if ( $GLOBALS['wp_query']->max_num_pages <= 1 ) {
            return;
        }

        $paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
        $pagenum_link = html_entity_decode( get_pagenum_link() );
        $query_args   = array();
        $url_parts    = explode( '?', $pagenum_link );

        if ( isset( $url_parts[1] ) ) {
            wp_parse_str( $url_parts[1], $query_args );
        }

        $pagenum_link = esc_url(remove_query_arg( array_keys( $query_args ), $pagenum_link ));
        $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

        $format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
        $format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

        // Set up paginated links.
        $links = paginate_links( array(
            'base'     => $pagenum_link,
            'format'   => $format,
            'total'    => $GLOBALS['wp_query']->max_num_pages,
            'current'  => $paged,
            'mid_size' => 1,
            'add_args' => array_map( 'urlencode', $query_args ),
            'prev_text' => '',
            'next_text' => '',
        ) );

        if ( $links ) :

            ?>
        <div class="row">
            <nav class="navigation paging-navigation" role="navigation">
                    <?php echo $links; ?>
            </nav><!-- .navigation -->
        </div>
        <?php
        endif;
    }
}


/**
 * Custom pagination that will take an array and the posts_per_page value
 * to generate the pagination links
 */
if ( ! function_exists( 'omCustomLoopPagination' ) ) {

    function omCustomLoopPagination($arrayOfItems, $postsPerPage) {
        $arrayOfItemsLength = count($arrayOfItems);

        // Don't print empty markup if there's only one page.
        if ( $arrayOfItemsLength <= 1 ) {
            return;
        }

        $paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
        $pagenum_link = html_entity_decode( get_pagenum_link() );
        $query_args   = array();
        $url_parts    = explode( '?', $pagenum_link );

        if ( isset( $url_parts[1] ) ) {
            wp_parse_str( $url_parts[1], $query_args );
        }

        $pagenum_link = esc_url(remove_query_arg( array_keys( $query_args ), $pagenum_link ));
        $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

        $format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
        $format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

        // Set up paginated links.
        $links = paginate_links( array(
            'base'     => $pagenum_link,
            'format'   => $format,
            'total'    => ceil((int)$arrayOfItemsLength / (int)$postsPerPage),
            'current'  => $paged,
            'mid_size' => 1,
            'add_args' => array_map( 'urlencode', $query_args ),
            'prev_text' => '',
            'next_text' => '',
        ) );

        if ( $links ) { ?>
        <div class="row">
            <nav class="navigation paging-navigation" role="navigation">
                <?php echo $links; ?>
            </nav><!-- .navigation -->
        </div>
      <?php }
    }
}

/**
 * Display navigation to next/previous post when applicable.
 */
if ( ! function_exists( 'omSinglePageNav' ) ) :

    function omSinglePageNav() {
        ?>
        <nav class="navigation post-navigation" role="navigation">
            <div class="nav-links">
                <?php
                    previous_post_link( '%link', '<span class="global-link-left">Previous Article</span>' );
                    next_post_link( '%link', '<span class="global-link">Next Article</span>');
                ?>
            </div><!-- .nav-links -->
        </nav><!-- .navigation -->
    <?php
    }
endif;

/**
 * Display navigation for comments
 */
if ( ! function_exists( 'omCommentPagination' ) ) :

    function omCommentPagination() {

        $commentPageArgs = array(
            'prev_next'    => True,
            'prev_text'    => __('Previous'),
            'next_text'    => __('Next'),
            'type'         => 'list',
            'add_args'     => False,
            'add_fragment' => '');

        paginate_comments_links( $commentPageArgs );

     ?>

    <?php
    }
endif;
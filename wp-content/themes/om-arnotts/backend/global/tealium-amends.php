<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 16/02/2015
 * Time: 12:35 PM
 */

/*
 * Switch Tealium environment based on website URL
 * @url: https://github.com/ianhampton/Tealium-Wordpress-Plugin#action-examples
 */
function omSwitchTealiumEnvironment() {
    global $tealiumtag;

    // In discussion with the client we agreed that Orchard staging and clients UAT will use the same environment
    if((function_exists('omIsLocal') && omIsLocal()) || (function_exists('omIsStaging') && omIsStaging())) {
        $tealiumtag = str_ireplace ( '/dev/', '/dev/', $tealiumtag );
        $tealiumtag = str_ireplace ( '/prod/', '/dev/', $tealiumtag );
        $tealiumtag = str_ireplace ( '/live/', '/dev/', $tealiumtag );
        $tealiumtag = str_ireplace ( '/qa/', '/dev/', $tealiumtag );
        $tealiumtag = str_ireplace ( '/uat/', '/dev/', $tealiumtag );

    } else if ((function_exists('omIsLive')) && omIsLive()) {
        $tealiumtag = str_ireplace ( '/PROD/', '/prod/', $tealiumtag );
        $tealiumtag = str_ireplace ( '/dev/', '/prod/', $tealiumtag );
        $tealiumtag = str_ireplace ( '/live/', '/prod/', $tealiumtag );
        $tealiumtag = str_ireplace ( '/qa/', '/prod/', $tealiumtag );
        $tealiumtag = str_ireplace ( '/uat/', '/prod/', $tealiumtag );
    }
}

add_action( 'tealium_tagCode', 'omSwitchTealiumEnvironment' );

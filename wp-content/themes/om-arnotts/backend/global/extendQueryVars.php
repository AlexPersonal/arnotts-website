<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 6/02/2015
 * Time: 9:35 AM
 */


// Eg. used for the filter on the faq listing page /about-arnotts/faqs (if the permalink hasn't been changed in the CMS)
esc_url(add_query_arg('category'));

// Eg. used on the 'Media Library' page.
esc_url(add_query_arg('keywords'));

// Eg. used on the 'Media Library' page.
esc_url(add_query_arg('filetype'));

// Eg. used on the 'Media Library' page.
esc_url(add_query_arg('mlyear'));

// Eg. used on the 'Media Centre' page.
esc_url(add_query_arg('mcyear'));

// Used on the 'Media Centre' page
esc_url(add_query_arg('newstype'));

// Used on the 'recipe' page
esc_url(add_query_arg('recipetype'));

// Used on the 'recipe' page
esc_url(add_query_arg('recipecat'));

/**
 * Populate new query vars to the loop
 * @param $vars
 * @return array
 */
function omAddQueryVarsFilter( $vars ){
    $vars[] = 'category';
    $vars[] = 'keywords';
    $vars[] = 'filetype';
    $vars[] = 'mlyear';
    $vars[] = 'mcyear';
    $vars[] = 'newstype';
    $vars[] = 'recipetype';
    $vars[] = 'recipecat';

    return $vars;
}
add_filter( 'query_vars', 'omAddQueryVarsFilter' );



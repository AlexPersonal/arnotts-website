<?php


function omArnottsLoadAssets(){
    wp_enqueue_script( 'jquery', '', '', '', true  );
    wp_enqueue_script( 'modernizr', get_template_directory_uri().'/assets/scripts/dev/vendor/libs/modernizr.min.js', '', '', true );
    wp_enqueue_script( 'requireJS', get_template_directory_uri().'/assets/scripts/dev/vendor/libs/require.js',  '', '', true );

    // Only use the minified version if you are on another environment than local
    if(omIsLocal()) {
        wp_enqueue_script( 'main', get_template_directory_uri().'/assets/scripts/main.js', array( 'modernizr','requireJS','jquery'), '' , true);
    } else {
        wp_enqueue_script( 'main', get_template_directory_uri().'/assets/scripts/main.min.js', array( 'modernizr','requireJS','jquery'), '' , true);
    }

    wp_localize_script( 'main', 'ViewAllProducts', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' , is_ssl() ? 'admin' : 'http' ),
        'nonce' => wp_create_nonce( 'view-more-products-nonce' )
    ) );

}

add_action( 'wp_enqueue_scripts' , 'omArnottsLoadAssets' );


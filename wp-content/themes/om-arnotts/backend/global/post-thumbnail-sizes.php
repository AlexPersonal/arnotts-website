<?php
// Activate Thumbnails in the theme support
add_theme_support('post-thumbnails');

// Create custom image sizes
add_image_size('om-hero', 2000, false);
add_image_size('om-tablet', 1024, false);
add_image_size('om-mobile', 667, false);

// Used on the products overview page
add_image_size('om-package-size-small', 229, false);
add_image_size('om-package-size-medium', 300, false);

// Media Centre
add_image_size('om-media-centre-size', 275, false);

// Promotions
add_image_size('om-promotion-size', 575, false);
add_image_size('om-promotion-size-small', 375, false);
add_image_size('om-promotion-size-large', 768, false);

// Recipes
add_image_size('om-related-recipe-size', 375, false);
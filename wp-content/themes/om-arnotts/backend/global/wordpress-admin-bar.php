<?php


// Hide the admin bar if the user has no edit permissions
if ( ! current_user_can( 'manage_options' ) ) {
    show_admin_bar( false );
}
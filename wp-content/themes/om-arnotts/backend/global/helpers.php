<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 17/01/2015
 * Time: 9:42 PM
 */


/**
 * @return string
 */
function omGetRootURL() {
    return (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
}

/**
 * Takes a string and converts it into a valid HTML id. This can be used for
 * anchoring.
 *
 * @param $stringToConvert {String} The string that needs to be converted into an id
 * @return mixed {String} The valid id
 */
function omConvertStringToValidID($stringToConvert) {
    // Make lowercase
    $stringToConvert = strtolower($stringToConvert);

    $specialChar = array("[","]","'","$","~", "!", "+", ".","&");

    $stringToConvert = str_replace("&", '-and-',$stringToConvert);

    // Filter out special characters
    $stringToConvert = str_replace($specialChar, '',$stringToConvert);

    // Substitute space with hyphen
    $stringToConvert = str_replace(' ', '-', $stringToConvert);

    // Remove duplicate --
    return str_replace('--', '-', $stringToConvert);

}

/**
 * Returns the number of the currently selected page. This is being used on all pages that have a pagination on
 * (e.g. Media Library, Media Centre, ...)
 * @return mixed { Empty String or Number } Empty String or the number of the current page that the user is on.
 */
function omGetCurrentPage() {
    return sanitize_text_field(get_query_var('paged'));
}

/**
 * Shortens the copy to a given limit.
 *
 * @param $text { String } the copy that needs to be shortened.
 * @param int $limit { number } the amount of characters that the copy needs to have.
 * @return string { String } returns the string
 */
function wordLimiter($text, $limit = 200) {

    $cut = substr(strip_tags($text), 0, $limit);

    $words = explode(' ', $cut);
    array_pop($words);

    $text = implode(' ', $words). '';

    // If the text is shorter than the limit, don't do anything
    if( strlen($text) > $limit) {
        return '<p>'.$text.' ...</p>';
    }

    return $text;
}
<?php

/**
 * Getting the posts for the homepage tiles
 */
function getHomepageGridPosts() {

    $media_centre_args = array(
        'meta_query' => array(
            array(
                'key' => 'media_centre_add_to_homepage',
                'value' => 1
            )
        ),
        'post_type' => 'media_centre',
        'posts_per_page'   => 5
    );
    $recipe_args = array(
        'meta_query' => array(
            array(
                'key' => 'recipe_add_to_homepage',
                'value' => 1
            )
        ),
        'post_type' => 'recipe',
        'posts_per_page'   => 2
    );
    
    $media_centre_articles = get_posts($media_centre_args);
    $recipe_articles = get_posts($recipe_args);
    $recipe_articles_one = array_slice($recipe_articles, 0, 1);
    $recipe_articles_two = array_slice($recipe_articles, 1, 2);
    $mixedArray = array_merge($recipe_articles_one, $media_centre_articles, $recipe_articles_two);
    

    return $mixedArray;
}

/**
 * Renders the smaller tile on the Homepage 'World of Arnotts'
 *
 * @param $post { object } The post that contains all the data. This currently used on the homepage and can either be a news (media-centre) post type
 *        or recipes post type.
 * @param $postImageUrl { String } URL to the posts image that needs to be rendered.
 * @param $postLink { String }
 * @param $media_centre_post_date { String } DD m YYYY
 * @param $seoPostDate { String } YYYY-MM-DD
 * @param $format_excerpt { String }
 */
function renderTileHeight1($post, $postImageUrl, $postLink, $media_centre_post_date,$seoPostDate,$format_excerpt ) { ?>

    <div class="tile tile--style-6 tile--height-1" style="background: url('<?php echo $postImageUrl ?>');">

        <div class="tile-view--default">
            <p><?php echo $post->post_title ?></p>
        </div>

        <!-- Tile active state -->
        <a href="<?php echo $postLink ?>" class="tile-view--active">
            <h3><?php echo $post->post_title ?></h3>

            <time datetime="<?php echo $seoPostDate ?>"><?php echo $media_centre_post_date ?></time>

            <p><?php echo $format_excerpt; ?></p>
        </a>
        <!-- /Tile active state -->

    </div>
<?php } ?>
<?php

/**
 * @param $post { object } The post that contains all the data. This currently used on the homepage and can either be a news (media-centre) post type
 *        or recipes post type.
 * @param $postImageUrl { String } URL to the posts image that needs to be rendered.
 * @param $postLink { String }
 * @param $media_centre_post_date { String } DD m YYYY
 * @param $seoPostDate { String } YYYY-MM-DD
 * @param $format_excerpt { String }
 */
function renderTileHeight2($post, $postImageUrl, $postLink, $media_centre_post_date,$seoPostDate, $format_excerpt ) { ?>
    <div class="tile tile--style-6 tile--height-2" style="background: url('<?php echo $postImageUrl ?>');">

        <!-- Tile default state -->
        <div class="tile-view--default">
            <p><?php echo $post->post_title ?></p>
        </div>
        <!-- / Tile default state -->

        <!-- Tile active state -->
         <a href="<?php echo $postLink ?>" class="tile-view--active">
            <div class="tile-view--active">
                <h3><?php echo $post->post_title ?></h3>
                <time datetime="<?php echo $seoPostDate; ?>"><?php echo $media_centre_post_date ?></time>
                <p><?php echo $format_excerpt ?></p>
            </div>
         </a>
        <!-- /Tile active state -->

    </div>
<?php } ?>
<?php


// Turn off wordpress compression
add_filter('jpeg_quality', create_function('', 'return 100;'));
add_filter('wp_editor_set_quality', create_function('', 'return 100;'));

function getFallbackHeroImage() {
    // Attachment ID of the fallback image
    return 322;
}
/**
 * @param $attachmentID { number } The id of the attachment.
 * @param $imgSize { String } The size of th image you want to provide. If you leave empty it'll provide the smallest necessary size in dependence to the user agent.
 * @return string The url of the thumbnail image
 */
function omGetPostThumbnailUrl($attachmentID, $imgSize) {

    if(function_exists('is_mobile') && is_mobile()) {

        if($imgSize) {

            $omPostThumbnailUrl = wp_get_attachment_image_src($attachmentID, $imgSize);

        } else {

            $omPostThumbnailUrl = wp_get_attachment_image_src($attachmentID, 'om-promotion-size');
        }

    }else if(function_exists('is_tablet') && is_tablet()) {


        if($imgSize) {

            $omPostThumbnailUrl = wp_get_attachment_image_src($attachmentID, $imgSize);

        } else {

            $omPostThumbnailUrl = wp_get_attachment_image_src($attachmentID, 'om-tablet');
        }

    } else {

        if($imgSize) {

            $omPostThumbnailUrl = wp_get_attachment_image_src($attachmentID, $imgSize);

        } else {

            $omPostThumbnailUrl = wp_get_attachment_image_src($attachmentID, 'om-hero');
        }

    }

    // Get the images url
    $omPostThumbnailUrl = $omPostThumbnailUrl[0];

    return $omPostThumbnailUrl;
}

/**
 * Used for full width carousels like the hero carousel.
 * @param $postID { number } The id of the post that the post thumbnail has to be retrieved for.
 * @return string The ulr of the thumbnail image
 */
function omGetHeroPostThumbnailUrl($postID) {

    if(function_exists('is_mobile') && is_mobile()) {

        $omPostThumbnailUrl = wp_get_attachment_image_src($postID, 'om-tablet');

    } else if(function_exists('is_tablet') && is_tablet()) {

        $omPostThumbnailUrl = wp_get_attachment_image_src($postID, 'om-tablet');

    } else {

        $omPostThumbnailUrl = wp_get_attachment_image_src($postID, 'om-hero');
    }

    // Get the images url
    $omPostThumbnailUrl = $omPostThumbnailUrl[0];

    return $omPostThumbnailUrl;
}

/**
 * This function is being used if you have the ID of the attachment already (e.g. if this is a custom meta value)
 * and you want to return the thumbnail. It'll return the image tag including the alt attribute and sizes.
 *
 * @param $attachmentID { Number } The id of the attachment
 * @param $size { String } The size that you need to return
 * @return mixed
 */
function omGetImageByAttachmentID($attachmentID, $size) {

    switch($size) {
        case 'large':
            $image = wp_get_attachment_image($attachmentID, 'large');
            break;
        default:
            $image = wp_get_attachment_image($attachmentID, $size);
    }

    return $image;
}


/**
 * Takes the id of the attachment and a string for the size (wordpress registered thumbnail sizes or null) and returns
 * the url to the attachment
 *
 * @param $attachmentID { Number } The id of the attachment image
 * @param $size { String } one of the registered Wordrpess thumbnail sizes
 * @return mixed { String } Returns the url to the image url.
 */
function omGetImageUrlByAttachmentID($attachmentID, $size) {
    if($size) {

        $imageUrl =  wp_get_attachment_image_src($attachmentID, $size);

    } else {

        if(function_exists('is_mobile') && is_mobile()) {

            $imageUrl = wp_get_attachment_image_src($attachmentID, 'om-tablet');

        } else if(function_exists('is_tablet') && is_tablet()) {

            $imageUrl = wp_get_attachment_image_src($attachmentID, 'om-tablet');

        }  else {

            $imageUrl = wp_get_attachment_image_src($attachmentID, 'om-hero');
        }
    }

    // Get the images url
    $imageUrl = $imageUrl[0];


    return $imageUrl;

}
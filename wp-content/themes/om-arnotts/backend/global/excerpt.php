<?php


//add_filter('excerpt_length', 'omCustomExcerptLength', 999);

/**
 * Overwrite the number of words that will be returned in an excerpt.
 * This doesn't work on the actual excerpt content only if no excerpt has been entered in the CMS
 * and the content is actually being used. Then that exact content will be trimmed of to the amount
 * of words (not characters) set.
 * @param $length
 * @return int
 */
//function omCustomExcerptLength($length)
//{
//
//    // Returns the number of words;
//    return 10;
//}

/**
 * Overwrite the original read more ui [...]
 * @param $more
 * @return string
 */
function omNewExcerptMore( $more ) {
    return ' ...';
}
add_filter('excerpt_more', 'omNewExcerptMore');


/**
 * Shortens the copy to a given limit.
 *
 * @param $text { String } the copy that needs to be shortened.
 * @param int $limit { number } the amount of characters that the copy needs to have.
 * @return string { String } returns the string
 */
function word_limiter($text, $limit = 200) {

    // If the text is shorter than the limit, don't do anything
    if( strlen($text) <= $limit) {
        return '<p>'.$text.' ...</p>';
    }

    $cut = substr(strip_tags($text), 0, $limit);

    $words = explode(' ', $cut);
    array_pop($words);

    $text = '<p>'.implode(' ', $words). " ...</p>";

    return $text;
}

/**
 * Limits the copy to the value given when the function is called. Takes the copy from the
 * excerpt if one given otherwise the content copy will be taken.
 *
 * @param $id { number } the id of the blog post.
 * @param $limit { number } the number of characters that should be shown.
 */
function omLimitPostContent($id,$limit) {

    $content_post = get_post($id);
    $content = $content_post->post_content;
    $excerpt = $content_post->post_excerpt;

    $content = ($excerpt) ? word_limiter($excerpt,$limit) : word_limiter($content,$limit);

    echo $content;
}
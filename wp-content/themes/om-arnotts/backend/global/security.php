<?php

/* Checks whether the current page is being served on HTTPS */
function isSecurePage() {
  return
    (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
    || $_SERVER['SERVER_PORT'] == 443;
}

/* 
	Add the X-Frame-Options header to the response for security purposes, set to DENY so site cannot be iframed into another site
	https://developer.mozilla.org/en-US/docs/Web/HTTP/X-Frame-Options
*/
function add_xframe_deny_header($headers){
	$headers["X-Frame-Options"] = "DENY";
	return $headers;
}

/* If we are on a HTTPS page, this will be secure so we need to ensure the cache-control is set correctly
so no pages are cached */
function add_nocache_nostore_cache_control_header($header){

	if(isSecurePage())
	{		
		$headers["Cache-Control"] = "no-cache, no-store, must-revalidate";
		$headers["Expires"]  = "Mon, 26 Jul 1997 05:00:00 GMT"; // Set to date in the past
		$headers["Pragma"] = "no-cache";
	}

	return $headers;
}

/* Call some custom filters for security */
add_filter('wp_headers', 'add_xframe_deny_header');
add_filter('wp_headers', 'add_nocache_nostore_cache_control_header');
<?php
/**
 * User: jessica.nierth
 * Date: 6/01/14
 * Time: 1:21 PM
 */

/**
 * @return string {String} Returns 'local' if it is a local environment, 'staging' if it is staging and live if environment is live.
 */
function omSetEnvironment() {

	if (strpos($_SERVER['HTTP_HOST'],'.local') !== false) {

		return $ENVIRONMENT = 'local';

	} else if((strpos($_SERVER['HTTP_HOST'], 'staging') !== false) ||
		(strpos($_SERVER['HTTP_HOST'], 'stage') !== false))  {

		return $ENVIRONMENT = 'staging';

    } else if((strpos($_SERVER['HTTP_HOST'], 'aws143-001d.server-access.com') !== false))  {

		return $ENVIRONMENT = 'uat';

    } else if((strpos($_SERVER['HTTP_HOST'], 'prelive') !== false) ||
        (strpos($_SERVER['HTTP_HOST'], 'server-access.com') !== false) ||
        (strpos($_SERVER['HTTP_HOST'], 'arnotts.com.au') !== false))  {

		return $ENVIRONMENT = 'live';

    }

    // Make it fail
    return '';
}

/**
 * Function to check if the current environment is staging or uat
 * @return bool {boolean} true if environment is staging or uat.
 */
function omIsStaging() {
    $ENVIRONMENT = omSetEnvironment();

	return (isset($ENVIRONMENT) == 'staging' || isset($ENVIRONMENT) == 'uat');
}

/**
 * Function to check if the current environment is live.
 * @return bool {boolean} true if environment is live.
 */
function omIsLive() {
    $ENVIRONMENT = omSetEnvironment();

	return (isset($ENVIRONMENT) && $ENVIRONMENT == 'live');
}

/**
 * Function to check if the current environment is local.
 * @return bool {boolean} true if environment is local.
 */
function omIsLocal() {
	$ENVIRONMENT = omSetEnvironment();

	if(isset($ENVIRONMENT) && ( $ENVIRONMENT == 'local' )) return true;

	return false;
}

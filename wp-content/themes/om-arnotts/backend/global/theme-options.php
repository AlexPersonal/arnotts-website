<?php
/**
 * Extend the Theme customizer with custom social media links.
 * @param $wp_customize
 */

function omarnotts_theme_customizer( $wp_customize ) {

    // PDF SUPPORT FOR PRODUCTS - NUTRITIONAL INFORMATION
    $wp_customize->add_section(
        'omarnotts_404_image',
        array(
            'title' => '404 Image',
            'description' => 'Custom 404 Hero image',
            'priority' => 20
        )
    );

    $wp_customize->add_setting(
        'omarnotts_404_hero_image'
    );

    $wp_customize->add_control(
        new WP_Customize_Upload_Control(
            $wp_customize,
            'omarnotts_404_hero_image',
            array(
                'label' => '404 hero image',
                'section' => 'omarnotts_404_image',
                'settings' => 'omarnotts_404_hero_image'
            )
        )
    );


    $wp_customize->add_section( 'omarnotts_socialMedia_section' , array(
        'title'       => __( 'Social Media Links', 'omarnotts' ),
        'priority'    => 30,
        'description' => 'Set the links to social media pages here'
    ) );

    // ADD FACEBOOK
    $wp_customize->add_setting( 'omarnotts_facebookPageLink' );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'omarnotts_facebookPageLink', array(
        'label'    => __( 'Facebook Page Link', 'omarnotts' ),
        'type'     => 'text',
        'section'  => 'omarnotts_socialMedia_section',
        'settings' => 'omarnotts_facebookPageLink',
    ) ) );

    // ADD TWITTER
    $wp_customize->add_setting( 'omarnotts_twitterPageLink' );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'omarnotts_twitterPageLink', array(
        'label'    => __( 'Twitter Page Link', 'omarnotts' ),
        'type'     => 'text',
        'section'  => 'omarnotts_socialMedia_section',
        'settings' => 'omarnotts_twitterPageLink',
    ) ) );

    // ADD LINKEDIN
    $wp_customize->add_setting( 'omarnotts_linkedInPageLink' );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'omarnotts_linkedInPageLink', array(
        'label'    => __( 'LinkedIn Page Link', 'omarnotts' ),
        'type'     => 'text',
        'section'  => 'omarnotts_socialMedia_section',
        'settings' => 'omarnotts_linkedInPageLink',
    ) ) );

    // PDF SUPPORT FOR PRODUCTS - NUTRITIONAL INFORMATION
    $wp_customize->add_section(
        'omarnotts_nutritional_information',
        array(
            'title' => 'Nutritional information',
            'description' => 'PDFs referenced on Products pages',
            'priority' => 35,
        )
    );

    $wp_customize->add_setting(
        'omarnotts_nutritional_information_file_1'
    );

    $wp_customize->add_control(
        new WP_Customize_Upload_Control(
            $wp_customize,
            'omarnotts_nutritional_information_file_1',
            array(
                'label' => 'PDF 1 / Image 1',
                'section' => 'omarnotts_nutritional_information',
                'settings' => 'omarnotts_nutritional_information_file_1'
            )
        )
    );

    $wp_customize->add_setting(
        'omarnotts_nutritional_information_file_2'
    );

    $wp_customize->add_control(
        new WP_Customize_Upload_Control(
            $wp_customize,
            'omarnotts_nutritional_information_file_2',
            array(
                'label' => 'PDF 2 / Image 2',
                'section' => 'omarnotts_nutritional_information',
                'settings' => 'omarnotts_nutritional_information_file_2'
            )
        )
    );
}
add_action('customize_register', 'omarnotts_theme_customizer');

/**
 * @param $contact_us_type {String} at the moment [facebook, twitter, linkedin] are defined.
 * @return string Returns the Link to the external social media page if there is one otherwise returns an empty string.
 */
function omGetSocialMediaLinkIfAvailable($contact_us_type)
{
    switch ($contact_us_type) {
        case 'facebook':
            $socialMediaLink = get_theme_mod('omarnotts_facebookPageLink');
            break;
        case 'twitter':
            $socialMediaLink = get_theme_mod('omarnotts_twitterPageLink');
            break;
        case 'linkedin':
            $socialMediaLink = get_theme_mod('omarnotts_linkedInPageLink');
            break;
        default:
            $socialMediaLink = '';
    }

    return $socialMediaLink;

}

?>
<?php

/**
 * In contrast to the other functions this functions takes two links and returns the internal link if there is one, otherwise
 * it'll return the external link. If neither ones are set it'll return an empty string.
 *
 * @param $internalLink { Number } The ID of the custom post / page that the user wants to link to.
 * @param $externalLink { String } The URL to an external resource
 * @return string { String } Either an empty string or a link.
 */
function omReturnLink($internalLink,$externalLink) {
    $link = '';

    if(!empty($internalLink)) {

        $link = get_permalink($internalLink);

    } else if (!empty($externalLink)) {

        $link = $externalLink;
    }

    return $link;
}

/**
 * Outputs the call to action HTML if the user has either entered an internal or external link. If both have been entered the
 * internal link will take precedence.
 * The user is required to type in an ID of the post or page he wants to link to. If the post or page does not exist any more
 * the get_permalink function will return false and the button will not be generated.
 *
 * @param $internalLink {Number} The ID of the post or page the user wants to link to.
 * @param $externalLink {String} The link to an external page if there is one
 * @param $ctaCopy {String} The copy for the button
 * @return string {String} The HTML for the button if the requirements are met else it returns an empty string.
 */
function omReturnButtonIfRequiredWithInternalLinkID($internalLink,$externalLink, $ctaCopy) {
    $link = '';

    if(!empty($internalLink)) {

        $internalPagesPermalink = get_permalink($internalLink);

        // Only add the button if the permalink exists
        if($internalPagesPermalink) {
            $link = omReturnLinkHTML(true, $internalPagesPermalink, $ctaCopy);
        }

    } else if (!empty($externalLink)) {

        $link = omReturnLinkHTML(false, $externalLink, $ctaCopy);
    }

    return $link;
}


/**
 * @param $isInternalLink {Boolean} true if it is an internal link else false.
 * @param $ctaLink {String} The link that the button should have.
 * @param $ctaCopy {String} The copy of the button.
 * @return string {String} The HTML that will be used for the button otherwise it'll return an empty string.
 */
function omReturnLinkHTML($isInternalLink, $ctaLink, $ctaCopy) {

    $link = '';
    $isInternalLink = (!$isInternalLink) ? ' target="_blank" ': '';

    if(!empty($ctaLink)) {
        if (!empty($ctaCopy)) {
            $link .= '<div class="cta cta--center">';
            $link .= '<a class="btn-global" href="'. $ctaLink. '"' . $isInternalLink . '>';
            $link .= $ctaCopy;
            $link .= '</a>';
            $link .= '</div>';
        }
    }
    return $link;
 }

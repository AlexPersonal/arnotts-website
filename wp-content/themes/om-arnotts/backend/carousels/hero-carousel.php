<?php

function omGetHeroCarousel()
{

    global $post;

    $args = array(
        'post_type' => 'hero_post',
        'posts_per_page' => 3,
        'post_status' => 'publish'
    );
    $hero_posts = get_posts($args);

    ?>

    <?php if (count($hero_posts)): ?>
    <div class="global-carousel js-hero-carousel hero-carousel js-responsive-carousel">

        <?php foreach ($hero_posts as $post) : setup_postdata($post); ?>

            <?php
                $postID = $post->ID;
                $heroPostHeadline = (get_post_meta($postID, 'hero_post_headline', true));
                $heroPostSubHeadline = (get_post_meta($postID, 'hero_post_sub_heading', true));
                $callToActionCopy = (get_post_meta($postID, 'hero_post_call_to_action_copy', true));
                $callToActionExternalLink = (get_post_meta($postID, 'hero_post_call_to_action_external_link', true));
                $callToActionInternalLink = (get_post_meta($postID, 'hero_post_call_to_action_internal_link', true));

                if(has_post_thumbnail()) {

                    $attachmendUrl =  omGetHeroPostThumbnailUrl(get_post_thumbnail_id($postID));
            ?>
                    <div style="background-image:url('<?php echo $attachmendUrl; ?>')">
                        <div class="copy-layer">
                            <h3><?php echo $heroPostHeadline; ?></h3>

                            <?php if ($heroPostSubHeadline): ?>
                                <div class="description">
                                    <p><?php echo $heroPostSubHeadline; ?></p>
                                </div>
                            <?php endif; ?>

                            <?php echo omReturnButtonIfRequiredWithInternalLinkID($callToActionInternalLink, $callToActionExternalLink, $callToActionCopy); ?>

                        </div>
                    </div>

            <?php } else { ?>

                <p>Please add an image to the hero post with the ID <?php echo $postID; ?>. </p>

            <?php }?>

        <?php endforeach; ?>
        <a class="scroll-to js-scroll-section">scroll for more</a>
    </div>
<?php
endif;

}

?>
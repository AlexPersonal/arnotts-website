<?php

function omGetFeatureCarousel()
{

    global $post;

    $args = array(
        'post_type' => 'feature_post'
    );
    $featured_posts = get_posts($args);

    ?>

    <?php if (count($featured_posts)): ?>
    <div class="global-carousel js-feature-carousel feature-carousel js-responsive-carousel">

        <?php foreach ($featured_posts as $post) : setup_postdata($post); ?>

            <?php
                $postID = $post->ID;

                $featurePostHeadline = (get_post_meta($postID, 'feature_post_headline', true));
                $featurePostSubHeading = (get_post_meta($postID, 'feature_post_sub_headline', true));
                $callToActionCopy = (get_post_meta($postID, 'featured_post_call_to_action_copy', true));
                $callToActionExternalLink = (get_post_meta($postID, 'featured_post_call_to_action_external_link', true));
                $callToActionInternalLink = (get_post_meta($postID, 'featured_post_call_to_action_internal_link', true));

                if(has_post_thumbnail()) {
                    $attachmendUrl =  omGetPostThumbnailUrl(get_post_thumbnail_id($postID), null);
            ?>

                    <div>
                        <div class="feature-image responsive-height">
                            <img src="<?php echo $attachmendUrl; ?>" alt="Feature image"/>
                        </div>
                        <div class="copy-layer">
                            <div class="vert-align">
                                <?php if($featurePostHeadline) : ?>
                                    <h3><?php echo $featurePostHeadline; ?></h3>
                                <?php endif; ?>

                                <?php if ($featurePostSubHeading): ?>
                                    <div class="description">
                                        <p><?php echo $featurePostSubHeading; ?></p>
                                    </div>
                                <?php endif; ?>

                                <?php echo omReturnButtonIfRequiredWithInternalLinkID($callToActionInternalLink, $callToActionExternalLink, $callToActionCopy); ?>
                            </div>

                        </div>
                    </div>

                <?php } else { ?>

                    <p>Please add an image to this feature post. </p>

                <?php }?>

        <?php endforeach; ?>
    </div>
<?php
endif;

}

?>
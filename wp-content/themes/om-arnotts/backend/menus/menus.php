<?php

/**
 * This function registers all the menus used in this Theme
 */
function register_om_menus() {
    register_nav_menus(
        array(
            'top_menu' => __('Top Menu'),
            'main_menu' => __( 'Main Menu' ),
            'about_arnotts_menu' => ('About Arnotts Menu'),
            'footer_menu' => __('Footer Menu'),
            'social_media_menu' => __('Social Media Menu')
        )
    );
}

add_action( 'init', 'register_om_menus' );
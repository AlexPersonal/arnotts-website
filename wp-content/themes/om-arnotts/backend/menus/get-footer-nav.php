<?php

function omGetFooterNav()
{
    $defaults = array(
        'theme_location' => 'footer_menu',
        'menu' => '',
        'container' => '',
        'container_class' => '',
        'container_id' => '',
        'menu_class' => 'footer-menu',
        'menu_id' => 'footer-menu',
        'echo' => false,
        'before' => '',
        'after' => '',
        'link_before' => '',
        'link_after' => '',
        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        'depth' => 1,
        'walker' => ''
    );


    $footerMenu = wp_nav_menu($defaults);

    return $footerMenu;
}
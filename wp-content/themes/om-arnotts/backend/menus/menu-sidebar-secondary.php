<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 24/01/2015
 * Time: 10:44 PM
 */

function omGetSecondaryMenu($post) {

    // If it is a child page
    if($post->post_parent){

        // If it is a wordpress native page
        if(is_page()) {

            $secondaryNavArgs = array(
                'child_of' => $post->post_parent,
                'title_li' => '',
                'echo' => 0
            );

            $children = wp_list_pages($secondaryNavArgs);
            $parentPermaLink = get_permalink($post->post_parent);

        // If no children could be found it might not be a page but a custom page type
        // so we'll have to use another loop.
        } else {

            $secondaryNavArgs = array(
                'posts_per_page'   => -1,
                'offset'           => 0,
                'category'         => '',
                'category_name'    => '',
                'orderby'          => 'post_date',
                'order'            => 'DESC',
                'include'          => '',
                'exclude'          => array($post->post_parent),
                'meta_key'         => '',
                'meta_value'       => '',
                'post_type'        => get_post_type($post),
                'post_mime_type'   => '',
                'post_parent'      => $post->post_parent,
                'post_status'      => 'publish'
            );

            $children = get_posts($secondaryNavArgs);
            $parentPermaLink = get_permalink($post->post_parent);
        }

        $pageTitle = get_the_title($post->post_parent);

    } else {

        if(is_page()) {

            $secondaryNavArgs = array(
                'child_of' => $post->ID,
                'title_li' => '',
                'echo' => 0
            );

            $children = wp_list_pages($secondaryNavArgs);


        } else {

            $secondaryNavArgs = array(
                'posts_per_page'   => -1,
                'offset'           => 0,
                'category'         => '',
                'category_name'    => '',
                'orderby'          => 'post_date',
                'order'            => 'DESC',
                'include'          => '',
                'exclude'          => '',
                'meta_key'         => '',
                'meta_value'       => '',
                'post_type'        => get_post_type($post),
                'post_mime_type'   => '',
                'post_parent'      => $post->ID,
                'post_status'      => 'publish'
            );


            $children = get_posts($secondaryNavArgs);
        }

        $pageTitle = get_the_title($post->ID);
    }

    if ($children) { ?>
        <ul class="list--secondary-nav">
        <?php
            $currentPageClass = "";
            if ($parentPermaLink == "" ) {
                $currentPageClass = "current_page_item";
            }
            ?>
            <li class="<?php echo "$currentPageClass" ?>">
                <a href="<?php echo $parentPermaLink; ?>"><?php echo $pageTitle; ?></a>
            </li>
            <?php if(is_array($children)) : ?>
                <?php foreach($children as $child): ?>

                    <?php if($child->ID == $post->ID): $currentChildClass = "current_page_item"; endif; ?>

                    <li class="<?php echo $currentChildClass; ?>">
                        <a href="<?php echo get_permalink($child->ID); ?>"><?php echo $child->post_title ?></a>
                    </li>
                <?php endforeach; ?>
            <?php else: ?>
                <?php echo $children; ?>
            <?php endif; ?>
        </ul>
   <?php }
}
?>
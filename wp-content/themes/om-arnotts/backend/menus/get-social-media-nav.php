<?php

function omGetSocialMediaNav()
{
    $menu_name = 'social_media_menu';

    if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {

        $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );

        $menu_items = wp_get_nav_menu_items($menu->term_id);

        $menu_list = '<ul class="list-social-media-links">';

        foreach ( (array) $menu_items as $key => $menu_item ) {
            $title = $menu_item->title;
            $url = $menu_item->url;
            $menu_list .= '<li><a class="'.strtolower($title).'" href="' . $url . '" target="_blank"><span class="sr-only">' . $title . '</span></a></li>';
        }

        $menu_list .= '</ul>';

        return $menu_list;
    }
}

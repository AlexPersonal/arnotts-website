<?php

/**
 * Class CustomMainNav
 * This class extens the Wordpress_Nav_Menu class to support
 * the description value to be printed out and to support of Submenu titles.
 */
class CustomMainNav extends Walker_Nav_Menu
{
    /**
     * Constructor. The custom title that will be used for the 2nd level
     * sub-menu.
     *
     * @param $customTitle { String } The custom title for the 2nd level menu.
     */
    function __construct($customTitle)
    {
        $this->customTitle = $customTitle;
    }

    function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output)
    {
        $id_field = $this->db_fields['id'];
        if (!empty($children_elements[$element->$id_field])) {
            $element->classes[] = 'dropdown';

//            $element->title .= '<span class="dropdown-toggle" data-toggle="dropdown"></span>';
        }
        Walker_Nav_Menu::display_element($element, $children_elements, $max_depth, $depth, $args, $output);

    }

    /**
     * Will write out the description value from the backend.
     * @param string $output
     * @param object $item
     * @param int $depth
     * @param array|object $args
     */
    function start_el(&$output, $item, $depth, $args)
    {

        global $wp_query;
        $indent = ($depth) ? str_repeat("\t", $depth) : '';

        $class_names = $value = '';

        $classes = empty($item->classes) ? array() : (array)$item->classes;

        if (count($classes)) {
            $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item));
            $class_names = ' class="' . esc_attr($class_names) . '"';

            $output .= $indent . '<li id="menu-item-' . $item->ID . '"' . $value . $class_names . '>';


            $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
            $attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
            $attributes .= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
            $attributes .= !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';

            $item_output = '';

            if ($depth >= 1) {
                $item_output .= $args->before;
                $item_output .= '<a' . $attributes . ' class="sub-category">';
                $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
                $item_output .= '</a>';
                $item_output .= $item->description;
                $item_output .= $args->after;

            } else {

                $item_output .= $args->before;

                // Remove the dropdown-toggle and data-toggle if the element doesn't have a child
                // Otherwise the element is not clickable
                if ($item->classes && in_array('menu-item-has-children', $item->classes)) {
                    $item_output .= '<a' . $attributes . ' class="parent-of-dropdown">';
                    $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
                    $item_output .= '</a><span class="dropdown-toggle" data-toggle="dropdown"></span>';


                } else {
                    $item_output .= '<a' . $attributes . ' class="bla">';
                    $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
                    $item_output .= '</a>';
                }

                $item_output .= $item->description;
                $item_output .= $args->after;
            }

            $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
        }
    }

    /**
     * Extend the Submenu hook.
     * @param string $output
     * @param int $item
     * @param array $depth
     */
    function start_lvl(&$output, $item, $depth)
    {
        $depth = (!empty($depth) && is_long($depth)) ? $depth : 1;

        // depth dependent classes
        $indent = str_repeat("\t", $depth); // code indent
        $display_depth = ($depth + 1); // because it counts the first submenu as 0
        $classes = array(
            'dropdown-menu',
            ($display_depth == 1 ? '' : ''),
            ($display_depth >= 2 ? 'dropdown-menu-lvl2' : ''),
            'menu-depth-' . $display_depth
        );

        $class_names = implode(' ', $classes);

        if ($display_depth == 2) {

            $output .= "\n" . $indent . '<ul class="' . $class_names . '">';

            // build html
            if($this->customTitle) {
                $output .= "\n" . '<h4>' . $this->customTitle . '</h4>' . "\n";
            }

        } else {
            $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
        }

        apply_filters('walker_nav_menu_start_lvl', $item, $item, $depth);
    }
}

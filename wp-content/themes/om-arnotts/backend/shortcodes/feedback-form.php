<?php

function omSendFeedback() {

	$obj = omSetFeedbackData();

	if($feedbackDropdown == 'complaints') {
		$email_subject = "User Complaint Feedback";
	}else{
		$email_subject = "User Comment Feedback";
	}

	$email_message = "There has been a new user submission.\n\nName: " . $obj->firstNameInput . " " . $obj->lastNameInput . "\n" .
	"Address: " . $obj->streetInput . ' ' . strtoupper($obj->suburbInput) . ', ' . $obj->stateDropdown . ' ' . $obj->postcodeInput . ', ' . $obj->countryDropdown . "\r\n" .
	'Email: ' . $obj->emailInput . "\n" .
	'Telephone: ' . $obj->telephoneInput . "\n" .
	'Details: ' . $obj->feedbackInput . "\n\n" .
	'Product details (if applicable);' . "\n" .
	'Pack Size: ' . $obj->packetSizeInput . "\n" .
	'Product Name: ' . $obj->productNamesInput . "\n" .
	'Best Before: ' . $obj->bestBeforeDropdown . "\n" .
	'Freq: ' . $obj->howOftenDropdown . "\n" .
	'Store Details: ' . $obj->wherePurchasedInput . ' in ' . $obj->storeLocationInput . "\n";

	$email_to = "consumers@arnotts.com";
	// $email_to = "tamara.wohl@orchard.com.au";

	$headers = 'From: consumercontactus@arnotts.com'."\n".

	'Reply-To: '.$obj->emailInput."\n" .

	'X-Mailer: PHP/' . phpversion();


	$sent = mail($email_to, $email_subject, $email_message, $headers);

}

function omSetFeedbackData() {

	$obj = (object) array(
		'feedbackDropdown' => $_POST['feedbackDropdown'],
		'feedbackInput' => stripslashes($_POST['feedbackInput']),
		'firstNameInput' => $_POST['firstNameInput'],
		'lastNameInput' => $_POST['lastNameInput'],
		'emailInput' => $_POST['emailInput'],
		'postcodeInput' => $_POST['postcodeInput'],
		'productNamesInput' => $_POST['productNamesInput'],
		'packetSizeInput' => $_POST['packetSizeInput'],
		'howOftenDropdown' => $_POST['howOftenDropdown'],
		'bestBeforeDropdown' => $_POST['bestBeforeDropdown'],
		'wherePurchasedInput' => $_POST['wherePurchasedInput'],
		'storeLocationInput' => $_POST['storeLocationInput'],
		'telephoneInput' => $_POST['telephoneInput'],
		'privacyStatment' => $_POST['privacyStatment'],
		'gRecaptchaResponse' => $_POST['g-recaptcha-response'],
		'streetInput' => $_POST['streetInput'],
		'suburbInput' => $_POST['suburbInput'],
		'stateDropdown' => $_POST['stateDropdown'],
		'countryDropdown' => $_POST['countryDropdown']
	);

	return $obj;
}

function omValidateMail() {

	$obj = omSetFeedbackData();

	$postArray = Array();
	$errorsArray = '';

	foreach ($_POST as $key => $value) {
		$postArray[$key] = $value;
	}

	foreach ($postArray as $key => $value) {
		// echo "$key = $value";
		$errorsArray .= validateSanitize($key, $value);
	}

	if($feedbackDropdown == 'complaints') {
		// Generic validation
		$errorsArray .= validateTelephone($obj->telephoneInput, "This doesn't look like a valid telephone number. Please try again."  . "<br/>");

		// Required fields
		$errorsArray .= validateRequiredField($obj->feedbackInput, "Please enter a description of your complaint."  . "<br/>");
		$errorsArray .= validateRequiredField($obj->productNamesInput, "Please enter a product name."  . "<br/>");
		$errorsArray .= validateRequiredField($obj->packetSizeInput, "Please enter the packet size"  . "<br/>");
		$errorsArray .= validateRequiredField($obj->howOftenDropdown, "Please select how often you eat this product."  . "<br/>");
		$errorsArray .= validateRequiredField($obj->wherePurchasedInput, "Please enter the store where purchased. "  . "<br/>");
		$errorsArray .= validateRequiredField($obj->storeLocationInput, "Please enter the location of the store.  "  . "<br/>");

		// No numbers
		$errorsArray .= validateNoNumbers($obj->productNamesInput, "Please enter a product name."  . "<br/>");
		$errorsArray .= validateNoNumbers($obj->wherePurchasedInput, "Please enter the store where purchased. "  . "<br/>");
		$errorsArray .= validateNoNumbers($obj->storeLocationInput, "Please enter the location of the store.  "  . "<br/>");

		// No characters
		$errorsArray .= validateDate($obj->bestBeforeDropdown, "Please enter the best before date"  . "<br/>");
		$errorsArray .= validateNoCharacters($obj->telephoneInput, "This doesn't look like a valid telephone number. Please try again."  . "<br/>");
	}else{
		$errorsArray .= validateRequiredField($obj->feedbackInput, "Please enter a description of your general feedback / question."  . "<br/>");
	}

	// Generic validation
	$errorsArray .= validateEmail($obj->emailInput, "This doesn't look like a valid email. Please try again."  . "<br/>");
	$errorsArray .= validatePostCode($obj->postcodeInput, "Please enter your 4-digit postcode."  . "<br/>");

	// Required fields
	$errorsArray .= validateRequiredField($obj->firstNameInput, "Please enter your first name."  . "<br/>");
	$errorsArray .= validateRequiredField($obj->lastNameInput, "Please enter your last name."  . "<br/>");
	$errorsArray .= validateRequiredField($obj->emailInput, "Please enter your email address."  . "<br/>");

	// No numbers
	$errorsArray .= validateNoNumbers($obj->firstNameInput, "Please enter your first name."  . "<br/>");
	$errorsArray .= validateNoNumbers($obj->lastNameInput, "Please enter your last name."  . "<br/>");

	// No characters
	$errorsArray .= validateNoCharacters($obj->postcodeInput, "Please enter your 4-digit postcode."  . "<br/>");

	return $errorsArray;

}

function validateEmail($value, $message) {
	$email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
	if(!preg_match($email_exp,$value)) {
		return $message;
	}
}

function validateDate($value, $message) {
	$dob_exp = '/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/';
	if(!preg_match($dob_exp,$value)) {
		return $message;
	}
}

function validatePostCode($value, $message) {
	if(strlen($value) != 4 && strlen($value) > 0) {
		return $message;
	}
}

function validateTelephone($value, $message) {
	if(strlen($value) != 20 && strlen($value) > 0) {
		return $message;
	}
}

function validateRequiredField($value, $message) {
	if(strlen($value) == 0) {
		return $message;
	}
}

function validateNoNumbers($value, $message) {
	$string_exp = "/^[A-Za-z .'-]+$/";
	if(!preg_match($string_exp,$value) && strlen($value) > 0) {
		return $message;
	}
}

function validateNoCharacters($value, $message) {
	$number_exp = "/^[0-9 .'-]+$/";
	if(!preg_match($number_exp,$value) && strlen($value) > 0) {
		return $message;
	}
}

function validateSanitize($key, $value) {
	if($key == 'emailInput' && $value != '') {
		if(!sanitize_email($value)) {
			$errors .= 'Naughty no attacking the database via your email ' . $value;
		}
	}else if($key != 'g-recaptcha-response' && $value != ''){
		if(!sanitize_text_field($value)) {
			$errors .= 'Naughty no attacking the database via a text field ' . $value;
		}
	}
}

?>
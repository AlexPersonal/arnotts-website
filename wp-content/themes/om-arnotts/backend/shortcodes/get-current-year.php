<?php

/**
 * @return bool|string { String } Returns the current year
 */
function omShowCurrentYear() {
    return date('Y');
}

add_shortcode( 'om-current-year', 'omShowCurrentYear' );
<?php
/**
 * Adds a shortcode for printing out all the pages that were pulled into the 'About Arnotts' Menu.
 * The area for this menu was registered here arnotts-corporate/source/wp-content/themes/om-arnotts/backend/menus/menus.php
 *
 */

function omListAboutArnottsMenuPages() {

    $defaults = array(
        'theme_location'  => 'about_arnotts_menu',
        'menu'            => '',
        'container'       => '',
        'container_class' => 'arnotts-submenu',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => 'about-arnotts-menu',
        'echo'            => false,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        'depth'           => 1,
        'walker'          => ''
    );

    $aboutArnottsMenu = wp_nav_menu( $defaults );

    return $aboutArnottsMenu;

}

add_shortcode( 'om-list-about-arnotts-menu-pages', 'omListAboutArnottsMenuPages' );
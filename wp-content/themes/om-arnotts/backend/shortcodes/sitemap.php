<?php
/**
 * Created by IntelliJ IDEA.
 * User: jessica.nierth
 * Date: 23/01/2015
 * Time: 3:32 PM
 */

/**
 * @param $args
 * @return string
 */
function om_print_sitemap($args)
{

    $product_id = 48;
    $nutrition_id = 52;
    $about_id = 56;


    $categories = omGetAll1stLevelProductCategories();

    $categoriesLength = count($categories);

    $o  = '<h2 class="menu-level-0 with-sub-menu"><a href="/">Home</a></h2>';
    $o .= '<hr>';
    $o .= '<ul class="menu-level-0 with-sub-menu">';
    $o .= wp_list_pages("title_li=&include=".$product_id."&echo=0");
    $o .= '</ul>';

    if ($categoriesLength) :
        for ($i = 0; $i < $categoriesLength; $i++) :

            if ($i == 0 || $i % 4 == 0) :
                $o .= '<div class="row">';
            endif;
            $o .= '<div class="sitemap-products">';
            $o .= '<h3 id="' . omConvertStringToValidID($categories[$i]->cat_name) . '">' . $categories[$i]->cat_name . '</h3>';

            $products = omGetProducts($categories[$i]);
            $productsLength = count($products);

            $o .= '<ul class="list-products-overview">';
                for ($j = 0; $j < $productsLength; $j++) :
                    $o .= '<li>';
                    $o .= '<a href="'. get_permalink($products[$j]->ID) .'" class="tile tile--style-3">';
                    $o .= '<p>' . $products[$j]->post_title . '</p>';
                    $o .= '</a>';
                    $o .= '</li>';
                endfor;
            $o .= '</ul>';
            $o .= '</div>';

            if ($i > 0 && $i % 3 == 0 || $i == $categoriesLength - 1) :
                $o .= '</div>';
            endif;

        endfor;

    endif;

    $o .= '<hr>';


    $nutrition_arg = array(
        'authors'      => '',
        'child_of'     => $nutrition_id,
        'depth'        => 1,
        'echo'         => FALSE,
        'exclude'      => 0,
        'include'      => 0,
        'link_after'   => '',
        'link_before'  => '',
        'post_type'    => 'page',
        'post_status'  => 'publish',
        'sort_column'  => 'post_title',
        'sort_order'   => 'ASC',
        'title_li'     => $post->title ,
        'walker'       => ''
    );

    $o .= '<div class="row">';
    $o .= '<div class="col-sm-12">';
    $o .= '<ul class="menu-level-0 with-sub-menu">';
    $o .= wp_list_pages("title_li=&include=".$nutrition_id."&echo=0");
    $o .= '</ul>';
    $o .= '<ul>';
    $o .= wp_list_pages($nutrition_arg);
    $o .= '</ul>';
    $o .= '</div>';
    $o .= '</div>';
    $o .= '<hr>';

    $about_arg = array(
        'authors'      => '',
        'child_of'     => $about_id,
        'depth'        => 1,
        'echo'         => FALSE,
        'exclude'      => 0,
        'include'      => '',
        'link_after'   => '',
        'link_before'  => '',
        'post_type'    => 'page',
        'post_status'  => 'publish',
        'sort_column'  => 'post_title',
        'sort_order'   => 'ASC',
        'title_li'     => FALSE,
        'walker'       => ''
    );

    $o .= '<div class="row">';
    $o .= '<div class="col-sm-12">';
    $o .= '<ul class="menu-level-0 with-sub-menu">';
    $o .= wp_list_pages("title_li=&include=".$about_id."&echo=0");
    $o .= '</ul>';
    $o .= '<ul class="with-arrow">';
    $o .= wp_list_pages($about_arg);
    $o .= '</ul>';
    $o .= '</div>';
    $o .= '</div>';
    $o .= '<hr>';

    $other_arg = array(
        'authors'      => '',
        'child_of'     => 0,
        'depth'        => 1,
        'echo'         => FALSE,
        'exclude'      => $about_id . ',' . $nutrition_id . ',' . $product_id,
        'include'      => 0,
        'link_after'   => '',
        'link_before'  => '',
        'post_type'    => 'page',
        'post_status'  => 'publish',
        'sort_column'  => 'post_title',
        'sort_order'   => 'ASC',
        'title_li'     => FALSE,
        'walker'       => ''
    );

    $o .= '<div class="row">';
    $o .= '<div class="col-sm-12">';
    $o .= '<h2 class="menu-level-0">Other pages</h2>';
    $o .= '<ul class="with-arrow">';
    $o .= wp_list_pages($other_arg);
    $o .= '</ul>';
    $o .= '</div>';
    $o .= '</div>';

    return $o;

}

add_shortcode('om-print-sitemap', 'om_print_sitemap');
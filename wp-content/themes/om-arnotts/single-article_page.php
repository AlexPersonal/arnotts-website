<?php

require('header.php');

?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
    // Get the hero image
    $heroImageUrl = (has_post_thumbnail()) ? omGetPostThumbnailUrl(get_post_thumbnail_id($post->ID), null) : omGetPostThumbnailUrl(getFallbackHeroImage(), null);
    ?>

    <div class="page-hero" style="<?php echo 'background-image:url(\''. $heroImageUrl .'\');'?>">
        <div class="container">
            <h1>
                <?php the_title(); ?>
            </h1>
        </div>
    </div>


    <div class="section-global--color-6">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php if (function_exists('om_breadcrumb_lists')) { om_breadcrumb_lists(); } ?>
                </div>
            </div>
        </div>
    </div>

    <div role="main">
        <div class="section-global page-intro">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">

                        <div class="wysiwyg">
                            <?php the_content() ?>
                        </div>


                <?php endwhile;

                    else:

                    // If no content, include the "No posts found" template.
                    get_template_part('content', 'none');

                    endif;
                ?>
                    </div><!-- /.col-lg-8 -->
                    <div class="col-lg-4">
                        <?php omGetSecondaryMenu($post); ?>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>


        <?php

        $crossLinkBottom = get_post_meta($post->ID, 'cross_link_tile_bottom', false);
        $crossLinkTileLength = count($crossLinkBottom);
        $contactUsTile = get_post_meta($post->ID, 'contact_us_tile');

        if($crossLinkTileLength && $crossLinkBottom[0] != false):
        ?>

            <div class="section-global section-global--color-12">
                <div class="container">

                    <?php
                    for ($i = 0; $i < $crossLinkTileLength; $i++) :

                        ?>
                        <div class="col-md-6 cross-tile--wrapper">
                            <?php
                            $crossLinkBackgroundImage = get_post_meta($crossLinkBottom[$i]['ID'], 'cross_link_background_image', true);
                            $crossLinkTitle = get_post_meta($crossLinkBottom[$i]['ID'], 'cross_link_title', true);
                            $crossLinkTileInternalLink = get_post_meta($crossLinkBottom[$i]['ID'], 'cross_link_internal_link', true);
                            $crossLinkTileExternalLink = get_post_meta($crossLinkBottom[$i]['ID'], 'cross_link_external_link', true);

                            ?>


                            <a href="<?php echo omReturnLink($crossLinkTileInternalLink, $crossLinkTileExternalLink); ?>" class="cross-tile cross-tile--default"
                               style="background-image:url('<?php echo omGetPostThumbnailUrl($crossLinkBackgroundImage['ID'], 'om-tablet'); ?>')">
                                <span class="overlay"></span>

                                <h3><?php echo $crossLinkTitle; ?></h3>
                            </a>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        <?php endif; ?>

        </div><!-- .main-content -->

<?php require('footer.php'); ?>
<?php
/*
 * Template Name: Product Overview
 */

require('header.php');

?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
    // Get the hero image
    $heroImageUrl = (has_post_thumbnail()) ? omGetPostThumbnailUrl(get_post_thumbnail_id($post->ID), null) : omGetPostThumbnailUrl(getFallbackHeroImage(), null);
    ?>

    <div class="page-hero" style="<?php echo 'background-image:url(\''. $heroImageUrl .'\');'?>">
        <div class="container">
            <h1>
                <?php $termSlug = get_query_var('product_category'); ?>
                <?php $term = (get_term_by('slug', $termSlug, 'product_category', true));  ?>
                <?php echo $term->name; ?>
            </h1>
        </div>
    </div>

    <div class="section-global--color-6">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php if (function_exists('om_breadcrumb_lists')) { om_breadcrumb_lists(); } ?>
                </div>
            </div>
        </div>
    </div>

    <div role="main">
        <div class="section-global section-global--color-6 page-intro">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="wysiwyg">
                            <?php the_content() ?>
                        </div>


                <?php endwhile;

                    else:

                    // If no content, include the "No posts found" template.
                    get_template_part('content', 'none');

                    endif;
                ?>
                    </div><!-- /.col-sm-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>

        <?php if(!empty($term->term_taxonomy_id)) {?>

            <div class="section-global section-products-overview">
                <div class="container">

                    <?php
                        $products = omGetProductsBySingleCategoryID($term->term_taxonomy_id);
                        $productsLength = count($products);
                    ?>

                    <ul class="list-products-overview">
                        <?php for($j=0; $j < $productsLength && ($j < 4); $j++) { ?>
                            <li>
                                <a href="<?php echo get_permalink($products[$j]->ID); ?>" class="tile tile--style-3">
                                    <div class="image--container">
                                        <?php $productThumb = get_post_meta( $products[$j]->ID, 'product_overview_image', true ); ?>

                                        <?php if(!empty($productThumb['ID'])): ?>
                                            <?php echo omGetImageByAttachmentID($productThumb['ID'],'small') ?>
                                        <?php endif; ?>
                                    </div>

                                    <div class="tile--footer">
                                        <p><?php echo $products[$j]->post_title; ?></p>
                                    </div>
                                </a>
                            </li>
                        <?php } ?>

                        <?php if($productsLength > 4):?>
                            <li class="view-more-products">
                                <button class="global-link global-link--plus js-view-more-products" data-product-category-id="<?php echo $term->term_taxonomy_id; ?>" data-nonce="<?php $nonce ?>"></button>
                            </li>
                        <?php endif;?>
                    </ul>
                <?php } ?>
            </div>
        </div>
    </div><!-- .main-content -->
<?php require('footer.php'); ?>
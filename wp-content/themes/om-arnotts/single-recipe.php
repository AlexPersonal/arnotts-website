<?php
/**
 * The recipe detail page
 *
 * @package WordPress
 * @since omarnotts
 */


require('header.php');

?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php
    $recipeListPageID = 50;
    // Get the hero image
    $heroImageUrl = omGetPostThumbnailUrl(get_post_thumbnail_id($recipeListPageID), null);
    ?>

<div class="page-recipe-detail" itemscope itemtype="http://schema.org/Recipe">
    <div class="page-hero" style="<?php echo 'background-image:url(\''. $heroImageUrl .'\');'?>">
        <div class="container">
            <h1 itemprop="name">
                <?php the_title(); ?>
            </h1>
        </div>
    </div>

    <div class="section-global--color-6 print-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <?php if (function_exists('om_breadcrumb_lists')) { om_breadcrumb_lists(); } ?>
                </div>
                <div class="col-sm-6">
                    <?php include_once('add-this.php'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="section-global section-global--color-6 page-intro">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <div class="page-intro--copy" itemprop="description">
                        <?php the_content() ?>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>


    <div class="section-global section-recipe-content">
            <div class="container">
                <div class="row">

                    <div class="col-md-8">

                    <?php $recipeCheatSheetData = omGetRecipeCheatSheet($post->ID); ?>
                        <?php // only show this if any cheatsheet data is available ?>
                        <?php if(count($recipeCheatSheetData)) :?>
                            <div class="section-global section-recipe-preview">
                                <div class="recipe-cheatsheet">

                                    <div class="recipe-preview--image">
                                        <?php $recipeImage = (get_post_meta($post->ID,'recipe_preview_image', true)); ?>
                                        <?php $recipeImageID = $recipeImage["ID"] ?>
                                        <?php $recipes_image_url = omGetPostThumbnailUrl($recipeImageID, 'om-tablet'); ?>
                                        <img src="<?php echo $recipes_image_url; ?>" itemprop="image" />
                                    </div>
                                    <ul class="recipe-cheatsheet-list">
                                        <?php foreach($recipeCheatSheetData as $recipeCheatSheet) : ?>
                                            <li class="recipe-cheatsheet--<?php echo $recipeCheatSheet['icon-type']; ?>">
                                                <span class="title"><?php echo $recipeCheatSheet['title']?>:</span>
                                                <?php if($recipeCheatSheet['title'] == 'Cooking time'){
                                                    ?><time datetime="<?php echo time_to_iso8601_duration($recipeCheatSheet['content']); ?>" itemprop="cookTime"><?php echo $recipeCheatSheet['content']?></time><?php
                                                }elseif($recipeCheatSheet['title'] == 'Prep time'){
                                                    ?><time datetime="<?php echo time_to_iso8601_duration($recipeCheatSheet['content']); ?>" itemprop="prepTime"><?php echo $recipeCheatSheet['content']?></time><?php
                                                }elseif($recipeCheatSheet['title'] == 'Serves'){
                                                    ?><span itemprop="recipeYield"><?php echo $recipeCheatSheet['content']?></span><?php
                                                }else{ ?>
                                                    <span><?php echo $recipeCheatSheet['content']?></span>
                                                <?php } ?>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                    <?php endif; ?>

                    <?php if(omGetRecipeIngredientsList($post->ID)) :?>
                    <div class="section-global section-global--color-12 section-recipe--ingredients">
                        <h3>Ingredients</h3>
                        <div class="wysiwyg" itemprop="ingredients">
                            <?php
                                echo omGetRecipeIngredientsList($post->ID);
                            ?>
                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if(omGetRecipeHowToDoItList($post->ID)) :?>
                    <div class="section-global section-global--color-12 section-recipe--method">
                        <hr/>
                        <h3>Method</h3>
                        <div class="wysiwyg" itemprop="recipeInstructions">
                            <?php
                                echo omGetRecipeHowToDoItList($post->ID);
                            ?>
                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if(omGetRecipeTips($post->ID)) : ?>
                    <div class="section-global section-global--color-12 section-recipe--tips">
                        <hr/>
                        <h3>Recipe Tips</h3>
                        <div class="wysiwyg">
                            <?php
                            echo omGetRecipeTips($post->ID);
                            ?>
                        </div>
                    </div>
                    <?php endif; ?>
                </div><!-- .col-md-8 -->

                    <div class="col-md-4 sidebar">
                        <?php omGetRecipeProductCrossLinkTile($post->ID); ?>
                    </div>

                </div><!-- .row -->
            </div><!-- .container -->
        </div><!-- .section-global -->

    <?php omGetRecommendedRecipesByPostMetaRecipeRelation($post->post_title, $post->ID); ?>

</div><!-- .page-recipe-detail -->


<?php
endwhile;

else:
    // If no content, include the "No posts found" template.
    get_template_part('content', 'none');

endif;
?>


<?php omGetBottomCrossLinkTiles($post->ID); ?>

<?php require('footer.php'); ?>

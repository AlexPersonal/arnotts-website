<?php
/*
 * Template Name: Product Overview
 */

require('header.php');

?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
    // Get the hero image
    $heroImageUrl = (has_post_thumbnail()) ? omGetPostThumbnailUrl(get_post_thumbnail_id($post->ID), null) : omGetPostThumbnailUrl(getFallbackHeroImage(), null);
    ?>

    <div class="page-hero" style="<?php echo 'background-image:url(\''. $heroImageUrl .'\');'?>">
        <div class="container">
            <h1>
                <?php the_title(); ?>
            </h1>
        </div>
    </div>

    <div class="section-global--color-6">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php if (function_exists('om_breadcrumb_lists')) { om_breadcrumb_lists(); } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="section-global section-global--color-6 page-intro">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div class="wysiwyg">
                        <?php the_content() ?>
                    </div>


                    <?php endwhile;

                        else:

                        // If no content, include the "No posts found" template.
                        get_template_part('content', 'none');

                        endif;
                    ?>
                    </div><!-- /.col-sm-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
    </div>

    <?php

    $categories = omGetAll1stLevelProductCategories();

    $categoriesLength = count($categories);


    if($categoriesLength) {?>
        <div class="section-global section-global--color-9 visible-md visible-lg">
            <div class="container">
                <div class="list-category-anchors-wrapper">
                    <p>Our Categories</p>
                    <ul class="list-category-anchors js-scroll-to-section">
                        <?php for($i=0;$i<$categoriesLength;$i++) { ?>
                            <li>
                                <a href="#heading-<?php echo omConvertStringToValidID($categories[$i]->cat_name); ?>-accordion"><?php echo $categories[$i]->cat_name . ' '; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="section-global section-products-overview js-section-products-overview panel-group" id="products-overview-accordion">
            <div class="container">
                <?php for($i=0;$i<$categoriesLength;$i++) { ?>
                <div class="panel panel-default">
                    <div id="heading-<?php echo omConvertStringToValidID($categories[$i]->cat_name); ?>-accordion" class="panel-heading">
                        <h2 class="panel-title">
                            <span class="hidden-xs hidden-sm"><?php echo $categories[$i]->cat_name . ' '; ?></span>
                            <a class="hidden-lg hidden-md collapsed" data-toggle="collapse" data-parent="#accordion" href="#<?php echo omConvertStringToValidID($categories[$i]->cat_name); ?>-accordion" >
                                <?php echo $categories[$i]->cat_name . ' '; ?>
                            </a>
                        </h2>
                        <?php
                            $products = omGetProducts($categories[$i]);
                            $productsLength = count($products);
                        ?>
                    </div>
                </div>
                <div id="<?php echo omConvertStringToValidID($categories[$i]->cat_name); ?>-accordion" class="list-products-overview section-remove-row panel-collapse collapse">
                    <div class="panel-body">
                        <p><?php echo $categories[$i]->description; ?></p>
                        <ul>
                            <?php for($j=0; $j < $productsLength && ($j < 4); $j++) { ?>
                                <li>
                                    <a href="<?php echo get_permalink($products[$j]->ID); ?>" class="tile tile--style-3">
                                        <div class="image--container">
                                            <?php $productThumb = get_post_meta( $products[$j]->ID, 'product_overview_image', true ); ?>

                                            <?php if(!empty($productThumb['ID'])): ?>
                                                <?php $imageURL = omGetImageUrlByAttachmentID($productThumb['ID'], 'om-package-size');?>
                                                <img src="<?php echo $imageURL ?>" alt="<?php echo $products[$j]->post_title; ?>"/>
                                            <?php endif; ?>
                                        </div>

                                        <div class="tile--footer">
                                            <p><?php echo $products[$j]->post_title; ?></p>
                                        </div>
                                    </a>
                                </li>
                            <?php } ?>

                            <?php if($productsLength > 4):?>
                                <li class="view-more-products">
                                    <button class="global-link global-link--plus js-view-more-products" data-product-category-id="<?php echo $categories[$i]->cat_ID; ?>" data-nonce="<?php $nonce ?>"></button>
                                </li>
                            <?php endif;?>
                        </ul>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>

       
    <?php  }?>



<?php require('footer.php'); ?>
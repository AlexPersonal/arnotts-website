<?php
/**
 * The template for displaying the add this.
 *
 * This has been included on the media_centre detail page.
 */
?>

<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
    <a class="addthis_button_facebook"></a>
    <a class="addthis_button_twitter"></a>
    <a class="addthis_button_print"></a>
    <a class="addthis_button_email"></a>
    <a class="addthis_button_compact"></a>
<!-- <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>-->
</div>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>

<?php
/**
 * The template for displaying the footer.
 *
 * @package WordPress
 */
?>

<!-- Closing main content -->
    <footer>
        <div class="container">

            <div class="row">
                <div class="col-md-6">
                    <a class="icon-logo" href="/">
                        <img src="<?php bloginfo('template_url'); ?>/assets/images/logo-arnotts-footer.png" alt="Arnotts - Logo"/>
                    </a>
                </div>
                <div class="col-md-6">

                    <?php if(function_exists('omGetSocialMediaNav')): ?>
                        <div class="social-media-footer pull-right">
                            <p>Join us on </p><?php echo omGetSocialMediaNav(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="footer--border-bottom"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?php if(function_exists('omGetFooterNav')): ?>
                            <?php echo omGetFooterNav(); ?>
                    <?php endif; ?>
                </div>
                <div class="col-md-6">
                    <?php if(is_active_sidebar('copywright')): ?>
                        <div class="copywright">
                            <?php dynamic_sidebar('copywright'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </footer>
   <?php wp_footer(); ?>

	</body>
</html>

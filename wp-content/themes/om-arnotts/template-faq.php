<?php
/*
 * Template Name: FAQ Listing Page
 */

require('header.php');

?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php

    $currentPostPageID = $post->ID;

    // Get the hero image
    $heroImageUrl = (has_post_thumbnail()) ? omGetPostThumbnailUrl(get_post_thumbnail_id($currentPostPageID), null) : omGetPostThumbnailUrl(getFallbackHeroImage(), null);

    ?>

    <div class="page-hero" style="<?php echo 'background-image:url(\''. $heroImageUrl .'\');'?>">
        <div class="container">
            <h1>
                <?php the_title(); ?>
            </h1>
        </div>
    </div>
    <?php endwhile;

    else:

        // If no content, include the "No posts found" template.
        get_template_part('content', 'none');

    endif;
    ?>

    <?php $faqs = omGetFAQs(); ?>
    <?php $currentPage = omGetCurrentPage(); ?>
    <?php $postsPerPage = omGetFAQPostsPerPage(); ?>
    <?php $faqsLength = count($faqs); ?>

    <div class="section-global section-global--color-6 page-intro search-form-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php if (function_exists('om_breadcrumb_lists')) { om_breadcrumb_lists(); } ?>
                </div>
            </div>
            <div class="row">
                <form action="/<?php echo $wp->query_vars['pagename']?>/" method="GET" class="section-global--color-6 search-form">
                    <div class="col-md-2 search-item">
                        <label class="keywordLabel search--single-line">Category</label>
                    </div>
                    <div class="col-md-4 search-item">
                        <?php omGetFAQCategoriesSelect(); ?>
                    </div>
                    <div class="col-md-1">
                        <button type="submit" class="btn btn-default">Go</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="" role="main">
        <div class="section-global">
            <div class="container">

                <?php // SET THE LOOP START POINTS ?>
                <?php $startLoop = (!$currentPage ||  $currentPage == 1) ? 0 : ($currentPage * $postsPerPage) - $postsPerPage; ?>

                <div class="row">
                    <div class="col-lg-9">
                        <?php if($faqsLength): ?>
                            <?php for($i = $startLoop; $i < $faqsLength && ($i < $startLoop + $postsPerPage); $i++): ?>

                                <div class="search-result-item">
                                    <div class="search-text--wrapper">
                                        <h3><?php echo $faqs[$i]->post_title; ?></h3>
                                        <?php echo $faqs[$i]->post_content; ?>
                                        <hr>
                                    </div>
                                </div>
                            <?php endfor; ?>
                        <?php else: ?>
                            <div class="search-result-item">
                                <div class="search-text--wrapper">
                                    <h3>Sorry no results could be found.</h3>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php omCustomLoopPagination($faqs, $postsPerPage); ?>

                    </div><!-- /.col-lg-9 -->
                    <div class="col-lg-3">
                        <?php omGetSidbarCrossLinkTiles($currentPostPageID); ?>
                    </div>
                </div>
            </div><!-- /.container -->
        </div>
    </div>


<?php omGetBottomCrossLinkTiles($currentPostPageID); ?>

<?php require('footer.php'); ?>
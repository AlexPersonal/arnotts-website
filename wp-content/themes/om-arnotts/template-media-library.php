<?php
/*
 * Template Name: Media Library Listing Page
 */
require('header.php');

?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
        $currentPostPageID = $post->ID;

        // Get the hero image
        $heroImageUrl = (has_post_thumbnail()) ? omGetPostThumbnailUrl(get_post_thumbnail_id($post->ID), null) : omGetPostThumbnailUrl(getFallbackHeroImage(), null);
    ?>

<div class="page-hero" style="<?php echo 'background-image:url(\''. $heroImageUrl .'\');'?>">
    <div class="container">
        <h1>
            <?php the_title(); ?>
        </h1>
    </div>
</div>


<div class="section-global--color-6">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php if (function_exists('om_breadcrumb_lists')) { om_breadcrumb_lists(); } ?>
            </div>
        </div>
    </div>
</div>

<div class="" role="main">
    <div class="section-global section-global--color-6 page-intro search-form-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <?php the_content() ?>

                        <?php endwhile;

                            else:

                            // If no content, include the "No posts found" template.
                            get_template_part('content', 'none');

                            endif;
                        ?>

                    </div><!-- /.col-sm-12 -->
                </div><!-- /.row -->


                <?php // SECONDARY LOOP FOR MEDIA LIBRARY POSTS ?>
                <?php $allMediaLibraryPosts = getAllMediaLibraryPosts(); ?>
                <?php $mediaLibraryPosts = omGetMediaLibraryPosts(); ?>
                <?php $postsPerPage = omGetMediaLibraryPostsPerPage(); ?>
                <?php $currentPage = omGetCurrentPage(); ?>
                <?php $selectedFileType = omGetMediaLibraryFileType(); ?>
                <?php $selectedYear = omGetMediaLibraryYear(); ?>
                <?php $allMediaLibraryPosts = omGetAvailableMediaLibraryAssetFiletypesAndYears($allMediaLibraryPosts); ?>
                <?php $fileTypes = $allMediaLibraryPosts['filetypes']; ?>
                <?php $years = $allMediaLibraryPosts['years']; ?>

                <div class="row">
                    <div class="col-sm-12">

                        <form action="/<?php echo $wp->query_vars['pagename']; ?>/" method="GET" class="row search-form">
                            <div class="col-md-2 search-item">
                                <label class="keywordLabel search--single-line">Search by</label>
                            </div>
                            <div class="col-md-4 search-item">
                                <input name="keywords" type="text" value="<?php echo sanitize_text_field(get_query_var('keywords')); ?>" placeholder="Keywords" />
                            </div>

                            <?php if(count($fileTypes)) :?>
                                <div class="col-md-3 search-item">
                                    <select name="filetype">
                                        <option value="">Choose file type</option>
                                        <?php foreach($fileTypes as $fileType => $value) : ?>
                                            <?php if($fileType === $selectedFileType) :?>
                                                <option selected="selected" value="<?php echo $fileType; ?>"><?php echo $fileType; ?></option>
                                            <?php else: ?>
                                                <option value="<?php echo $fileType; ?>"><?php echo $fileType; ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            <?php endif; ?>

                            <?php if(count($years)) :?>
                                <div class="col-md-3 search-item">
                                    <select name="mlyear" class="year-drop-down">
                                        <option value="">Choose year</option>
                                        <?php foreach($years as $year => $value) : ?>
                                            <?php if($year == $selectedYear) :?>
                                                <option selected="selected" value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                            <?php else: ?>
                                                <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>

                                    <button type="submit" class="btn btn-default">Go</button>
                                </div>
                            <?php endif; ?>
                        </form>
                    </div>
                </div>
        </div><!-- /.container -->
    </div>

    <div class="section-global search-list media-library">
        <div class="container">
            <div class="row">
                <?php // SET THE LOOP START POINTS ?>
                <?php $startLoop = (!$currentPage ||  $currentPage == 1) ? 0 : ($currentPage * $postsPerPage) - $postsPerPage; ?>

                <div class="col-lg-9">

                        <?php $keywords = sanitize_text_field($wp->query_vars['keywords']); ?>
                        <?php $fileType = sanitize_text_field($wp->query_vars['filetype']); ?>
                        <?php $year = sanitize_text_field($wp->query_vars['mlyear']); ?>
                        <?php $mediaLibraryLength  = count($mediaLibraryPosts) ?>
                        <?php $resultsCopy = ($mediaLibraryLength) ? 'Your search has ' . $mediaLibraryLength . ' results' : 'Sorry, your search has 0 results'; ?>

                        <div class="search-tagline">
                            <p><?php echo $resultsCopy ?></p>

                            <?php if($keywords || $fileType || $year) : ?>
                                <p>You searched for <?php echo omGetMediaLibrarySearchParameterList($keywords, $fileType, $year); ?></p>
                            <?php endif; ?>
                        </div>


                    <?php for($i = $startLoop; $i < $mediaLibraryLength && ($i < $startLoop + $postsPerPage); $i++): ?>

                    <?php
                        $mediaLibraryPostID = $mediaLibraryPosts[$i]->ID;
                        $media_library_asset = (get_post_meta($mediaLibraryPostID, 'media_library_asset', true));
                        $media_library_title = (get_post_meta($mediaLibraryPostID, 'media_library_title', true));
                        $media_library_description = (get_post_meta($mediaLibraryPostID, 'media_library_description', true));

                        $media_library_asset_url = $media_library_asset['guid'];
                        $media_library_id = $media_library_asset['ID'];
                        $media_library_post_date = mysql2date('j F Y', $mediaLibraryPosts[$i]->post_date);
                        $media_library_file_type = wp_check_filetype($media_library_asset_url);
                        $media_library_size = size_format(filesize( get_attached_file( $media_library_id ) ), 0);
                    ?>

                    <div class="search-result-item">
                        <div class="row">
                            <div class="col-sm-2 media-image">
                                <a href="<?php echo $media_library_asset_url ?>" download>
                                    <i class="icon icon-<?php echo $media_library_file_type['ext'] ?>"></i>
                                </a>
                            </div>
                            <div class="col-sm-10 search-text--wrapper">
                                <h3><?php echo $media_library_title ?></h3>
                                <p class="date"><?php echo $media_library_post_date; ?></p>
                                <p><?php echo $media_library_description ?></p>
                                <a href="<?php echo $media_library_asset_url ?>" class="global-link" download>download  <?php echo $media_library_file_type['ext'] ?> (<?php echo $media_library_size; ?>)</a>
                            </div>
                        </div>
                    </div>
                    <?php endfor; ?>

                    <?php omCustomLoopPagination($mediaLibraryPosts, $postsPerPage); ?>

                </div><!-- /.col-lg-9 -->

                <div class="col-lg-3">
                    <?php omGetSidbarCrossLinkTiles($currentPostPageID); ?>
                </div>
            </div>
        </div>
</div><!-- .section-global -->
</div>
<?php omGetBottomCrossLinkTiles($currentPostPageID); ?>

<?php require('footer.php'); ?>
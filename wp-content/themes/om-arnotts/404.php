<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage omarnotts
 */

get_header(); ?>

    <?php
    // Get the hero image
    $heroImageUrl = (get_theme_mod('omarnotts_404_hero_image')) ? get_theme_mod('omarnotts_404_hero_image') : omGetPostThumbnailUrl(getFallbackHeroImage(), null);

    ?>

    <div class="page-hero" style="<?php echo 'background-image:url(\''. $heroImageUrl .'\');'?>">
        <div class="container">
            <h1>
                Oops! That page couldn't be found.
            </h1>
        </div>
    </div>

    <div class="section-global--color-6">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php if (function_exists('om_breadcrumb_lists')) { om_breadcrumb_lists(); } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container error404--wrapper">


        <div class="row">
            <div class="col-sm-12">
                <div class="page-content">
                    <p><?php _e( 'The page you are looking for might have been moved, had its name changed or is temporarily unavailable. Please check that the Web site address is spelled correctly.', 'omarnotts' ); ?></p>

                    <a class="global-link" href="/">Go Back Home</a>
                    <a class="global-link" href="/sitemap">View Sitemap</a>
                </div><!-- .page-content -->

            </div><!-- #content -->
        </div><!-- .row -->
    </div>
<?php

get_footer();
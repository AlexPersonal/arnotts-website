<?php
    /**
 * Front Page
 */

    require('header.php');

?>

<?php if (function_exists('omGetHeroCarousel')) :?>
    <?php omGetHeroCarousel(); ?>
<?php endif; ?>


    <div class="section-global section-global--color-6 section-whats-new-at-arnotts">
        <div class="container">

            <?php if(is_active_sidebar('what-s-new')): ?>
                <div class="intro-copy">
                    <?php dynamic_sidebar('what-s-new'); ?>
                </div>
            <?php endif; ?>

            <?php if (function_exists('omGetFeatureCarousel')) :?>
                <?php omGetFeatureCarousel(); ?>
            <?php endif; ?>
        </div><!-- ./container -->
    </div>


    <div class="section-global section-global--color-6 explore-the-world-of-arnotts">
        <div class="container">

            <?php if(is_active_sidebar('explore')): ?>
                <div class="intro-copy">
                    <?php dynamic_sidebar('explore'); ?>
                </div>
            <?php endif; ?>


            <?php $mixedArray = getHomepageGridPosts(); ?>

                <?php if (count($mixedArray)): ?>
                    <div class="row visible-lg visible-md">

                        <?php
                            $i = 0;
                            foreach ($mixedArray as $articles):

                                $postID = $mixedArray[$i]->ID;

                                // Excerpt
                                // Only used as a fallback. This field is required so if it is null we know we are dealing with a recipe
                                $media_centre_excerpt = get_post_meta($postID, 'media_centre_excerpt', true);
                                $media_centre_homepage_tile_excerpt = get_post_meta($postID, 'homepage_grid_tile_excerpt', true);
                                $recipe_excerpt = (get_post_meta($postID, 'recipe_homepage_excerpt', true));

                                $media_centre_excerpt = (strlen($media_centre_homepage_tile_excerpt) == 0) ? $media_centre_excerpt : $media_centre_homepage_tile_excerpt;
                                $format_excerpt = ($media_centre_excerpt) ? $media_centre_excerpt : $format_excerpt = $recipe_excerpt;

                                // Background Image
                                $media_center_homepage_image = (get_post_meta($postID, 'homepage_grid_tile_desktop', true));
                                $recipeHomepageImage = (get_post_meta($postID, 'homepage_grid_tile_desktop', true));
                                $homepage_image  = ($recipeHomepageImage) ? $recipeHomepageImage : $media_center_homepage_image;
                                $recipeOrNewsImageUrl = omGetPostThumbnailUrl($homepage_image['ID'], null);

                                // Post Date
                                $postDate = mysql2date('j F Y', $articles->post_date);
                                $seoPostDate = mysql2date('Y-m-d', 'Y-m-d',true);
                                $postLink = get_permalink($postID);

                                if($mixedArray && $i == 0) {
                            ?>

                                <div class="col-sm-4">

                                   <?php renderTileHeight2($mixedArray[$i], $recipeOrNewsImageUrl,$postLink, $postDate,$seoPostDate,$format_excerpt); ?>

                                <?php } elseif($i == 1  && $mixedArray[$i]) { ?>

                                   <?php renderTileHeight1($mixedArray[$i], $recipeOrNewsImageUrl,$postLink, $postDate,$seoPostDate,$format_excerpt); ?>

                                </div>
                            <?php } elseif($i == 2 && $mixedArray[$i]){ ?>

                                <div class="col-sm-4">
                                    <?php renderTileHeight1($mixedArray[$i], $recipeOrNewsImageUrl,$postLink, $postDate,$seoPostDate,$format_excerpt); ?>

                            <?php } elseif($i == 3 && $mixedArray[$i]){ ?>


                                <?php renderTileHeight1($mixedArray[$i], $recipeOrNewsImageUrl,$postLink, $postDate,$seoPostDate,$format_excerpt); ?>

                            <?php }elseif($i == 4 && $mixedArray[$i]){ ?>

                                    <?php renderTileHeight1($mixedArray[$i], $recipeOrNewsImageUrl,$postLink, $postDate,$seoPostDate,$format_excerpt); ?>
                                </div> <!-- /.col-sm-4 -->

                            <?php }elseif($i == 5 && $mixedArray[$i]){ ?>
                            <div class="col-sm-4">

                                    <?php renderTileHeight1($mixedArray[$i], $recipeOrNewsImageUrl,$postLink, $postDate,$seoPostDate,$format_excerpt); ?>

                                <?php }elseif($i == 6 && $mixedArray[$i]){ ?>

                                    <?php renderTileHeight2($mixedArray[$i], $recipeOrNewsImageUrl,$postLink, $postDate, $seoPostDate, $format_excerpt); ?>

                            </div>
                            <?php } ?>

                            <?php $i++; ?>

                         <?php endforeach; ?>
                        </div>

                    <div class="row js-responsive-carousel global-carousel js-news-carousel news-carousel">

                        <?php $j = 0; ?>
                        <?php foreach ($mixedArray as $articles): ?>

                            <?php
                                $postID = $articles->ID;
                                $recipeHomepageImage = (get_post_meta($postID, 'homepage_grid_tile_mobile_and_listing', true));
                                $media_center_homepage_image = (get_post_meta($postID, 'homepage_grid_tile_mobile_and_listing', true));
                                $media_centre_excerpt = (get_post_meta($postID, 'media_centre_excerpt', true));
                                $recipe_excerpt = (get_post_meta($postID, 'recipe_homepage_excerpt', true));
                                $postDate = mysql2date('j F Y', $articles->post_date);
                                $seoPostDate = mysql2date('Y-m-d', 'Y-m-d',true);

                                $postLink = get_permalink($postID);

                                $homepage_image  = ($recipeHomepageImage) ? $recipeHomepageImage : $media_center_homepage_image;
                                $recipeOrNewsImageUrl = omGetPostThumbnailUrl($homepage_image['ID'], null);
                            ?>


                        <div class="tile tile--style-6 tile--height-2">
                            <a href="<?php echo $postLink; ?>">
                                <div class="news-carousel--image-wrapper" style="background-image:url('<?php echo $recipeOrNewsImageUrl ?>');"></div>
                                <div class="tile-view--active">

                                    <h3><?php echo $articles->post_title ?></h3>

                                    <time datetime="<?php echo $seoPostDate; ?>"><?php echo $postDate ?></time>
                                    <?php if($recipe_excerpt) {?>
                                        <p><?php echo $recipe_excerpt ?></p>
                                    <?php }else{ ?>
                                        <p><?php echo $media_centre_excerpt ?></p>
                                    <?php } ?>
                                </div>
                            </a>
                        </div>



                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>


        <div class="section-global--color-6 section-social-media-feeds">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="tile tile--style-4 tile--facebook">
                            <h3>
                                <a href="<?php echo omGetSocialMediaLinkIfAvailable('facebook'); ?>" target="_blank">
                                    Arnott's on Facebook
                                </a>
                            </h3>
                            <?php
                                echo do_shortcode('[custom-facebook-feed textlength=400]');
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="tile tile--style-4 tile--twitter">
                            <h3>
                                <a href="<?php echo omGetSocialMediaLinkIfAvailable('twitter'); ?>" target="_blank">
                                    Arnott's on Twitter
                                </a>
                            </h3>
                            <?php echo do_shortcode('[wolf_tweet username="arnottsbikkies" type="single"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php get_footer(); ?>
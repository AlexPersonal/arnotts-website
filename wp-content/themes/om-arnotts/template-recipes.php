<?php
/*
 * Template Name: Recipes Listing Page
 */

require('header.php');

?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php

    $currentPostPageID = $post->ID;

    // Get the hero image
    $heroImageUrl = (has_post_thumbnail()) ? omGetPostThumbnailUrl(get_post_thumbnail_id($currentPostPageID), null) : omGetPostThumbnailUrl(getFallbackHeroImage(), null);
    $allRecipesPosts = omGetRecipePosts('');
    
    ?>

    <div class="page-hero" style="<?php echo 'background-image:url(\''. $heroImageUrl .'\');'?>">
        <div class="container">
            <h1>
                <?php the_title(); ?>
            </h1>
        </div>
    </div>


    <div class="section-global--color-6">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php if (function_exists('om_breadcrumb_lists')) { om_breadcrumb_lists(); } ?>
                </div>
            </div>
        </div>
    </div>
    <div role="main" class="recipe-list--wrapper">
        <div class="section-global section-global--color-6 page-intro">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="wysiwyg">
                            <?php the_content() ?>
                        </div>

                <?php endwhile;

                    else:

                    // If no content, include the "No posts found" template.
                    get_template_part('content', 'none');

                    endif;
                ?>
                    </div><!-- /.col-sm-12 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-6">
                        <form action="/<?php echo $wp->query_vars['pagename']; ?>/" method="GET" class="search-form row">
                            <div class="search-item col-md-4">
                                <label class="keywordLabel" for="keywordInput">I'm looking for</label>
                            </div>
                            <div class="search-item col-md-6">
                                <?php omGetRecipesCategoriesSelect(); ?>
                            </div>
                            <button type="submit" class="btn btn-default">Go</button>
                        </form>
                    </div>
                </div>
            </div><!-- /.container -->
        </div>

        <div class="section-global">

            <div class="container">
                <div class="row">
                    <?php 

                    
                    $postsPerPage = omGetRecipesPostsPerPage();
                    $currentPage = omGetCurrentPage();
                    $startLoop = (!$currentPage ||  $currentPage == 1) ? 0 : ($currentPage * $postsPerPage) - $postsPerPage;
                    
                    $recipesNewLength  = count($allRecipesPosts); 

                    for($i = $startLoop; $i < $recipesNewLength && ($i < $startLoop + $postsPerPage); $i++): 
                        $recipesPostID = $allRecipesPosts[$i]->ID;
                        $recipes_image = (get_post_meta($recipesPostID, 'recipe_thumbnail_image', true));
                        $recipes_asset_url = omGetPostThumbnailUrl($recipes_image['ID'], null);
                        $recipes_serves = (get_post_meta($recipesPostID, 'recipe_serves', true));
                        $recipe_difficulty = (get_post_meta($recipesPostID, 'recipe_difficulty', true));
                        $recipe_preparation_time = (get_post_meta($recipesPostID, 'recipe_preparation_time', true));
                        $recipes_link = get_permalink($recipesPostID);
                        
                        ?>
                        <a href="<?php echo $recipes_link ?>" class="recipe-tile--wrapper col-sm-4">
                            <div class="recipe-tile">
                                <img src="<?php echo $recipes_asset_url ?>" alt="<?php echo $allRecipesPosts[$i]->post_title; ?>" />
                                <h3 class="recipe-title"><?php echo $allRecipesPosts[$i]->post_title; ?></h3>
                                <div class="text-wrapper">
                                    <h3><?php echo $allRecipesPosts[$i]->post_title; ?></h3>
                                    <p>Difficulty: <span><?php echo $recipe_difficulty; ?></span></p>
                                    <p>Prep Time: <span><?php echo $recipe_preparation_time; ?></span></p>
                                    <p>Serves: <span><?php echo $recipes_serves; ?></span></p>
                                </div>
                            </div>
                        </a>
                            

                    <?php endfor; ?>

                    <?php if($recipesNewLength == 0): ?>
                        <h2>There are no current Recipes available.</h2>
                        <p>Please check back soon</p>
                    <?php endif; ?>
                </div>

            </div>
            <div class="container">
                <?php omCustomLoopPagination($allRecipesPosts, $postsPerPage); ?>
            </div>
        </div>
    </div><!-- .main-content -->

<?php echo omGetBottomCrossLinkTiles($currentPostPageID); ?>

<?php require('footer.php'); ?>
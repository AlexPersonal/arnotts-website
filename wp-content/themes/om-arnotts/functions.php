<?php
/**
 * The functions.php is Worpdress's first entry point and contains
 * the backend logic.
 *
 * @package WordPress
 */

// Configurations for deployment environments
include_once('backend/global/configs.php');

// Globally used functions
include_once('backend/global/helpers.php');

// Announcement banner
include_once('announcement.php');

// Homepage
include_once('backend/global/homepage.php');

// JS, CSS loading
include_once('backend/global/loadAssets.php');

// Security helpers
include_once('backend/global/security.php');

// Tracking - Tealium amends
//include_once('backend/global/tealium-amends.php');

// Theme adjustments
include_once('backend/global/theme-options.php');
include_once('backend/global/tinymc-adjustments.php');

// Images
include_once('backend/global/post-thumbnail-helpers.php');
include_once('backend/global/post-thumbnail-sizes.php');

// Show Admin bar
include_once('backend/global/wordpress-admin-bar.php');

// Extend Query Vars
include_once('backend/global/extendQueryVars.php');

// Add filter for the excerpts
include_once('backend/global/excerpt.php');

// Paginations
include_once('backend/global/pagination.php');

// Include breadcrumbs
include_once('backend/global/breadcrumbs.php');

// Include wordpress custom menus
include_once('backend/menus/menus.php');

// Overwrite the main menu structure to support the bootstrap 3 menu
//include_once('backend/menus/menu-walker.php');
include_once('backend/menus/menu-sidebar-secondary.php');
include_once('backend/menus/get-footer-nav.php');
include_once('backend/menus/get-social-media-nav.php');


// Custom post type helpers
include_once('backend/custom-post-types/crosslink-tiles.php');

// Include shortcodes
include_once('backend/shortcodes/about-arnotts-menu.php');
// Helper Methods - output the current year
include_once('backend/shortcodes/get-current-year.php');
// Prints sitemap
include_once('backend/shortcodes/sitemap.php');

// Sidebars
include_once('backend/sidebars/register-sidebars.php');

// Adjust list of categories
include_once('backend/sidebars/categories.php');

// Theme specific styles
include_once('backend/carousels/carousel-helpers.php');
include_once('backend/carousels/feature-carousel.php');
include_once('backend/carousels/hero-carousel.php');

// Custom post type - Products
include_once('backend/custom-post-types/products/view-more-handler.php');
include_once('backend/custom-post-types/products/products-overview.php');
include_once('backend/custom-post-types/products/product-single.php');
include_once('backend/custom-post-types/products/products-recommended-products.php');
include_once('backend/custom-post-types/products/products-promotion-panel.php');
include_once('backend/custom-post-types/products/products-social-media-panel.php');
include_once('backend/custom-post-types/products/products-recipe-panel.php');

// Custom post type - Recipes
include_once('backend/custom-post-types/recipes/recipe-detail-page.php');

// SEO
include_once('backend/seo/canonical-url.php');

// Custom post type - FAQs
include_once('backend/custom-post-types/faqs.php');

// Custom post type - MEDIA LIBRARY
include_once('backend/custom-post-types/media-library.php');

// Custom post type - MEDIA CENTRE
include_once('backend/custom-post-types/media-centre.php');

// Custom post type - PROMOTIONS
include_once('backend/custom-post-types/promotions.php');

// Custom post type - RECIPES
include_once('backend/custom-post-types/recipes.php');

// Feedback form
include_once('backend/shortcodes/feedback-form.php');
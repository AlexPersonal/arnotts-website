<?php

function omGetAnnouncementBanner() {
    $announcementsArgs = array(
        'posts_per_page'   => '-1',
        'category'         => '',
        'category_name'    => '',
        'post_type'        => 'announcement',
        'post_status'      => 'publish'
    );


    $announcements = new WP_Query($announcementsArgs);
    if($announcements->posts) {

        $announcements = $announcements->posts;

    } else {
        $announcements = null;
    }

    $annoucementsPostID = $announcements[0]->ID;
    $announcements_text = (get_post_meta($annoucementsPostID, 'announcement_text', true));
    $announcements_link = (get_post_meta($annoucementsPostID, 'announcement_link', true));

    if($announcements):

?>
        <div class="announcement--wrapper">
            <div class="container">
                <i class="fa fa-exclamation"></i>
                <p>
                    <?php echo $announcements_text ?>
                </p>

                <a class="global-link" href="<?php echo $announcements_link ?>">Read More</a>
                <span class="js-close-announcement annuncement-close">
                    <i class="fa fa-close"></i> 
                </span>
            </div>
        </div>
<?php
    endif;
}
?>
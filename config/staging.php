<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'arnotts_corporate_stage');

/** MySQL database username */
define('DB_USER', 'publishing');

/** MySQL database password */
define('DB_PASSWORD', 'Pr0t3ct10n');


/** MySQL hostname */
define('DB_HOST', 'aus1st8');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm#K[M1y{!2`CF_M?>Wn/7k:EE ]6S78>q,ST]*O]jC)OAyYr6}2kI+KZF|$?y9K4');
define('SECURE_AUTH_KEY',  'VOI@1)71YEI`V#Q_.84Z}n/V|<;qG=EZqk~clpxvS@P-SzUq%HIt~[],9$d9ersp');
define('LOGGED_IN_KEY',    'w6}mrN8S[`3^xMX6@X(<>GuJC87+O&cayVf9aQ{!ZwSG]F0z i|.#;OD6K1dhHU,');
define('NONCE_KEY',        'e9dB,HM=dU|{U oAhV>diVAi1[WN E25w553am+nwJt2no!F0csSy=zo)|z3ZN%H');
define('AUTH_SALT',        'J#Cwd_]4bs1qqM1~e1Z!`kfG-vIKz+-_@MenzW.YD+xkQZr_es2&Im f/*$P6A:M');
define('SECURE_AUTH_SALT', 'W5stic=4 _+Nz)!bVQtNCH0d)%ulGf^PRe3--Khe+LPN=0,pcf?P#hUUHw%-gc.-');
define('LOGGED_IN_SALT',   '+l=F=8kQD5/,}A-~uu4]y+D;>MY[9S^&H*l(5>}6V38-mu_W&Q`G%?)u!A?r9gL:');
define('NONCE_SALT',       '|{LlH$ /_AQEOv(,`3z+G8c+a|Dw$CzHl :e]kz4tVXy:|RSr7?^hx~X+!c,vqyy');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/**
 * http://codex.wordpress.org/Administration_Over_SSL
 */
define('FORCE_SSL_ADMIN', true);
